#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

from flask import current_app as app
from PyGnuTLS.crypto import X509Certificate, X509PrivateKey, X509TrustList, Pkcs7
from PyGnuTLS.library.constants import (
    GNUTLS_PKCS7_INCLUDE_TIME,
    GNUTLS_PKCS7_INCLUDE_CERT,
)
from PyGnuTLS.library.errors import GNUTLSError

from jcat import JcatBlobText, JcatBlobKind
from lvfs.pluginloader import (
    PluginBase,
    PluginError,
    PluginSetting,
    PluginSettingBool,
    PluginSettingList,
)
from lvfs import ploader


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self, "sign-pkcs7")
        self.name = "PKCS#7 Signing"
        self.summary = "Sign files using the GnuTLS public key infrastructure"
        self.settings.append(PluginSettingBool(key="sign_pkcs7_enable", name="Enabled"))
        self.settings.append(
            PluginSetting(
                key="sign_pkcs7_privkey",
                name="Private Key",
                default="pkcs7/fwupd.org.key",
            )
        )
        self.settings.append(
            PluginSetting(
                key="sign_pkcs7_certificate",
                name="Certificate",
                default="pkcs7/fwupd.org_signed.pem",
            )
        )
        self.settings.append(
            PluginSettingList(key="sign_pkcs7_pubkey", name="Public Key")
        )

    def _sign_blob(self, contents: bytes) -> str:

        with open(self.get_setting("sign_pkcs7_certificate", required=True), "rb") as f:
            cert = X509Certificate(f.read())
        with open(self.get_setting("sign_pkcs7_privkey", required=True), "rb") as f:
            privkey = X509PrivateKey(f.read())
        pkcs7 = Pkcs7()
        try:
            pkcs7.sign(
                cert,
                privkey,
                contents,
                flags=GNUTLS_PKCS7_INCLUDE_TIME | GNUTLS_PKCS7_INCLUDE_CERT,
            )
        except GNUTLSError as e:
            raise PluginError("Failed to sign") from e
        data_sig = pkcs7.export()

        # verify this will actually work
        pubkey = self.get_setting("sign_pkcs7_pubkey")
        if pubkey:
            tl = X509TrustList()
            cert = X509Certificate(pubkey.encode())
            tl.add_ca(cert)
            pkcs7_tmp = Pkcs7()
            pkcs7_tmp.import_signature(data_sig)
            try:
                pkcs7_tmp.verify(tl, contents)
            except GNUTLSError as e:
                raise PluginError("Failed to sign") from e

        # return PEM string
        return data_sig.decode()

    def metadata_sign(self, blob):

        # create the detached signature
        blob_p7b = self._sign_blob(blob)
        return JcatBlobText(JcatBlobKind.PKCS7, blob_p7b)

    def archive_sign(self, blob):

        # create the detached signature
        blob_p7b = self._sign_blob(blob)
        return JcatBlobText(JcatBlobKind.PKCS7, blob_p7b)
