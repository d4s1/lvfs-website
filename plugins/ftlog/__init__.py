#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=protected-access

import random
import hashlib
import json
import os
import fcntl
import subprocess
from typing import Optional, Dict, List, Any
import tempfile
from flask import current_app as app

from cabarchive import CabArchive, CabFile
from jcat import JcatBlobText, JcatBlobKind, JcatBlob

from lvfs.firmware.models import Firmware
from lvfs.components.models import Component, ComponentGuid
from lvfs.pluginloader import PluginBase, PluginError, PluginSetting, PluginSettingBool


def _build_log_manifest(md: Component) -> bytes:
    """create a JSON file that represents the component"""
    return json.dumps(
        {
            "component_id": md.component_id,
            "guids": list(md.guid_values),
            "filename_contents": md.filename_contents,
            "sha256": md.checksum_contents_sha256,
        },
        indent=4,
        separators=(",", ": "),
    ).encode()


# must be used in a "with" block
class AtomicOpen:

    # open the file then acquire a lock
    def __init__(self, path: str, *args: Any, **kwargs: Any) -> None:
        self.file = open(  # pylint: disable=unspecified-encoding,consider-using-with
            path, *args, **kwargs
        )
        if self.file.writable():
            fcntl.lockf(self.file, fcntl.LOCK_EX)

    # return the opened file object
    def __enter__(self, *args: Any, **kwargs: Any) -> Any:
        return self.file

    # unlock the file and close the file object
    def __exit__(
        self, exc_type: Any = None, exc_value: Any = None, traceback: Any = None
    ) -> bool:
        self.file.flush()
        os.fsync(self.file.fileno())
        if self.file.writable():
            fcntl.lockf(self.file, fcntl.LOCK_UN)
        self.file.close()
        if exc_type is not None:
            return False
        return True


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self, "ftlog")
        self.name = "FT Log"
        self.summary = "Add metadata to a firmware transparency log"
        self.settings.append(PluginSettingBool(key="ftlog_enable", name="Enabled"))
        self.settings.append(
            PluginSettingBool(key="ftlog_jcat", name="Include checkpoint in JCat file")
        )
        self.settings.append(
            PluginSettingBool(key="ftlog_debug", name="Debugging enabled")
        )
        self.settings.append(
            PluginSetting(
                key="ftlog_certdir",
                name="Certificate Directory",
                default="/data/.ftlog_certs",
            )
        )
        self.settings.append(
            PluginSetting(
                key="ftlog_bindir",
                name="CLI Tools Directory",
                default="/usr/bin",
            )
        )
        self.settings.append(
            PluginSetting(
                key="ftlog_logdir",
                name="Log Directory",
                default="/data/.ftlog_log",
            )
        )
        self.settings.append(
            PluginSetting(
                key="ftlog_origin",
                name="Log Origin",
                default="lvfstest",
            )
        )
        self._needs_integrate: bool = False

    @property
    def _certdir(self) -> str:
        return os.getenv("FTLOG_CERTDIR") or self.get_setting(
            "ftlog_certdir", required=True
        )

    @property
    def _bindir(self) -> str:
        return os.getenv("FTLOG_BINDIR") or self.get_setting(
            "ftlog_bindir", required=True
        )

    @property
    def _origin(self) -> str:
        return os.getenv("FTLOG_ORIGIN") or self.get_setting("ftlog_origin")

    @property
    def _logdir(self) -> str:
        return os.getenv("FTLOG_LOGDIR") or os.path.join(
            self.get_setting("ftlog_logdir", required=True), self._origin
        )

    @property
    def _cachedir(self) -> str:
        """do not store anything locally"""
        return ""

    @property
    def _privkey(self) -> str:
        return os.path.join(self._certdir, "key.secret")

    @property
    def _pubkey(self) -> str:
        return os.path.join(self._certdir, "key.pub")

    def _generate_inclusion_proof(self, manifest: bytes) -> str:
        """generate the "detached" inclusion proof for the manifest"""

        with tempfile.NamedTemporaryFile(
            mode="wb", prefix="lvfs_", suffix=".bin", delete=True
        ) as src:
            src.write(manifest)
            src.flush()

            with tempfile.NamedTemporaryFile(
                mode="w+b", prefix="lvfs_", suffix=".inclusionproof", delete=True
            ) as dst:
                try:
                    argv = [
                        os.path.join(self._bindir, "client"),
                        "--cache_dir",
                        self._cachedir,
                        "--log_url",
                        "file://{}".format(self._logdir),
                        "--origin",
                        self._origin,
                        "--log_public_key",
                        self._pubkey,
                        "--logtostderr",
                        "--output_inclusion_proof",
                        dst.name,
                        "inclusion",
                        src.name,
                    ]
                    res = subprocess.run(argv, check=True, capture_output=True)
                    if self.debug:
                        print(res.stderr)
                except subprocess.CalledProcessError as e:
                    raise PluginError(
                        "Failed to build inclusion proof using '{}' : {}".format(
                            " ".join(argv), e.stderr
                        )
                    ) from e
                inclusion_proof = dst.read().decode()
                if not inclusion_proof:
                    raise PluginError(
                        "Got no inclusion proof data using '{}'".format(" ".join(argv))
                    )
        return inclusion_proof

    def _generate_checkpoint(self) -> str:
        """generate the latest checkpoint for the tree"""

        with tempfile.NamedTemporaryFile(
            mode="w+b", prefix="lvfs_", suffix=".checkpoint", delete=True
        ) as dst:
            try:
                argv = [
                    os.path.join(self._bindir, "client"),
                    "--cache_dir",
                    self._cachedir,
                    "--log_url",
                    "file://{}".format(self._logdir),
                    "--origin",
                    self._origin,
                    "--log_public_key",
                    self._pubkey,
                    "--logtostderr",
                    "--output_checkpoint",
                    dst.name,
                    "update",
                ]
                res = subprocess.run(argv, check=True, capture_output=True)
                if self.debug:
                    print(res.stderr)
            except subprocess.CalledProcessError as e:
                raise PluginError(
                    "Failed to get checkpoint using '{}' : {}".format(
                        " ".join(argv), e.stderr
                    )
                ) from e
            checkpoint = dst.read().decode()
            if not checkpoint:
                raise PluginError(
                    "Got no checkpoint data using '{}'".format(" ".join(argv))
                )
        return checkpoint

    def _ensure_log(self) -> None:
        """sets up the log as required, generating keys and initializing the tree"""

        # do we need to generate a new keypair?
        os.makedirs(self._certdir, exist_ok=True)
        if not os.path.exists(self._privkey):
            try:
                argv = [
                    os.path.join(self._bindir, "generate_keys"),
                    "--logtostderr",
                    "--key_name",
                    self._origin,
                    "--out_pub",
                    self._pubkey,
                    "--out_priv",
                    self._privkey,
                ]
                res = subprocess.run(argv, check=True, capture_output=True)
                if self.debug:
                    print(res.stderr)
            except subprocess.CalledProcessError as e:
                raise PluginError(
                    "Failed to create keys using '{}' : {}".format(
                        " ".join(argv), e.stderr
                    )
                ) from e

        # create the log if it does not already exist
        if not os.path.exists(os.path.join(self._logdir, "checkpoint")):
            try:
                argv = [
                    os.path.join(self._bindir, "integrate"),
                    "--initialise",
                    "--storage_dir",
                    self._logdir,
                    "--logtostderr",
                    "--public_key",
                    self._pubkey,
                    "--private_key",
                    self._privkey,
                    "--origin",
                    self._origin,
                ]
                res = subprocess.run(argv, check=True, capture_output=True)
                if self.debug:
                    print(res.stderr)
            except subprocess.CalledProcessError as e:
                raise PluginError(
                    "Failed to create log using '{}' : {}".format(
                        " ".join(argv), e.stderr
                    )
                ) from e

        # link the public key from the certdir into the tree
        if not os.path.exists(os.path.join(self._logdir, "key.pub")):
            os.symlink(
                os.path.join(os.path.relpath(self._certdir, self._logdir), "key.pub"),
                os.path.join(self._logdir, "key.pub"),
            )

    def _sequence(self, manifest: bytes) -> None:
        """adds new content into the log"""

        with tempfile.NamedTemporaryFile(
            mode="wb", prefix="lvfs_", suffix=".bin", delete=True
        ) as src:
            src.write(manifest)
            src.flush()
            try:
                argv = [
                    os.path.join(self._bindir, "sequence"),
                    "--storage_dir",
                    self._logdir,
                    "--logtostderr",
                    "--entries",
                    src.name,
                    "--public_key",
                    self._pubkey,
                    "--origin",
                    self._origin,
                ]
                res = subprocess.run(argv, check=True, capture_output=True)
                if self.debug:
                    print(res.stderr)
                if b"(dupe)" not in res.stderr:
                    self._needs_integrate = True
            except subprocess.CalledProcessError as e:
                raise PluginError(
                    "Failed to integrate log using '{}' : {}".format(
                        " ".join(argv), e.stderr
                    )
                ) from e

    def _integrate(self) -> None:
        """adds the sequenced content into the log"""

        # integrate the log only if required
        if not self._needs_integrate:
            return
        try:
            argv = [
                os.path.join(self._bindir, "integrate"),
                "--storage_dir",
                self._logdir,
                "--logtostderr",
                "--public_key",
                self._pubkey,
                "--private_key",
                self._privkey,
                "--origin",
                self._origin,
            ]
            res = subprocess.run(argv, check=True, capture_output=True)
            if self.debug:
                print(res.stderr)
        except subprocess.CalledProcessError as e:
            raise PluginError(
                "Failed to integrate log using '{}': {}".format(
                    " ".join(argv), res.stderr.decode()
                )
            ) from e
        self._needs_integrate = False

    def component_sign(self, md: Component) -> List[JcatBlob]:

        # ensure the log is set up correctly
        self._ensure_log()

        # write blob
        manifest = _build_log_manifest(md)

        # protect against multiple runners
        with AtomicOpen(os.path.join(self._logdir, "ftlog.lock"), "wb"):
            self._sequence(manifest)
            self._integrate()

        # do not add by default so we can work with old JCat versions
        if not self.get_setting_bool("ftlog_jcat"):
            return []

        # save manifest, checkpoint and inclusion proof
        return [
            JcatBlobText(JcatBlobKind.BT_MANIFEST, manifest.decode()),
            JcatBlobText(JcatBlobKind.BT_CHECKPOINT, self._generate_checkpoint()),
            JcatBlobText(
                JcatBlobKind.BT_INCLUSION_PROOF,
                self._generate_inclusion_proof(manifest),
            ),
        ]


# run with PYTHONPATH=. ./env/bin/python3 plugins/ftlog/__init__.py
if __name__ == "__main__":
    plugin = Plugin()
    _md = Component()
    _md.component_id = random.randrange(1, 9999)
    _md.filename_contents = "firmware.bin"
    _md.checksum_contents_sha256 = hashlib.sha256(b"hello").hexdigest()
    _md.guids.append(ComponentGuid(value="2082b5e0-7a64-478a-b1b2-e3404fab6dad"))
    os.environ["FTLOG_CERTDIR"] = "/tmp/ftlog/certs"
    os.environ["FTLOG_ORIGIN"] = "lvfstest"
    os.environ["FTLOG_LOGDIR"] = "/tmp/ftlog/ftlog"
    os.environ["FTLOG_BINDIR"] = "/home/hughsie/Code/trillian-examples"
    for blob in plugin.component_sign(_md):
        if not blob.data:
            continue
        print("{}: {}".format(blob.filename_ext, blob.data.decode()))
