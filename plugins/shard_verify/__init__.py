#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=no-member,too-few-public-methods,unused-argument

from typing import Dict

from lvfs import db
from lvfs.pluginloader import PluginBase, PluginSettingList
from lvfs.components.models import Component, ComponentShard
from lvfs.firmware.models import Firmware
from lvfs.tests.models import Test


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self, "shard-verify")
        self.name = "Shard Verify"
        self.summary = "Check shards for known issues"
        self.order_after = ["uefi-extract"]
        self.settings.append(
            PluginSettingList(
                key="shard_verify_data",
                name="Data",
                default=[
                    "16f41157-1de8-484e-b316-ddb77cb4080c:Affected by CVE-2021-3971:SecureBackDoorPeim present",
                    "32f16d8c-c094-4102-bd6c-1e533f8810f4:Affected by CVE-2021-3971:SecureBackDoor present",
                ],
            )
        )

    def ensure_test_for_fw(self, fw: Firmware) -> None:

        # add if not already exists
        test = fw.find_test_by_plugin_id(self.id)
        if not test:
            test = Test(plugin_id=self.id, waivable=True)
            fw.tests.append(test)

    def run_test_on_md(self, test: Test, md: Component) -> None:

        # find any shards that indicate a test failure
        for data in self.get_setting_list("shard_verify_data", required=True):
            try:
                guid, title, desc = data.split(":", maxsplit=2)
            except ValueError:
                continue
            if md.shard_by_guid(guid):
                test.add_fail(title, desc)


# run with PYTHONPATH=. ./env/bin/python3 plugins/shard_verify/__init__.py
if __name__ == "__main__":

    plugin = Plugin()
    _test = Test(plugin_id=plugin.id)
    _fw = Firmware()
    _md = Component()
    _shard = ComponentShard(guid="32f16d8c-c094-4102-bd6c-1e533f8810f4")
    _md.shards.append(_shard)
    _fw.mds.append(_md)
    plugin.run_test_on_md(_test, _md)
    for _attr in _test.attributes:
        print(_attr)
