#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=unused-argument

import hashlib
import hmac
import datetime
from flask import current_app as app

from lvfs import db
from lvfs.vendors.models import Vendor, VendorAffiliation
from lvfs.firmware.models import Firmware
from lvfs.metadata.models import Remote
from lvfs.tasks.models import Task


def _vendor_hash(vendor: Vendor) -> str:
    """Generate a HMAC of the vendor name"""
    return hmac.new(
        key=app.config["SECRET_VENDOR_SALT"].encode(),
        msg=vendor.group_id.encode(),
        digestmod=hashlib.sha256,
    ).hexdigest()


def _update_attrs_vendor(v: Vendor) -> None:

    # is an ODM if affiliation is set up
    v.is_odm = (
        db.session.query(VendorAffiliation.affiliation_id)
        .filter(VendorAffiliation.vendor_id_odm == v.vendor_id)
        .first()
        is not None
    )

    # 26 weeks is half a year or about 6 months
    now = datetime.datetime.utcnow() - datetime.timedelta(weeks=26)
    v.fws_stable_recent = (
        db.session.query(Firmware.firmware_id)
        .join(Firmware.remote)
        .filter(
            Remote.name == "stable",
            Firmware.vendor_id == v.vendor_id,
            Firmware.timestamp > now,
        )
        .count()
    )
    v.fws_stable = (
        db.session.query(Firmware.firmware_id)
        .join(Firmware.remote)
        .filter(Firmware.vendor_id == v.vendor_id, Remote.name == "stable")
        .count()
    )


def task_update_attrs_vendor(task: Task) -> None:
    v = db.session.query(Vendor).filter(Vendor.vendor_id == task.id).first()
    if v:
        _update_attrs_vendor(v)
        db.session.commit()


def task_update_attrs(task: Task) -> None:
    for v in db.session.query(Vendor).with_for_update(of=Vendor):
        _update_attrs_vendor(v)
    db.session.commit()
