#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

from typing import Any

from flask import Blueprint, request, url_for, redirect, flash, render_template
from flask_login import login_required
from sqlalchemy.exc import NoResultFound, IntegrityError

from lvfs import db

from lvfs.util import admin_login_required

from .models import Claim

bp_claims = Blueprint("claims", __name__, template_folder="templates")


@bp_claims.route("/")
@login_required
@admin_login_required
def route_list() -> Any:

    # only show claims with the correct group_id
    claims = db.session.query(Claim).order_by(Claim.kind.asc()).all()
    return render_template("claim-list.html", category="admin", claims=claims)


@bp_claims.post("/create")
@login_required
@admin_login_required
def route_create() -> Any:

    # ensure has enough data
    try:
        kind = request.form["kind"] or None
    except KeyError:
        flash("No form data found", "warning")
        return redirect(url_for("claims.route_list"))
    if not kind or not kind.islower() or kind.find(" ") != -1:
        flash("Failed to add claim: Value needs to be a lower case word", "warning")
        return redirect(url_for("claims.route_list"))

    # add claim
    try:
        claim = Claim(kind=kind, summary=request.form.get("summary"))
        db.session.add(claim)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Failed to add claim: The claim already exists", "info")
        return redirect(url_for("claims.route_list"))

    flash("Added claim", "info")
    return redirect(url_for("claims.route_show", claim_id=claim.claim_id))


@bp_claims.post("/<int:claim_id>/delete")
@login_required
@admin_login_required
def route_delete(claim_id: int) -> Any:

    # get claim
    try:
        db.session.query(Claim).filter(Claim.claim_id == claim_id).delete()
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Cannot delete", "danger")
        return redirect(url_for("claims.route_list"))

    flash("Deleted claim", "info")
    return redirect(url_for("claims.route_list"))


@bp_claims.post("/<int:claim_id>/modify")
@login_required
@admin_login_required
def route_modify(claim_id: int) -> Any:

    # find claim
    try:
        claim = (
            db.session.query(Claim)
            .filter(Claim.claim_id == claim_id)
            .with_for_update(of=Claim)
            .one()
        )
    except NoResultFound:
        flash("No claim found", "info")
        return redirect(url_for("claims.route_list"))

    # modify claim
    for key in ["kind", "icon", "summary", "url"]:
        if key in request.form:
            setattr(claim, key, request.form[key])
    db.session.commit()

    # success
    flash("Modified claim", "info")
    return redirect(url_for("claims.route_show", claim_id=claim_id))


@bp_claims.route("/<int:claim_id>/details")
@login_required
@admin_login_required
def route_show(claim_id: int) -> Any:

    # find claim
    try:
        claim = db.session.query(Claim).filter(Claim.claim_id == claim_id).one()
    except NoResultFound:
        flash("No claim found", "info")
        return redirect(url_for("claims.route_list"))

    # show details
    return render_template("claim-details.html", category="admin", claim=claim)
