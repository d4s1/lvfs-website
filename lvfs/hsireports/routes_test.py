#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=wrong-import-position

import os
import sys
import unittest

sys.path.append(os.path.realpath("."))

from lvfs.testcase import LvfsTestCase


class LocalTestCase(LvfsTestCase):
    def test_hsireports(self, _app, client):

        # upload a firmware that can receive a report
        self.login()
        self.upload(target="testing")

        # send empty
        rv = client.post("/lvfs/hsireports/upload")
        assert b"No data" in rv.data, rv.data.decode()

        # self less than what we need
        rv = client.post("/lvfs/hsireports/upload", data='{"MachineId" : "abc"}')
        assert b"invalid data, expected ReportVersion" in rv.data, rv.data.decode()

    def test_hsireports_infos(self, _app, client):

        self.login()
        self.upload()
        rv = client.get("/lvfs/hsireports/info/")
        assert "acme" not in rv.data.decode("utf-8"), rv.data

        # create
        rv = client.post(
            "/lvfs/hsireports/info/create",
            data=dict(
                appstream_id="acme",
            ),
            follow_redirects=True,
        )
        assert b"Added HSI report attribute" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/hsireports/info/")
        assert "acme" in rv.data.decode("utf-8"), rv.data.decode()
        rv = client.post(
            "/lvfs/hsireports/info/create",
            data=dict(
                appstream_id="acme",
            ),
            follow_redirects=True,
        )
        assert b"Already exists" in rv.data, rv.data.decode()

        # modify
        rv = client.post(
            "/lvfs/hsireports/info/1/modify",
            data=dict(
                name="ACME",
                level="2",
            ),
            follow_redirects=True,
        )
        assert b"Modified HSI report attribute" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/hsireports/info/")
        assert "ACME" in rv.data.decode("utf-8"), rv.data.decode()

        # show
        rv = client.get("/lvfs/hsireports/info/1", follow_redirects=True)
        assert b"ACME" in rv.data, rv.data.decode()

        # delete
        rv = client.post("/lvfs/hsireports/info/1/delete", follow_redirects=True)
        assert b"Deleted HSI report attribute" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/hsireports/info/")
        assert "acme" not in rv.data.decode("utf-8"), rv.data.decode()


if __name__ == "__main__":
    unittest.main()
