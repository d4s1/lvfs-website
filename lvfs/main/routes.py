#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=wrong-import-position,assigning-non-slot,too-many-lines

import os
import datetime
import uuid
import fnmatch
import random
import json
from typing import Dict, List, Iterator, Optional, Any

from flask import (
    Blueprint,
    Response,
    abort,
    flash,
    g,
    make_response,
    redirect,
    render_template,
    request,
    send_from_directory,
    stream_with_context,
    url_for,
)
from flask import current_app as app
from flask_login import login_required, login_user, logout_user
from sqlalchemy.orm import joinedload
from sqlalchemy.orm.exc import NoResultFound


from uswid import (
    uSwidIdentity,
    uSwidEntity,
    uSwidEntityRole,
    uSwidLink,
    uSwidContainer,
    uSwidFormatUswid,
    uSwidFormatCoswid,
    uSwidFormatIni,
)

from pkgversion import vercmp

from lvfs import db, lm, ploader, csrf, auth, cache

from lvfs.pluginloader import PluginError

from lvfs.analytics.models import AnalyticVendor
from lvfs.clients.models import Client, ClientMetric, ClientAcl
from lvfs.components.models import ComponentRequirement
from lvfs.firmware.models import Firmware, FirmwareRevision
from lvfs.tests.models import Test
from lvfs.metadata.models import Remote
from lvfs.metadata.utils import PulpManifest
from lvfs.users.models import User
from lvfs.geoip.models import Geoip
from lvfs.geoip.utils import _convert_ip_addr_to_integer
from lvfs.util import (
    admin_login_required,
    _event_log,
    _get_chart_labels_days,
    _get_client_address,
    _get_datestr_from_datetime,
    _get_settings,
)

from lvfs.vendors.models import Vendor
from lvfs.settings.models import Setting

from .models import Event

bp_main = Blueprint("main", __name__, template_folder="templates")


def _user_agent_safe_for_requirement(user_agent: str) -> bool:

    # very early versions of fwupd used 'fwupdmgr' as the user agent
    if user_agent == "fwupdmgr":
        return False

    # gnome-software/3.26.5 (Linux x86_64 4.14.0) fwupd/1.0.4
    sections = user_agent.split(" ")
    for chunk in sections:
        toks = chunk.split("/")
        if len(toks) == 2 and toks[0] == "fwupd":
            return vercmp(toks[1], "0.8.0") >= 0

    # this is a heuristic; the logic is that it's unlikely that a distro would
    # ship a very new gnome-software and a very old fwupd
    for chunk in sections:
        toks = chunk.split("/")
        if len(toks) == 2 and toks[0] == "gnome-software":
            return vercmp(toks[1], "3.26.0") >= 0

    # is is probably okay
    return True


def _user_agent_use_cdn(user_agent: str) -> bool:

    # gnome-software/3.26.5 (Linux x86_64 4.14.0) fwupd/1.0.4
    for chunk in user_agent.split(" "):
        try:
            name, version = chunk.split("/", maxsplit=1)
        except ValueError:
            continue
        if name == "fwupd":
            if version.startswith("1.5."):
                return vercmp(version, "1.5.10") >= 0
            return vercmp(version, "1.6.1") >= 0

    # assume yes
    return True


def _user_agent_fwupd_old(user_agent: str) -> bool:

    # fwupd/1.2.5
    for chunk in user_agent.split(" "):
        try:
            name, version = chunk.split("/", maxsplit=1)
        except ValueError:
            continue
        if name == "fwupd":
            # be pragmatic, and raise over time
            return vercmp(version, "1.2.10") < 0

    # assume not
    return False


def _user_agent_fwupd_obsolete(user_agent: str) -> bool:

    # fwupd/0.8.0
    for chunk in user_agent.split(" "):
        try:
            name, version = chunk.split("/", maxsplit=1)
        except ValueError:
            continue
        if name == "fwupd":
            return vercmp(version, "1.0.0") < 0

    # assume not
    return False


def _user_agent_use_html_404(user_agent: str) -> bool:

    # gnome-software/3.26.5 (Linux x86_64 4.14.0) fwupd/1.0.4
    for name in ["gnome-software", "fwupd", "sync-pulp"]:
        if user_agent.find(name) != -1:
            return False
    return True


# this is linked from each README, so redirect to somewhere better than 404
@bp_main.route("/downloads/")
def route_downloads() -> Any:
    return redirect(url_for("devices.route_list"))


@bp_main.route("/downloads/PULP_MANIFEST/vendor")
@bp_main.route("/downloads/PULP_MANIFEST/vendor/<int:vendor_id>")
@auth.login_required
def route_pulp_manifest_vendor(vendor_id: Optional[int] = None) -> Any:

    # optional, but useful for the admin to use
    if vendor_id:
        if g.user.check_acl("@admin"):
            try:
                vendor = (
                    db.session.query(Vendor).filter(Vendor.vendor_id == vendor_id).one()
                )
            except NoResultFound:
                return Response(
                    response="Invalid Vendor", status=404, mimetype="text/plain"
                )
        else:
            return Response(
                response="Permission Denied", status=403, mimetype="text/plain"
            )
    else:
        vendor = g.user.vendor

    manifest: PulpManifest = PulpManifest()
    download_dir = app.config["DOWNLOAD_DIR"]

    # add metadata
    if request.args.get("metadata", default=1, type=int):
        for basename in vendor.remote.basenames:
            fn = os.path.join(download_dir, basename)
            if os.path.exists(fn):
                manifest.add_file(fn)

    # add firmware in embargo
    for fw in vendor.remote.fws:
        manifest.add_fw(fw)

    # return response
    rsp = make_response(manifest.export())
    rsp.headers["Content-Disposition"] = "attachment; filename=PULP_MANIFEST"
    rsp.mimetype = "text/plain"
    return rsp


def _read_file_chunks(fn: str, chunksz: int = 0x80000) -> Iterator[bytes]:
    with open(fn, "rb") as fd:
        while 1:
            buf = fd.read(chunksz)
            if buf:
                yield buf
            else:
                break


def _send_from_directory_chunked(path: str, filename: str, content_type: str) -> Any:
    fullpath = os.path.join(path, filename)
    try:
        response = Response(
            stream_with_context(_read_file_chunks(fullpath)),
            headers={
                "Content-Type": content_type,
                "Content-Disposition": "attachment; filename={}".format(filename),
                "Content-Length": str(os.path.getsize(fullpath)),
            },
        )
        response.cache_control.public = True
        response.cache_control.max_age = 14400
        return response
    except FileNotFoundError:
        return Response(
            response="file not found",
            status=404,
            mimetype="text/plain",
        )


@bp_main.route("/<path:resource>")
def serveStaticResource(resource: str) -> Any:
    """Return a static image or resource"""

    # ban the robots that ignore robots.txt
    user_agent = request.headers.get("User-Agent")
    if user_agent:
        if user_agent.find("MJ12BOT") != -1:
            abort(403)
        if user_agent.find("ltx71") != -1:
            abort(403)
        if user_agent.find("Sogou") != -1:
            abort(403)

    # firmware transparency log
    if resource.startswith("ftlog"):
        return send_from_directory(app.config["DOWNLOAD_DIR"], resource)

    # cached
    if resource == "downloads/PULP_MANIFEST":
        return _send_from_directory_chunked(
            app.config["DOWNLOAD_DIR"],
            os.path.basename(resource),
            content_type="text/plain",
        )

    # log certain kinds of files
    if resource.endswith(".cab"):

        # check client address against blocklist
        client_address = _get_client_address()
        try:
            cacl = (
                db.session.query(ClientAcl)
                .filter(ClientAcl.addr == client_address)
                .with_for_update(of=ClientAcl)
                .one()
            )
            cacl.mtime = datetime.datetime.utcnow()
            cacl.cnt = cacl.cnt + 1 if cacl.cnt else 1
            db.session.commit()
            return Response(
                response=f"{cacl.message}\n",
                status=429,
                mimetype="text/plain",
            )
        except NoResultFound:
            pass

        # a special message for our friends with broken software
        if user_agent:
            try:
                useragent_globs = (
                    db.session.query(Setting.value)
                    .filter(Setting.key == "useragent_blocklist")
                    .one()[0]
                    .split("\n")
                )
                for useragent_glob in useragent_globs:
                    if fnmatch.fnmatch(user_agent, useragent_glob):
                        return Response(
                            response="blocked due to abuse, please contact the LVFS "
                            "admin team if this download is legitimate\n",
                            status=429,
                            mimetype="text/plain",
                        )
            except NoResultFound:
                pass

        # these are not secure, and have had zero testing; it's a liability allowing them
        if user_agent and _user_agent_fwupd_obsolete(user_agent):
            return Response(
                response="The fwupd version you are using is end-of-life and is no longer getting "
                "security updates.\n"
                "You can download firmware upgrades by updating the fwupd package to a newer version, "
                "which may also require upgrading your Linux distribution.\n",
                status=412,
                mimetype="text/plain",
            )

        # unsupported fwupd versions download a tiny percentage of all firmware updates but account
        # for a good chunk of the daily bandwidth as they do not process the CDN redirect
        if user_agent and _user_agent_fwupd_old(user_agent):
            if random.randint(0, 60) == 0:
                return Response(
                    response="The fwupd version you are using is very old and is no longer supported. "
                    "You should update fwupd to a newer version by upgrading your system or by "
                    "filing a bug with your Linux distribution.\n",
                    status=412,
                    mimetype="text/plain",
                )

        # increment the firmware download counter
        try:
            fw = (
                db.session.query(Firmware)
                .join(FirmwareRevision)
                .filter(FirmwareRevision.filename == os.path.basename(resource))
                .options(joinedload(Firmware.vendor))
                .one()
            )
        except NoResultFound:
            if user_agent and not _user_agent_use_html_404(user_agent):
                return Response(
                    response="file not found\n",
                    status=404,
                    mimetype="text/plain",
                )
            abort(404)

        # used as a CDN key to ensure we cache the right 'version' for the file
        # from the CDN, matching the current version
        fw_timestamp = int(fw.timestamp.timestamp())
        try:
            req_timestamp = int(request.args["ts"])
            if req_timestamp != fw_timestamp:
                return Response(
                    response="firmware timestamp did not match latest signed "
                    "version (expected {}, got {}), try refreshing metadata".format(
                        fw_timestamp, req_timestamp
                    ),
                    status=404,
                    mimetype="text/plain",
                )
            return _send_from_directory_chunked(
                app.config["DOWNLOAD_DIR"],
                os.path.basename(resource),
                content_type="application/vnd.ms-cab-compressed",
            )
        except ValueError as _:
            return Response(
                response="firmware signing timestamp was not valid",
                status=404,
                mimetype="text/plain",
            )
        except KeyError:
            pass

        # check the user agent isn't in the blocklist for this firmware
        for md in fw.mds:
            req = (
                db.session.query(ComponentRequirement)
                .filter(ComponentRequirement.component_id == md.component_id)
                .filter(ComponentRequirement.kind == "id")
                .filter(ComponentRequirement.value == "org.freedesktop.fwupd")
                .first()
            )
            if req and user_agent and not _user_agent_safe_for_requirement(user_agent):
                return Response(
                    response="detected fwupd version too old",
                    status=412,
                    mimetype="text/plain",
                )

        # get the country code
        ip_val = _convert_ip_addr_to_integer(client_address)
        try:
            (country_code,) = (
                db.session.query(Geoip.country_code)
                .filter(Geoip.addr_start < ip_val)
                .filter(Geoip.addr_end > ip_val)
                .first()
            )
        except TypeError:
            country_code = None

        # check the firmware vendor has no country block
        if country_code in fw.banned_country_codes:
            return Response(
                response="firmware not available from this IP range [{}]".format(
                    country_code
                ),
                status=451,
                mimetype="text/plain",
            )

        # each client has an allowance per day to download the same firmware
        datestr = _get_datestr_from_datetime(datetime.datetime.utcnow())
        client_cnt = (
            db.session.query(Client)
            .filter(Client.datestr == datestr)
            .filter(Client.addr == client_address)
            .filter(Client.firmware_id == fw.firmware_id)
            .count()
        )
        client_limit: int = app.config.get("CLIENT_DAILY_LIMIT", 1000)
        if client_cnt > client_limit:
            errmsg: str = "Client blocked due to abuse "
            errmsg += f"(daily limit {client_cnt+1} of {client_limit}). "
            errmsg += "Please contact the LVFS admin team if legitimate."
            cacl = ClientAcl(
                addr=client_address,
                message=errmsg,
                user_id=2,
            )
            db.session.add(cacl)
            db.session.commit()
            return Response(
                response=f"{errmsg}\n",
                status=429,
                mimetype="text/plain",
            )

        # log the client request
        if not fw.do_not_track:
            db.session.add(
                Client(
                    firmware_id=fw.firmware_id,
                    user_agent=user_agent,
                    addr=client_address,
                    country_code=country_code,
                    datestr=datestr,
                )
            )

            # this is updated best-effort, but also set in the cron job
            fw.download_cnt += 1
            metric = (
                db.session.query(ClientMetric)
                .filter(ClientMetric.key == "ClientCnt")
                .with_for_update(of=ClientMetric)
                .first()
            )
            if metric:
                metric.value += 1
            db.session.commit()

        # use the CDN
        if user_agent and _user_agent_use_cdn(user_agent):
            return redirect(
                os.path.join(
                    app.config["CDN_DOMAIN"],
                    "{}?ts={}".format(resource, fw_timestamp),
                ),
                code=302,
            )

        # fall back for older fwupd versions
        return _send_from_directory_chunked(
            app.config["DOWNLOAD_DIR"],
            os.path.basename(resource),
            content_type="application/vnd.ms-cab-compressed",
        )

    # non-firmware blobs
    if resource.startswith("downloads/"):
        return send_from_directory(
            app.config["DOWNLOAD_DIR"], os.path.basename(resource)
        )
    if resource.startswith("uploads/"):
        return send_from_directory(app.config["UPLOAD_DIR"], os.path.basename(resource))

    # static files served locally
    return send_from_directory(os.path.join(app.root_path, "static"), resource)


@lm.unauthorized_handler
def unauthorized():
    msg = ""
    if request.url:
        msg += "Tried to request %s" % request.url
    if request.user_agent:
        msg += " from %s" % request.user_agent
    flash("Permission denied: {}".format(msg), "danger")
    return redirect(url_for("main.route_index_uncached"))


def _route_index() -> Any:
    vendors_logo = (
        db.session.query(Vendor)
        .filter(Vendor.visible_on_landing)
        .order_by(Vendor.display_name)
        .limit(10)
        .all()
    )
    vendors_quote = (
        db.session.query(Vendor)
        .filter(Vendor.quote_text != None)
        .filter(Vendor.quote_text != "")
        .order_by(Vendor.display_name)
        .limit(10)
        .all()
    )
    return render_template(
        "index.html", vendors_logo=vendors_logo, vendors_quote=vendors_quote
    )


@bp_main.route("/")
@cache.cached()
def route_index() -> Any:
    return _route_index()


@bp_main.route("/lvfs/")
def route_index_uncached() -> Any:
    return _route_index()


@bp_main.route("/lvfs/dashboard")
@login_required
def route_dashboard() -> Any:
    settings = _get_settings()

    # get the 10 most recent firmwares
    fws = (
        db.session.query(Firmware)
        .filter(Firmware.user_id == g.user.user_id)
        .join(Remote)
        .filter(Remote.name != "deleted")
        .order_by(Firmware.timestamp.desc())
        .limit(10)
        .all()
    )

    download_cnt = 0
    devices_cnt = 0
    appstream_ids: Dict[str, Firmware] = {}
    for fw in g.user.vendor.fws:
        download_cnt += fw.download_cnt
        for md in fw.mds:
            appstream_ids[md.appstream_id] = fw
    devices_cnt = len(appstream_ids)

    # this is somewhat clunky
    data: List[int] = []
    datestr = _get_datestr_from_datetime(
        datetime.datetime.now() - datetime.timedelta(days=31)
    )
    for cnt in (
        db.session.query(AnalyticVendor.cnt)
        .filter(AnalyticVendor.vendor_id == g.user.vendor.vendor_id)
        .filter(AnalyticVendor.datestr > datestr)
        .order_by(AnalyticVendor.datestr)
    ):
        data.append(int(cnt[0]))

    return render_template(
        "dashboard.html",
        fws_recent=fws,
        devices_cnt=devices_cnt,
        download_cnt=download_cnt,
        labels_days=_get_chart_labels_days(limit=len(data))[::-1],
        data_days=data,
        server_warning=settings.get("server_warning", None),
        category="home",
    )


@bp_main.route("/lvfs/newaccount")
def route_new_account() -> Any:
    """New account page for prospective vendors"""
    return redirect("https://lvfs.readthedocs.io/en/latest/apply.html", code=302)


def _create_user_for_oauth_username(username: str) -> Optional[User]:
    """If any oauth wildcard match, create a *un-committed* User object"""

    # does this username match any globs specified by the vendor
    for v in db.session.query(Vendor).filter(
        Vendor.oauth_domain_glob != None
    ):  # pylint: disable=singleton-comparison
        for glob in v.oauth_domain_glob.split(","):
            if not fnmatch.fnmatch(username.lower(), glob):
                continue
            if v.oauth_unknown_user == "create":
                return User(username=username, vendor_id=v.vendor_id, auth_type="oauth")
            if v.oauth_unknown_user == "disabled":
                return User(username=username, vendor_id=v.vendor_id)
    return None


# unauthenticated
@bp_main.route("/lvfs/login1")
def route_login1() -> Any:
    if hasattr(g, "user") and g.user:
        flash("You are already logged in", "warning")
        return redirect(url_for("main.route_dashboard"))
    return render_template("login1.html")


# unauthenticated
@bp_main.post("/lvfs/login1")
def route_login1_response() -> Any:
    if "username" not in request.form:
        flash("Username not specified", "warning")
        return redirect(url_for("main.route_login1"))
    username = request.form["username"].lower()
    try:
        user = db.session.query(User).filter(User.username == username).one()
    except NoResultFound:
        flash("Failed to log in: Incorrect username {}".format(username), "danger")
        return redirect(url_for("main.route_login1"))
    return render_template("login2.html", u=user)


@bp_main.post("/lvfs/login")
@csrf.exempt
def route_login() -> Any:
    """A login screen to allow access to the LVFS main page"""
    if "username" not in request.form:
        flash("Username not specified", "warning")
        return redirect(url_for("main.route_login1"))

    # auth check
    username = request.form["username"].lower()
    user = (
        db.session.query(User)
        .filter(User.username == username)
        .with_for_update(of=User)
        .first()
    )
    if user:
        if user.auth_type == "oauth":
            flash(
                "Failed to log in as %s: Only OAuth can be used for this user"
                % user.username,
                "danger",
            )
            return redirect(url_for("main.route_index_uncached"))
        if not user.verify_password(request.form["password"]):
            flash(
                "Failed to log in: Incorrect password for {}".format(username), "danger"
            )
            return redirect(url_for("main.route_login1"))
    else:
        # check OAuth, user is NOT added to the database
        user = _create_user_for_oauth_username(username)
        if not user:
            flash("Failed to log in: Incorrect username {}".format(username), "danger")
            return redirect(url_for("main.route_index_uncached"))
        flash("Failed to log in: {} must use OAuth to login".format(username), "danger")
        return redirect(url_for("main.route_index_uncached"))

    # check auth type
    if not user.auth_type or user.auth_type == "disabled":
        if user.dtime:
            flash(
                "Failed to log in as %s: User account was disabled on %s"
                % (username, user.dtime.strftime("%Y-%m-%d")),
                "danger",
            )
        else:
            flash(
                "Failed to log in as %s: User account is disabled" % username, "danger"
            )
        return redirect(url_for("main.route_index_uncached"))

    # check OTP
    if user.is_otp_enabled:
        if "otp" not in request.form or not request.form["otp"]:
            flash("Failed to log in: 2FA OTP required", "danger")
            return redirect(url_for("main.route_login1"))
        if not user.verify_totp(request.form["otp"]):
            flash("Failed to log in: Incorrect 2FA OTP", "danger")
            return redirect(url_for("main.route_login1"))

    # success
    login_user(user, remember=False)
    g.user = user
    if user.password_ts:
        flash("Logged in", "info")
    else:
        flash("Logged in, now change your password using Profile ⇒ User", "info")

    # if we warned the user, clear that timer
    user.unused_notify_ts = None

    # set the access time
    user.atime = datetime.datetime.utcnow()
    db.session.commit()

    return redirect(url_for("main.route_dashboard"))


@bp_main.route("/lvfs/login/<plugin_id>")
def route_login_oauth(plugin_id: str) -> Any:

    # find the plugin that can authenticate us
    p = ploader.get_by_id(plugin_id)
    if not p:
        flash("no plugin {}".format(plugin_id), "warning")
        return render_template("error.html"), 402
    try:
        return p.oauth_authorize(
            url_for(
                "main.route_login_oauth_authorized",
                plugin_id=plugin_id,
                _external=True,
                _scheme="https",
            )
        )
    except PluginError as e:
        flash(str(e), "warning")
        return render_template("error.html"), 402


@bp_main.route("/lvfs/login/authorized/<plugin_id>")
def route_login_oauth_authorized(plugin_id: str) -> Any:

    # find the plugin that can authenticate us
    p = ploader.get_by_id(plugin_id)
    if not p:
        flash("no plugin {}".format(plugin_id), "warning")
        return render_template("error.html"), 402
    try:
        data = p.oauth_get_data()
        if "userPrincipalName" not in data:
            flash("No userPrincipalName in profile", "warning")
            return render_template("error.html"), 402
    except PluginError as e:
        flash(str(e), "warning")
        return render_template("error.html"), 402
    except NotImplementedError:
        flash("no oauth support in plugin {}".format(plugin_id), "warning")
        return render_template("error.html"), 402

    # auth check
    created_account = False
    username = data["userPrincipalName"].lower()
    user = (
        db.session.query(User)
        .filter(User.username == username)
        .with_for_update(of=User)
        .first()
    )
    if not user:
        user = _create_user_for_oauth_username(username)
        if user:
            db.session.add(user)
            db.session.commit()
            _event_log(
                "Auto created user of type %s for vendor %s"
                % (user.auth_type, user.vendor.group_id)
            )
            created_account = True
    if not user:
        flash("Failed to log in: no user for {}".format(username), "danger")
        return redirect(url_for("main.route_index_uncached"))
    if not user.auth_type:
        flash("Failed to log in: User account %s is disabled" % user.username, "danger")
        return redirect(url_for("main.route_index_uncached"))
    if user.auth_type != "oauth":
        flash("Failed to log in: Only some accounts can log in using OAuth", "danger")
        return redirect(url_for("main.route_index_uncached"))

    # sync the display name
    if "displayName" in data:
        if user.display_name != data["displayName"]:
            user.display_name = data["displayName"]
            db.session.commit()

    # success
    login_user(user, remember=False)
    g.user = user
    if created_account:
        flash("Logged in, and created account", "info")
    else:
        flash("Logged in", "info")

    # set the access time
    user.atime = datetime.datetime.utcnow()
    db.session.commit()

    return redirect(url_for("main.route_dashboard"))


@bp_main.route("/lvfs/logout")
@login_required
def route_logout() -> Any:
    flash("Logged out from %s" % g.user.username, "info")
    ploader.oauth_logout()
    logout_user()
    return redirect(url_for("main.route_index_uncached"))


@bp_main.route("/lvfs/eventlog")
@bp_main.route("/lvfs/eventlog/<int:start>")
@bp_main.route("/lvfs/eventlog/<int:start>/<int:length>")
@login_required
def route_eventlog(start: int = 0, length: int = 20) -> Any:
    """
    Show an event log of user actions.
    """
    # security check
    if not g.user.check_acl("@view-eventlog"):
        flash("Permission denied: Unable to show event log for non-QA user", "danger")
        return redirect(url_for("main.route_dashboard"))

    # get the page selection correct
    if g.user.check_acl("@admin"):
        eventlog_len = db.session.query(Event.id).count()
    else:
        eventlog_len = (
            db.session.query(Event.id)
            .filter(Event.vendor_id == g.user.vendor_id)
            .count()
        )

    # limit this to keep the UI sane
    if eventlog_len / length > 20:
        eventlog_len = length * 20

    # table contents
    if g.user.check_acl("@admin"):
        events = (
            db.session.query(Event)
            .order_by(Event.id.desc())
            .offset(start)
            .limit(length)
            .all()
        )
    else:
        events = (
            db.session.query(Event)
            .filter(Event.vendor_id == g.user.vendor_id)
            .order_by(Event.id.desc())
            .offset(start)
            .limit(length)
            .all()
        )
    return render_template(
        "eventlog.html",
        events=events,
        category="home",
        start=start,
        page_length=length,
        total_length=eventlog_len,
    )


@bp_main.route("/lvfs/profile")
@login_required
def route_profile() -> Any:
    """
    Allows the normal user to change details about the account,
    """
    return render_template("profile.html", u=g.user)


@bp_main.route("/lvfs/guid", methods=["GET", "POST"])
def route_guid() -> Any:
    """
    Allows the normal user to convert the instance ID to a GUID.
    """

    # no payload
    if request.method != "POST":
        return render_template("guid.html", instance_id=None, guid=None)

    # sanity check
    if "instance_id" not in request.form:
        flash("Input not specified", "warning")
        return redirect(url_for("main.route_guid"))

    # remove the square brackets if the user is pasting from a quirk file
    instance_id = request.form["instance_id"]
    if instance_id.startswith("["):
        instance_id = instance_id[1:]
    if instance_id.endswith("]"):
        instance_id = instance_id[:-1]

    # success
    guid = uuid.uuid5(uuid.NAMESPACE_DNS, instance_id)
    return render_template("guid.html", instance_id=instance_id, guid=guid)


@bp_main.route("/lvfs/uswid", methods=["GET", "POST"])
def route_uswid() -> Any:
    """
    Allows the normal user to generate a uSWID blob.
    """

    # no payload
    if request.method != "POST":
        return render_template("uswid.html", data={"uswid": None})

    # required data
    if "format" not in request.form:
        flash("Format not specified", "warning")
        return redirect(url_for("main.route_uswid"))

    # identity
    data = {}
    identity = uSwidIdentity()
    for key in [
        "tag_id",
        "tag_version",
        "software_name",
        "software_version",
        "product",
        "summary",
        "colloquial_version",
        "revision",
        "edition",
    ]:
        if key in request.form and request.form[key]:
            data[key] = request.form[key]
            setattr(identity, key, data[key])

    # entity
    entity = uSwidEntity()
    for key in [
        "entity_name",
        "entity_regid",
    ]:
        if key in request.form and request.form[key]:
            data[key] = request.form[key]
            setattr(entity, key[7:], data[key])
    identity.add_entity(entity)

    # entity role
    entity.roles.append(uSwidEntityRole.TAG_CREATOR)
    for key, role in [
        ("software_creator", uSwidEntityRole.SOFTWARE_CREATOR),
        ("aggregator", uSwidEntityRole.AGGREGATOR),
        ("distributor", uSwidEntityRole.DISTRIBUTOR),
        ("licensor", uSwidEntityRole.LICENSOR),
        ("maintainer", uSwidEntityRole.MAINTAINER),
    ]:
        if key in request.form and request.form[key]:
            data[key] = request.form[key]
            entity.roles.append(role)

    # link
    if "link_href" in request.form and request.form["link_href"]:
        data["link_href"] = request.form["link_href"]
        identity.add_link(uSwidLink(rel="license", href=data["link_href"]))

    # export
    container = uSwidContainer([identity])
    if request.form["format"] == "uswid":
        resp = Response(
            response=uSwidFormatUswid(compress=True).save(container),
            status=200,
            mimetype="application/octet-stream",
        )
        resp.headers["Content-Disposition"] = "attachment; filename=sbom.uswid"
        return resp
    if request.form["format"] == "coswid":
        resp = Response(
            response=uSwidFormatCoswid().save(container),
            status=200,
            mimetype="application/octet-stream",
        )
        resp.headers["Content-Disposition"] = "attachment; filename=sbom.coswid"
        return resp
    if request.form["format"] == "ini":
        # inline ini
        data["ini"] = uSwidFormatIni().save(container).decode()

    # fallthrough
    return render_template("uswid.html", data=data)


@bp_main.route("/lvfs/metrics")
def route_metrics() -> Any:

    item: Dict[str, int] = {}
    for metric in db.session.query(ClientMetric).order_by(ClientMetric.key):
        item[metric.key] = metric.value

    # unsigned files
    fws = (
        db.session.query(Firmware.firmware_id)
        .filter(Firmware.signed_timestamp == None)
        .order_by(Firmware.firmware_id.asc())
        .all()
    )
    item["UnsignedFirmware"] = len(fws)

    # pending tests
    tests = (
        db.session.query(Test.test_id)
        .filter(Test.started_ts == None)
        .order_by(Test.test_id.asc())
        .all()
    )
    item["PendingTests"] = len(tests)

    dat = json.dumps(item, indent=4, separators=(",", ": "))
    return Response(response=dat, status=200, mimetype="application/json")


@bp_main.route("/lvfs/metrics/db")
@login_required
@admin_login_required
def route_metrics_db() -> Any:

    item: Dict[str, int] = {}
    for schemaname, relname, data_size in db.session.execute(
        "SELECT schemaname, relname, pg_relation_size(relid) FROM "
        "pg_catalog.pg_statio_user_tables ORDER BY pg_relation_size(relid) DESC;"
    ):
        item["{}_{}".format(schemaname, relname)] = data_size
    dat = json.dumps(item, indent=4, separators=(",", ": "))
    return Response(response=dat, status=200, mimetype="application/json")


# old names used on the static site
@bp_main.route("/users.html")
def route_users_html() -> Any:
    return redirect(url_for("docs.route_users"), code=302)


@bp_main.route("/vendors.html")
def route_vendors_html() -> Any:
    return redirect(url_for("docs.route_vendors"), code=302)


@bp_main.route("/developers.html")
def route_developers_html() -> Any:
    return redirect(url_for("docs.route_developers"), code=302)


@bp_main.route("/index.html")
def route_index_html() -> Any:
    return redirect(url_for("main.route_index"), code=302)


@bp_main.route("/lvfs/devicelist")
@cache.cached()
def route_devicelist() -> Any:
    return redirect(url_for("devices.route_list"), code=302)


@bp_main.route("/status")
@bp_main.route("/vendorlist")  # deprecated
@bp_main.route("/lvfs/vendorlist")
def route_vendorlist() -> Any:
    return redirect(url_for("vendors.route_list"), code=302)
