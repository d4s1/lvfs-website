#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2023 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=wrong-import-position

import os
import sys
import unittest

sys.path.append(os.path.realpath("."))

from lvfs.testcase import LvfsTestCase


class LocalTestCase(LvfsTestCase):
    def test_clients(self, _app, client):

        self.login()
        self.upload()
        rv = client.get("/lvfs/clients/acl/")
        assert "127.0.0.1" not in rv.data.decode(), rv.data.decode()

        # blocked user agent
        assert self.checksum_signed_sha256
        rv = client.get(
            "/downloads/"
            + self.checksum_signed_sha256
            + "-hughski-colorhug2-2.0.3.cab",
            environ_base={
                "HTTP_USER_AGENT": "gnome-software/3.34.2 (Linux x86_64 5.3.18-lp152.106-default; "
                "pt-BR; openSUSE Leap 15.2) fwupd/1.2.14"
            },
        )
        assert rv.status_code == 429, rv.status_code

        # create
        rv = client.post(
            "/lvfs/clients/acl/create",
            data={
                "addr": "127.0.0.1",
            },
            follow_redirects=True,
        )
        assert b"Added client ACL" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/clients/acl/")
        assert "127.0.0.1" in rv.data.decode(), rv.data.decode()
        rv = client.post(
            "/lvfs/clients/acl/create",
            data={
                "addr": "127.0.0.1",
            },
            follow_redirects=True,
        )
        assert b"already exists" in rv.data, rv.data.decode()

        # test a download
        assert self.checksum_signed_sha256
        rv = client.get(
            "/downloads/"
            + self.checksum_signed_sha256
            + "-hughski-colorhug2-2.0.3.cab",
            environ_base={"HTTP_USER_AGENT": "fwupd/1.5.0"},
        )
        assert rv.status_code == 429, rv.status_code

        # modify
        rv = client.post(
            "/lvfs/clients/acl/1/modify",
            data={
                "message": "Blocked due to abuse",
            },
            follow_redirects=True,
        )
        assert b"Modified client ACL" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/clients/acl/")
        assert "Blocked due to abuse" in rv.data.decode(), rv.data.decode()

        # show
        rv = client.get("/lvfs/clients/acl/1", follow_redirects=True)
        assert b"Blocked due to abuse" in rv.data, rv.data.decode()

        # delete
        rv = client.post("/lvfs/clients/acl/1/delete", follow_redirects=True)
        assert b"Deleted client ACL" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/clients/")
        assert "127.0.0.1" not in rv.data.decode(), rv.data.decode()

        # automatically add client being dumb
        for _ in range(4):
            self._download_firmware()
        rv = client.get(
            "/downloads/"
            + self.checksum_signed_sha256
            + "-hughski-colorhug2-2.0.3.cab",
            environ_base={"HTTP_USER_AGENT": "fwupd/1.5.0"},
        )
        assert rv.status_code == 429, rv.status_code


if __name__ == "__main__":
    unittest.main()
