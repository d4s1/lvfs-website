#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2023 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

from typing import Any

from flask import Blueprint, request, url_for, redirect, flash, render_template, g
from flask_login import login_required
from sqlalchemy.exc import IntegrityError, NoResultFound

from lvfs import db

from lvfs.util import admin_login_required

from .models import Client, ClientAcl

bp_clients = Blueprint("clients", __name__, template_folder="templates")


@bp_clients.route("/acl/")
@login_required
@admin_login_required
def route_acl_list() -> Any:
    client_acls = (
        db.session.query(ClientAcl).order_by(ClientAcl.client_acl_id.asc()).all()
    )
    return render_template(
        "clients-acl-list.html", category="admin", client_acls=client_acls
    )


@bp_clients.post("/acl/create")
@login_required
@admin_login_required
def route_acl_create() -> Any:

    try:
        cacl = ClientAcl(
            addr=request.form["addr"],
            message="blocked due to overuse, please contact the LVFS admin team if "
            "this download is legitimate",
            user=g.user,
        )
        db.session.add(cacl)
        db.session.commit()
    except KeyError:
        flash("No form data found", "warning")
        return redirect(url_for("clients.route_acl_list"))
    except IntegrityError:
        db.session.rollback()
        flash("Failed to add client ACL: The address already exists", "info")
        return redirect(url_for("clients.route_acl_list"))
    flash("Added client ACL", "info")
    return redirect(url_for("clients.route_acl_show", client_acl_id=cacl.client_acl_id))


@bp_clients.post("/acl/<int:client_acl_id>/delete")
@login_required
@admin_login_required
def route_acl_delete(client_acl_id: int) -> Any:

    # delete
    try:
        db.session.query(ClientAcl).filter(
            ClientAcl.client_acl_id == client_acl_id
        ).delete()
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("ClientAcl still in use", "warning")
        return redirect(url_for("clients.route_acl_show", client_acl_id=client_acl_id))

    flash("Deleted client ACL", "info")
    return redirect(url_for("clients.route_acl_list"))


@bp_clients.post("/acl/<int:client_acl_id>/delete/clients")
@login_required
@admin_login_required
def route_acl_delete_clients(client_acl_id: int) -> Any:

    try:
        cacl = (
            db.session.query(ClientAcl)
            .filter(ClientAcl.client_acl_id == client_acl_id)
            .one()
        )
        if not cacl.addr:
            flash("No client ACL address", "info")
            return redirect(
                url_for("clients.route_acl_show", client_acl_id=client_acl_id)
            )
        db.session.query(Client).filter(Client.addr == cacl.addr).delete()
        db.session.commit()
    except NoResultFound:
        flash("No client ACL found", "info")
        return redirect(url_for("clients.route_acl_list"))
    except IntegrityError:
        db.session.rollback()
        flash("Client in use", "warning")
        return redirect(url_for("clients.route_acl_show", client_acl_id=client_acl_id))

    flash("Deleted clients", "info")
    return redirect(url_for("clients.route_acl_show", client_acl_id=client_acl_id))


@bp_clients.post("/acl/<int:client_acl_id>/modify")
@login_required
@admin_login_required
def route_acl_modify(client_acl_id: int) -> Any:

    try:
        cacl = (
            db.session.query(ClientAcl)
            .filter(ClientAcl.client_acl_id == client_acl_id)
            .with_for_update(of=ClientAcl)
            .one()
        )
    except NoResultFound:
        flash("No client ACL found", "info")
        return redirect(url_for("clients.route_acl_list"))
    for key in ["addr", "message", "comment"]:
        if key in request.form:
            setattr(cacl, key, request.form[key] or None)
    db.session.commit()

    # success
    flash("Modified client ACL", "info")
    return redirect(url_for("clients.route_acl_show", client_acl_id=client_acl_id))


@bp_clients.route("/acl/<int:client_acl_id>")
@login_required
@admin_login_required
def route_acl_show(client_acl_id: int) -> Any:

    try:
        cacl = (
            db.session.query(ClientAcl)
            .filter(ClientAcl.client_acl_id == client_acl_id)
            .one()
        )
    except NoResultFound:
        flash("No client ACL found", "info")
        return redirect(url_for("clients.route_acl_list"))
    clients = db.session.query(Client).filter(Client.addr == cacl.addr).limit(250).all()
    return render_template(
        "clients-acl-details.html",
        category="admin",
        cacl=cacl,
        clients=clients,
    )
