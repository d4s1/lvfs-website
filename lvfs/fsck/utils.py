#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=protected-access,line-too-long

import json
import datetime

from typing import List, Dict
from collections import defaultdict

from lvfs import db
from lvfs.util import _get_datestr_from_datetime
from lvfs.clients.models import Client
from lvfs.components.fsck import _fsck as fsck_components
from lvfs.components.models import Component, ComponentShard
from lvfs.firmware.fsck import _fsck as fsck_firmwares
from lvfs.firmware.models import Firmware
from lvfs.main.models import Event
from lvfs.protocols.fsck import _fsck as fsck_protocols
from lvfs.protocols.models import Protocol
from lvfs.reports.fsck import _fsck as fsck_reports
from lvfs.reports.models import Report
from lvfs.shards.fsck import _fsck as fsck_component_shards
from lvfs.shards.utils import _regenerate_shard_infos
from lvfs.tasks.models import Task
from lvfs.users.fsck import _fsck as fsck_users
from lvfs.users.models import User
from lvfs.users.utils import (
    _user_disable_notify,
    _user_disable_actual,
    _user_add_message_survey,
)


def _fsck_update_descriptions(search: str, replace: str) -> None:

    for md in db.session.query(Component).with_for_update(of=Component):
        for tx in md.descriptions:
            if not tx.value:
                continue
            if tx.value.find(search) != -1:
                tx.value = tx.value.replace(search, replace)
    db.session.commit()


def task_all_update_descriptions(task: Task) -> None:
    # search: str, replace: str
    values = json.loads(task.value)
    _fsck_update_descriptions(values["search"], values["replace"])


def task_all_eventlog_delete(task: Task) -> None:

    values = json.loads(task.value)
    db.session.query(Event).filter(Event.message.contains(values["value"])).delete(
        synchronize_session=False
    )
    db.session.commit()


def task_all(task: Task) -> None:

    try:
        values = json.loads(task.value)
    except TypeError as e:
        task.add_fail("JSON load failed", str(e))
        return
    groups: Dict[str, List[str]] = defaultdict(list)
    for kind in values["kinds"]:
        group, subkind = kind.split("::", maxsplit=2)
        groups[group].append(subkind)

    with task:
        if "Client" in groups:
            datestr = _get_datestr_from_datetime(
                datetime.datetime.today() - datetime.timedelta(days=30)
            )
            db.session.query(Client).filter(Client.datestr > datestr).filter(
                Client.user_agent == "Mozilla/5.0"
            ).delete()
            db.session.commit()
        if "User" in groups:
            _user_disable_notify(task)
            _user_disable_actual(task)
            _user_add_message_survey(task)
            task.percentage_current = 0
            task.percentage_total = db.session.query(User).count()
            for (user_id,) in db.session.query(User.user_id).order_by(
                User.user_id.asc()
            ):
                task.percentage_current += 1
                if task.percentage_current % 50 == 0:
                    task.status = "User {}".format(user_id)
                    db.session.commit()
                user = (
                    db.session.query(User)
                    .filter(User.user_id == user_id)
                    .with_for_update(of=User)
                    .first()
                )
                if not user:
                    continue
                fsck_users(user, task, groups["User"])
                if task.ended_ts:
                    break
            db.session.commit()
        if "Protocol" in groups:
            task.percentage_current = 0
            task.percentage_total = db.session.query(Protocol).count()
            for protocol in (
                db.session.query(Protocol)
                .order_by(Protocol.protocol_id)
                .with_for_update(of=Protocol)
            ):
                task.percentage_current += 1
                if task.percentage_current % 5 == 0:
                    task.status = "Protocol {}".format(protocol.protocol_id)
                    db.session.commit()
                fsck_protocols(protocol, task, groups["Protocol"])
                if task.ended_ts:
                    break
            db.session.commit()
        if "Report" in groups:
            task.percentage_current = 0
            task.percentage_total = db.session.query(Report).count()
            for (report_id,) in db.session.query(Report.report_id).order_by(
                Report.report_id.asc()
            ):
                task.percentage_current += 1
                if task.percentage_current % 500 == 0:
                    task.status = "Report {}".format(report_id)
                    db.session.commit()
                report = (
                    db.session.query(Report)
                    .filter(Report.report_id == report_id)
                    .with_for_update(of=Report)
                    .first()
                )
                if not report:
                    continue
                fsck_reports(report, task, groups["Report"])
                if task.ended_ts:
                    break
            db.session.commit()
        if "Firmware" in groups:
            task.percentage_current = 0
            task.percentage_total = db.session.query(Firmware).count()
            for (firmware_id,) in db.session.query(Firmware.firmware_id).order_by(
                Firmware.firmware_id.asc()
            ):
                task.percentage_current += 1
                if task.percentage_current % 50 == 0:
                    task.status = "Firmware {}".format(firmware_id)
                    db.session.commit()
                fw = (
                    db.session.query(Firmware)
                    .filter(Firmware.firmware_id == firmware_id)
                    .with_for_update(of=Firmware)
                    .first()
                )
                if not fw:
                    continue
                fsck_firmwares(fw, task, groups["Firmware"])
                if task.ended_ts:
                    break
                db.session.commit()
        if "Component" in groups:
            task.percentage_current = 0
            task.percentage_total = db.session.query(Component).count()
            for (component_id,) in db.session.query(Component.component_id).order_by(
                Component.component_id.asc()
            ):
                task.percentage_current += 1
                if task.percentage_current % 50 == 0:
                    task.status = "Component {}".format(component_id)
                    db.session.commit()
                md = (
                    db.session.query(Component)
                    .filter(Component.component_id == component_id)
                    .with_for_update(of=Component)
                    .first()
                )
                if not md:
                    continue
                fsck_components(md, task, groups["Component"])
                if task.ended_ts:
                    break
                db.session.commit()
        if "ComponentShard" in groups:
            task.percentage_current = 0
            task.percentage_total = db.session.query(ComponentShard).count()
            for (component_shard_id,) in db.session.query(
                ComponentShard.component_shard_id
            ).order_by(ComponentShard.component_shard_id.asc()):
                task.percentage_current += 1
                if task.percentage_current % 50 == 0:
                    task.status = "ComponentShard {}".format(component_shard_id)
                    db.session.commit()
                shard = (
                    db.session.query(ComponentShard)
                    .filter(ComponentShard.component_shard_id == component_shard_id)
                    .with_for_update(of=ComponentShard)
                    .first()
                )
                if not shard:
                    continue
                fsck_component_shards(shard, task, groups["ComponentShard"])
                if task.ended_ts:
                    break
            if "dedupe" in groups["ComponentShard"]:
                _regenerate_shard_infos(task)
            db.session.commit()
