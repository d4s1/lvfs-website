Dear {{ user.display_name }},

We've written a short survey for people using the LVFS, and now you have used the LVFS for a while we'd love your input.

It only takes ~5 minutes to complete and all feedback helps us make the LVFS easier to use.

Anytime in the next 30 days please click this link to complete the survey: https://forms.gle/jhyQUkdXiq8biquh9

Regards,

The LVFS admins
