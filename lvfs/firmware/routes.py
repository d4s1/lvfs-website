#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-many-lines

import os
import datetime
import json
import hashlib

from typing import List, Dict, Optional, Any

import dateutil.parser

from flask import Blueprint, request, url_for, redirect, render_template, flash, g
from flask_login import login_required

from sqlalchemy import func, or_
from sqlalchemy.orm import joinedload
from sqlalchemy.exc import NoResultFound


from lvfs import db, csrf, cache

from lvfs.analytics.models import AnalyticFirmware
from lvfs.components.models import (
    Component,
    ComponentRequirement,
    ComponentShard,
    ComponentShardChecksum,
)
from lvfs.clients.models import Client
from lvfs.metadata.models import Remote
from lvfs.reports.models import Report
from lvfs.util import _get_datestr_from_datetime
from lvfs.util import _get_chart_labels_months, _get_chart_labels_days
from lvfs.vendors.models import Vendor
from lvfs.tasks.models import Task
from lvfs.reports.routes import route_report

from .models import (
    Firmware,
    FirmwareAction,
    FirmwareAsset,
    FirmwareEvent,
    FirmwareEventKind,
    FirmwareRevision,
    FirmwareVendor,
)
from .utils import _firmware_delete

bp_firmware = Blueprint("firmware", __name__, template_folder="templates")


@bp_firmware.route("/")
@bp_firmware.route("/state/<state>")
@login_required
def route_firmware(state: Optional[str] = None) -> Any:
    """
    Show all firmware uploaded by this user or vendor.
    """
    # pre-filter by user ID or vendor
    if g.user.check_acl("@analyst") or g.user.check_acl("@qa"):
        stmt = db.session.query(Firmware).filter(
            or_(
                Firmware.vendor == g.user.vendor,
                Firmware.odm_vendors.contains(g.user.vendor),
            )
        )
    else:
        stmt = db.session.query(Firmware).filter(Firmware.user_id == g.user.user_id)
    if not state:
        remote = None
    elif state == "embargo":
        remote = g.user.vendor.remote
        remote_ids: List[int] = []
        for state_tmp in ["private", "testing", "stable", "deleted"]:
            remote_ids.append(
                db.session.query(Remote)
                .filter(Remote.name == state_tmp)
                .one()
                .remote_id
            )
        stmt = stmt.filter(Firmware.remote_id.not_in(remote_ids))
    elif state in ["private", "testing", "stable", "deleted"]:
        remote = db.session.query(Remote).filter(Remote.name == state).one()
        stmt = stmt.filter(Firmware.remote_id == remote.remote_id)
    else:
        flash("No state of {}".format(state), "warning")
        return redirect(url_for("firmware.route_firmware"))
    stmt = stmt.options(joinedload(Firmware.tests))
    fws = stmt.order_by(Firmware.timestamp.desc()).all()
    return render_template(
        "firmware-search.html", category="firmware", state=state, remote=remote, fws=fws
    )


@bp_firmware.post("/report")
@csrf.exempt
def route_report_for_compat() -> Any:
    return route_report()


@bp_firmware.route("/events")
@login_required
def route_events() -> Any:
    """
    Show what firmware has changed state recently.
    """
    # pre-filter by user ID or vendor

    if g.user.check_acl("@admin"):
        stmt = db.session.query(FirmwareEvent)
    elif g.user.check_acl("@qa"):
        stmt = (
            db.session.query(FirmwareEvent)
            .join(Firmware)
            .filter(
                or_(
                    Firmware.vendor == g.user.vendor,
                    Firmware.odm_vendors.contains(g.user.vendor),
                )
            )
        )
    else:
        stmt = db.session.query(FirmwareEvent).filter(
            FirmwareEvent.user_id == g.user.user_id
        )
    stmt = stmt.options(joinedload(FirmwareEvent.fw))
    fwevts = stmt.order_by(FirmwareEvent.timestamp.desc()).limit(250).all()
    return render_template("firmware-events.html", category="firmware", fwevts=fwevts)


@bp_firmware.route("/user")
@login_required
def route_user() -> Any:
    """
    Show all firmware uploaded by this user.
    """
    # pre-filter by user ID or vendor
    stmt = db.session.query(Firmware).filter(Firmware.user_id == g.user.user_id)
    stmt = stmt.options(joinedload(Firmware.tests))
    fws = stmt.order_by(Firmware.timestamp.desc()).all()
    return render_template("firmware-search.html", category="firmware", fws=fws)


@bp_firmware.route("/new")
@bp_firmware.route("/new/<int:limit>")
@cache.cached()
def route_new(limit: int = 50) -> Any:

    fwevs_public: Dict[int, FirmwareEvent] = {}
    for evt in (
        db.session.query(FirmwareEvent)
        .filter(FirmwareEvent.remote_id == 1)  # stable
        .join(Firmware)
        .join(Remote)
        .filter(Remote.remote_id == 1)
        .order_by(FirmwareEvent.timestamp.desc())
        .limit(limit)
        .options(joinedload(FirmwareEvent.fw))
    ):
        if evt.fw.firmware_id not in fwevs_public:
            fwevs_public[evt.fw.firmware_id] = evt
    return render_template(
        "firmware-new.html",
        category="firmware",
        fwevs=fwevs_public.values(),
        limit=limit,
    )


@bp_firmware.post("/<int:firmware_id>/undelete")
@login_required
def route_undelete(firmware_id: int) -> Any:
    """Undelete a firmware entry and also restore the file from disk"""

    # check firmware exists in database
    try:
        fw = (
            db.session.query(Firmware)
            .filter(Firmware.firmware_id == firmware_id)
            .with_for_update(of=Firmware)
            .one()
        )
    except NoResultFound:
        flash("No firmware {} exists".format(firmware_id), "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not fw.check_acl("@undelete"):
        flash(
            "Permission denied: Insufficient permissions to undelete firmware", "danger"
        )
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))

    # find private remote
    try:
        remote = db.session.query(Remote).filter(Remote.name == "private").one()
    except NoResultFound:
        flash("No private remote", "warning")
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))

    # put back to the private state
    fw.events.append(
        FirmwareEvent(
            kind=FirmwareEventKind.UNDELETED.value,
            remote_old=fw.remote,
            remote=remote,
            user=g.user,
        )
    )
    fw.remote_id = remote.remote_id
    db.session.commit()

    flash("Firmware undeleted", "info")
    return redirect(url_for("firmware.route_show", firmware_id=firmware_id))


@bp_firmware.post("/<int:firmware_id>/delete")
@login_required
def route_delete(firmware_id: int) -> Any:
    """Delete a firmware entry and also delete the file from disk"""

    # check firmware exists in database
    try:
        fw = (
            db.session.query(Firmware)
            .filter(Firmware.firmware_id == firmware_id)
            .with_for_update(of=Firmware)
            .one()
        )
    except NoResultFound:
        flash("No firmware {} exists".format(firmware_id), "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not fw.check_acl("@delete"):
        flash(
            "Permission denied: Insufficient permissions to delete firmware", "danger"
        )
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))

    # delete firmware
    _firmware_delete(fw, task=Task(user=g.user))
    db.session.commit()

    flash("Firmware deleted", "info")
    return redirect(url_for("firmware.route_firmware"))


@bp_firmware.post("/<int:firmware_id>/nuke")
@login_required
def route_nuke(firmware_id: int) -> Any:
    """Delete a firmware entry and also delete the file from disk"""

    # check firmware exists in database
    try:
        fw = (
            db.session.query(Firmware)
            .filter(Firmware.firmware_id == firmware_id)
            .with_for_update(of=Firmware)
            .one()
        )
    except NoResultFound:
        flash("No firmware {} exists".format(firmware_id), "danger")
        return redirect(url_for("firmware.route_firmware"))

    # firmware is not deleted yet
    if not fw.is_deleted:
        flash("Cannot nuke file not yet deleted", "danger")
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))

    # security check
    if not fw.check_acl("@nuke"):
        flash("Permission denied: Insufficient permissions to nuke firmware", "danger")
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))

    # async
    db.session.add(
        Task(
            value=json.dumps({"id": fw.firmware_id}),
            caller=__name__,
            user=g.user,
            function="lvfs.firmware.utils.task_nuke",
        )
    )
    db.session.commit()

    flash("Firmware will be nuked", "info")
    return redirect(url_for("firmware.route_firmware"))


@bp_firmware.post("/<int:firmware_id>/derestrict")
@login_required
def route_derestrict(firmware_id: int) -> Any:
    """Derestric a firmware embargo date"""

    # check firmware exists in database
    try:
        fw = (
            db.session.query(Firmware)
            .filter(Firmware.firmware_id == firmware_id)
            .with_for_update(of=Firmware)
            .one()
        )
    except NoResultFound:
        flash("No firmware {} exists".format(firmware_id), "danger")
        return redirect(url_for("firmware.route_firmware"))

    # firmware is not deleted yet
    if not fw.is_restricted:
        flash("Cannot derestrict file with no restriction", "warning")
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))

    # security check
    if not fw.check_acl("@derestrict"):
        flash(
            "Permission denied: Insufficient permissions to remove restriction",
            "danger",
        )
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))

    # all done
    fw.restricted_ts = datetime.datetime.utcnow()
    db.session.commit()

    flash("Firmware derestricted", "info")
    return redirect(url_for("firmware.route_firmware"))


@bp_firmware.post("/<int:firmware_id>/resign")
@login_required
def route_resign(firmware_id: int) -> Any:
    """Re-sign a firmware archive"""

    # check firmware exists in database
    try:
        fw = (
            db.session.query(Firmware)
            .filter(Firmware.firmware_id == firmware_id)
            .with_for_update(of=Firmware)
            .one()
        )
    except NoResultFound:
        flash("No firmware {} exists".format(firmware_id), "danger")
        return redirect(url_for("firmware.route_firmware"))

    # firmware is not signed yet
    if not g.user.check_acl("@admin") and not fw.signed_timestamp:
        flash("Cannot resign unsigned file", "danger")
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))
    if not fw.check_acl("@resign"):
        flash("Permission denied: Cannot resign {}".format(firmware_id), "danger")
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))

    # asynchronously sign
    db.session.add(
        Task(
            value=json.dumps({"id": fw.firmware_id}),
            caller=__name__,
            user=g.user,
            url=url_for("firmware.route_show", firmware_id=firmware_id),
            function="lvfs.firmware.utils.task_sign_fw",
        )
    )
    db.session.commit()

    flash("Firmware will be re-signed soon", "info")
    return redirect(url_for("firmware.route_show", firmware_id=firmware_id))


@bp_firmware.route(
    "/<int:firmware_id>/revision/<int:firmware_revision_id>/immutable/<int:value>",
    methods=["POST"],
)
@login_required
def route_revision_modify(
    firmware_id: int, firmware_revision_id: int, value: int
) -> Any:
    """
    Promote or demote a firmware file from one target to another,
    for example from testing to stable, or stable to testing.
    """

    # check revision exists in database
    try:
        fwrev = (
            db.session.query(FirmwareRevision)
            .filter(FirmwareRevision.firmware_revision_id == firmware_revision_id)
            .join(Firmware)
            .filter(Firmware.firmware_id == firmware_id)
            .with_for_update(of=[Firmware, FirmwareRevision])
            .one()
        )
    except NoResultFound:
        flash("No firmware revision {} exists".format(firmware_id), "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not fwrev.fw.check_acl("@delete"):
        flash("Permission denied: No access to {}".format(firmware_id), "danger")
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))

    # change value
    fwrev.immutable = bool(value)
    db.session.commit()
    flash("Modified revision", "info")
    return redirect(url_for("firmware.route_target", firmware_id=firmware_id))


@bp_firmware.post("/<int:firmware_id>/promote/<target>")
@login_required
def route_promote(firmware_id: int, target: str) -> Any:
    """
    Promote or demote a firmware file from one target to another,
    for example from testing to stable, or stable to testing.
    """

    # check valid
    if target not in ["stable", "testing", "private", "embargo"]:
        flash("Target {} invalid".format(target), "warning")
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))

    # check firmware exists in database
    try:
        fw = (
            db.session.query(Firmware)
            .filter(Firmware.firmware_id == firmware_id)
            .with_for_update(of=Firmware)
            .one()
        )
    except NoResultFound:
        flash("No firmware {} exists".format(firmware_id), "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not fw.check_acl("@promote-" + target):
        flash("Permission denied: No QA access to {}".format(firmware_id), "danger")
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))

    # async
    db.session.add(
        Task(
            value=json.dumps({"id": firmware_id, "target": target}),
            caller=__name__,
            user=g.user,
            function="lvfs.firmware.utils.task_promote_fw",
        )
    )
    db.session.commit()

    flash("Moving firmware…", "info")
    return redirect(url_for("firmware.route_target", firmware_id=firmware_id))


@bp_firmware.route("/<int:firmware_id>/components")
@login_required
def route_components(firmware_id: int) -> Any:

    # get details about the firmware
    try:
        fw = (
            db.session.query(Firmware).filter(Firmware.firmware_id == firmware_id).one()
        )
    except NoResultFound:
        flash("No firmware {} exists".format(firmware_id), "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not fw.check_acl("@view"):
        flash(
            "Permission denied: Insufficient permissions to view components", "danger"
        )
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))

    return render_template("firmware-components.html", category="firmware", fw=fw)


@bp_firmware.route("/<int:firmware_id>/limits")
@login_required
def route_limits(firmware_id: int) -> Any:

    # get details about the firmware
    try:
        fw = (
            db.session.query(Firmware).filter(Firmware.firmware_id == firmware_id).one()
        )
    except NoResultFound:
        flash("No firmware matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not fw.check_acl("@view"):
        flash("Permission denied: Insufficient permissions to view limits", "danger")
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))

    return render_template("firmware-limits.html", category="firmware", fw=fw)


@bp_firmware.post("/<int:firmware_id>/action/create")
@login_required
def route_action_create(firmware_id: int) -> Any:
    """Creates a firmware action"""

    # get details about the firmware
    try:
        fw = (
            db.session.query(Firmware)
            .filter(Firmware.firmware_id == firmware_id)
            .with_for_update(of=Firmware)
            .one()
        )
    except NoResultFound:
        flash("No firmware matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not fw.check_acl("@modify"):
        flash("Permission denied: Insufficient permissions to add action", "danger")
        return redirect(url_for("firmware.route_target", firmware_id=fw.firmware_id))
    try:
        remote = (
            db.session.query(Remote)
            .filter(Remote.remote_id == request.form["remote_id"])
            .one()
        )
    except NoResultFound:
        flash("No remote matched!", "danger")
        return redirect(url_for("firmware.route_target", firmware_id=fw.firmware_id))

    # security check
    if not fw.check_acl("@promote-" + remote.key):
        flash(
            "Permission denied: Cannot promote {} to {}".format(
                firmware_id, remote.key
            ),
            "danger",
        )
        return redirect(url_for("firmware.route_target", firmware_id=fw.firmware_id))

    # check not a duplicate
    for action in fw.actions:
        if remote == action.remote:
            flash(
                "Already added an action for the {} target".format(remote.name), "info"
            )
            return redirect(
                url_for("firmware.route_target", firmware_id=fw.firmware_id)
            )

    # parse date
    try:
        mtime = dateutil.parser.parse(request.form["mtime"]).replace(tzinfo=None)
    except KeyError:
        flash("Invalid date specified", "warning")
        return redirect(url_for("firmware.route_target", firmware_id=fw.firmware_id))
    if mtime < datetime.datetime.utcnow():
        flash("Date cannot specified to be before today", "warning")
        return redirect(url_for("firmware.route_target", firmware_id=fw.firmware_id))

    # add restriction
    fw.actions.append(FirmwareAction(mtime=mtime, remote=remote, user=g.user))
    db.session.commit()
    flash("Added action", "info")

    return redirect(url_for("firmware.route_target", firmware_id=fw.firmware_id))


@bp_firmware.post("/<int:firmware_id>/action/<int:firmware_action_id>/delete")
@login_required
def route_action_delete(firmware_id: int, firmware_action_id: int) -> Any:
    """Deletes a firmware action"""

    # get details about the firmware
    try:
        action = (
            db.session.query(FirmwareAction)
            .filter(FirmwareAction.firmware_action_id == firmware_action_id)
            .join(Firmware)
            .filter(Firmware.firmware_id == firmware_id)
            .with_for_update(of=[Firmware, FirmwareAction])
            .one()
        )
    except NoResultFound:
        flash("No firmware action matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not action.fw.check_acl("@modify"):
        flash("Permission denied: Insufficient permissions to modify", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # success
    db.session.delete(action)
    db.session.commit()
    flash("Deleted action", "info")

    return redirect(url_for("firmware.route_target", firmware_id=firmware_id))


@bp_firmware.post("/<int:firmware_id>/modify")
@login_required
def route_modify(firmware_id: int) -> Any:
    """Modifies the firmware properties"""

    # find firmware
    try:
        fw = (
            db.session.query(Firmware)
            .filter(Firmware.firmware_id == firmware_id)
            .with_for_update(of=Firmware)
            .one()
        )
    except NoResultFound:
        flash("No firmware {}".format(firmware_id), "warning")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not fw.check_acl("@modify"):
        flash(
            "Permission denied: Insufficient permissions to modify firmware", "danger"
        )
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))

    # set new metadata values
    if "failure_minimum" in request.form:
        fw.failure_minimum = request.form["failure_minimum"]
    if "failure_percentage" in request.form:
        fw.failure_percentage = request.form["failure_percentage"]

    # modify
    db.session.commit()
    flash("Firmware updated", "info")
    return redirect(url_for("firmware.route_limits", firmware_id=firmware_id))


@bp_firmware.route("/<int:firmware_id>/affiliation")
@login_required
def route_affiliation(firmware_id: int) -> Any:

    # get details about the firmware
    try:
        fw = (
            db.session.query(Firmware).filter(Firmware.firmware_id == firmware_id).one()
        )
    except NoResultFound:
        flash("No firmware matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not fw.check_acl("@modify-affiliation"):
        flash(
            "Permission denied: Insufficient permissions to modify affiliations",
            "danger",
        )
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))

    # add other vendors
    if g.user.check_acl("@admin"):
        vendors: List[Vendor] = []
        for v in db.session.query(Vendor).order_by(Vendor.display_name):
            if not v.is_account_holder:
                continue
            vendors.append(v)
    else:
        vendors = [g.user.vendor]
        for aff in fw.vendor.affiliations_for:
            vendors.append(aff.vendor)

    return render_template(
        "firmware-affiliation.html", category="firmware", fw=fw, vendors=vendors
    )


@bp_firmware.post("/<int:firmware_id>/affiliation/change")
@login_required
def route_affiliation_change(firmware_id: int) -> Any:
    """Changes the assigned vendor ID for the firmware"""

    # find firmware
    try:
        fw = (
            db.session.query(Firmware)
            .filter(Firmware.firmware_id == firmware_id)
            .join(Remote)
            .with_for_update(of=[Firmware, Remote])
            .one()
        )
    except NoResultFound:
        flash("No firmware matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not fw.check_acl("@modify-affiliation"):
        flash(
            "Permission denied: Insufficient permissions to change affiliation",
            "danger",
        )
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))

    try:
        vendor_id = int(request.form["vendor_id"])
    except KeyError:
        flash("No vendor ID specified", "warning")
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))
    if vendor_id == fw.vendor_id:
        flash("No affiliation change required", "info")
        return redirect(
            url_for("firmware.route_affiliation", firmware_id=fw.firmware_id)
        )
    if (
        not g.user.check_acl("@admin")
        and not g.user.vendor.is_affiliate_for(vendor_id)
        and vendor_id != g.user.vendor_id
    ):
        flash(
            "Insufficient permissions to change affiliation to {}".format(vendor_id),
            "danger",
        )
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))
    old_vendor = fw.vendor

    try:
        new_vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .join(Remote)
            .with_for_update(of=Remote)
            .one()
        )
    except NoResultFound:
        flash("No vendor matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))
    fw.vendor = new_vendor
    db.session.commit()

    # remove the ODM access if now the OEM
    try:
        fw.odm_vendors.remove(fw.vendor)
        db.session.commit()
    except ValueError:
        pass

    # add the new ODM automatically; the OEM can always remove access if desired
    if old_vendor not in fw.odm_vendors:
        fw.odm_vendors_map.append(FirmwareVendor(vendor=old_vendor, user=g.user))
        db.session.commit()

    # do we need to regenerate remotes?
    if fw.remote.name.startswith("embargo"):
        fw.events.append(
            FirmwareEvent(
                kind=FirmwareEventKind.AFFILIATION_CHANGE.value,
                remote_old=fw.remote,
                remote=fw.vendor.remote,
                user=g.user,
            )
        )
        fw.remote = fw.vendor.remote
        db.session.commit()

    flash("Changed firmware vendor", "info")

    # asynchronously sign firmware (for LICENSE.txt) which also does the metadata
    db.session.add(
        Task(
            value=json.dumps({"id": fw.firmware_id}),
            caller=__name__,
            user=g.user,
            url=url_for("firmware.route_show", firmware_id=fw.firmware_id),
            function="lvfs.firmware.utils.task_sign_fw",
        )
    )
    db.session.commit()

    return redirect(url_for("firmware.route_affiliation", firmware_id=fw.firmware_id))


@bp_firmware.post("/vendor/<int:firmware_vendor_id>/delete")
@login_required
def route_vendor_delete(firmware_vendor_id: int) -> Any:

    # get details about the firmware
    try:
        fvm = (
            db.session.query(FirmwareVendor)
            .filter(FirmwareVendor.firmware_vendor_id == firmware_vendor_id)
            .join(Firmware)
            .with_for_update(of=[Firmware, FirmwareVendor])
            .one()
        )
    except NoResultFound:
        flash("No firmware vendor matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not fvm.check_acl("@delete"):
        flash("Permission denied: Insufficient permissions to delete vendor", "danger")
        return redirect(url_for("firmware.route_firmware"))

    firmware_id = fvm.fw.firmware_id
    db.session.delete(fvm)
    db.session.commit()
    flash("Deleted vendor", "info")

    # asynchronously sign
    db.session.add(
        Task(
            value=json.dumps({"id": fvm.fw.remote.remote_id}),
            caller=__name__,
            user=g.user,
            function="lvfs.metadata.utils.task_regenerate_remote",
        )
    )
    db.session.commit()

    return redirect(url_for("firmware.route_affiliation", firmware_id=firmware_id))


@bp_firmware.post("/vendor/create")
@login_required
def route_vendor_create() -> Any:

    # get details about the firmware
    try:
        fw = (
            db.session.query(Firmware)
            .filter(Firmware.firmware_id == request.form["firmware_id"])
            .with_for_update(of=Firmware)
            .one()
        )
    except KeyError:
        flash("No form data found!", "warning")
        return redirect(url_for("firmware.route_firmware"))
    except NoResultFound:
        flash("No firmware matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not fw.check_acl("@add-odm-vendor"):
        flash("Permission denied: Insufficient permissions to add ODM vendor", "danger")
        return redirect(
            url_for("firmware.route_affiliation", firmware_id=fw.firmware_id)
        )
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == request.form["vendor_id"])
            .one()
        )
    except KeyError:
        flash("No form data found!", "warning")
        return redirect(url_for("firmware.route_firmware"))
    except NoResultFound:
        flash("No vendor matched!", "danger")
        return redirect(
            url_for("firmware.route_affiliation", firmware_id=fw.firmware_id)
        )
    if not fw.vendor.is_affiliate(vendor.vendor_id):
        flash("Not an affiliate!", "danger")
        return redirect(
            url_for("firmware.route_affiliation", firmware_id=fw.firmware_id)
        )

    # check not a duplicate
    if vendor in fw.odm_vendors:
        flash("Already added that vendor", "info")
        return redirect(
            url_for("firmware.route_affiliation", firmware_id=fw.firmware_id)
        )

    # add restriction
    fw.odm_vendors_map.append(FirmwareVendor(vendor=vendor, user=g.user))
    db.session.commit()
    flash("Added vendor", "info")

    # asynchronously sign
    db.session.add(
        Task(
            value=json.dumps({"id": fw.remote.remote_id}),
            caller=__name__,
            user=g.user,
            function="lvfs.metadata.utils.task_regenerate_remote",
        )
    )
    db.session.commit()

    return redirect(url_for("firmware.route_affiliation", firmware_id=fw.firmware_id))


@bp_firmware.route("/<int:firmware_id>/problems")
@login_required
def route_problems(firmware_id: int) -> Any:

    # get details about the firmware
    try:
        fw = (
            db.session.query(Firmware).filter(Firmware.firmware_id == firmware_id).one()
        )
    except NoResultFound:
        flash("No firmware matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not fw.check_acl("@view"):
        flash(
            "Permission denied: Insufficient permissions to view components", "danger"
        )
        return redirect(url_for("firmware.route_firmware"))

    return render_template("firmware-problems.html", category="firmware", fw=fw)


@bp_firmware.route("/<int:firmware_id>/target")
@login_required
def route_target(firmware_id: int) -> Any:

    # get details about the firmware
    try:
        fw = (
            db.session.query(Firmware).filter(Firmware.firmware_id == firmware_id).one()
        )
    except NoResultFound:
        flash("No firmware matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not fw.check_acl("@view"):
        flash("Permission denied: Insufficient permissions to view firmware", "danger")
        return redirect(url_for("firmware.route_firmware"))

    return render_template("firmware-target.html", category="firmware", fw=fw)


@bp_firmware.route("/<int:firmware_id>")
@login_required
def route_show(firmware_id: int) -> Any:
    """Show firmware information"""

    # get details about the firmware
    try:
        fw = (
            db.session.query(Firmware).filter(Firmware.firmware_id == firmware_id).one()
        )
    except NoResultFound:
        flash("No firmware matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not fw.check_acl("@view"):
        flash("Permission denied: Insufficient permissions to view firmware", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # get data for the last month or year
    graph_data: List[int] = []
    graph_labels: List[str] = []
    if fw.check_acl("@view-analytics") and not fw.do_not_track:
        if fw.timestamp.replace(
            tzinfo=None
        ) > datetime.datetime.today() - datetime.timedelta(days=30):
            datestr = _get_datestr_from_datetime(
                datetime.datetime.now() - datetime.timedelta(days=31)
            )
            data = (
                db.session.query(AnalyticFirmware.cnt)
                .filter(AnalyticFirmware.firmware_id == fw.firmware_id)
                .filter(AnalyticFirmware.datestr > datestr)
                .order_by(AnalyticFirmware.datestr.desc())
                .all()
            )
            graph_data = [r[0] for r in data]
            graph_data = graph_data[::-1]
            graph_labels = _get_chart_labels_days(limit=len(data))[::-1]
        else:
            datestr = _get_datestr_from_datetime(
                datetime.datetime.now() - datetime.timedelta(days=360)
            )
            data = (
                db.session.query(AnalyticFirmware.cnt)
                .filter(AnalyticFirmware.firmware_id == fw.firmware_id)
                .filter(AnalyticFirmware.datestr > datestr)
                .order_by(AnalyticFirmware.datestr.desc())
                .all()
            )
            # put in month-sized buckets
            for _ in range(12):
                graph_data.append(0)
            cnt = 0
            for res in data:
                try:
                    graph_data[int(cnt / 30)] += res[0]
                except IndexError:
                    pass
                cnt += 1
            graph_data = graph_data[::-1]
            graph_labels = _get_chart_labels_months()[::-1]

    return render_template(
        "firmware-details.html",
        category="firmware",
        fw=fw,
        graph_data=graph_data,
        graph_labels=graph_labels,
    )


@bp_firmware.route("/<int:firmware_id>/analytics")
@bp_firmware.route("/<int:firmware_id>/analytics/clients")
@login_required
def route_analytics_clients(firmware_id: int) -> Any:
    """Show firmware clients information"""

    # get details about the firmware
    try:
        fw = (
            db.session.query(Firmware).filter(Firmware.firmware_id == firmware_id).one()
        )
    except NoResultFound:
        flash("No firmware matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not fw.check_acl("@view-analytics"):
        flash("Permission denied: Insufficient permissions to view analytics", "danger")
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))

    # group by country of origin
    now = datetime.datetime.today() - datetime.timedelta(days=30)
    datestr = _get_datestr_from_datetime(now)
    data: List[int] = []
    labels: List[str] = []
    for (cc, cnt) in (
        db.session.query(Client.country_code, func.count("*"))
        .filter(Client.firmware_id == fw.firmware_id)
        .filter(Client.datestr > datestr)
        .filter(Client.country_code != None)
        .group_by(Client.country_code)
        .order_by(func.count("*").desc())
        .limit(10)
    ):
        labels.append(cc)
        data.append(cnt)

    # last 10 downloads
    clients = (
        db.session.query(Client)
        .filter(Client.firmware_id == fw.firmware_id)
        .order_by(Client.id.desc(), Client.timestamp.desc())
        .limit(10)
        .all()
    )
    return render_template(
        "firmware-analytics-clients.html",
        category="firmware",
        fw=fw,
        data=data,
        labels=labels,
        clients=clients,
    )


@bp_firmware.route("/<int:firmware_id>/analytics/reports")
@bp_firmware.route("/<int:firmware_id>/analytics/reports/<int:state>")
@bp_firmware.route("/<int:firmware_id>/analytics/reports/<int:state>/<int:limit>")
@login_required
def route_analytics_reports(
    firmware_id: int, state: Optional[str] = None, limit: int = 100
) -> Any:
    """Show firmware clients information"""

    # get reports about the firmware
    try:
        fw = (
            db.session.query(Firmware).filter(Firmware.firmware_id == firmware_id).one()
        )
    except NoResultFound:
        flash("No firmware matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not fw.check_acl("@view-analytics"):
        flash("Permission denied: Insufficient permissions to view analytics", "danger")
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))
    if state:
        reports = (
            db.session.query(Report)
            .filter(Report.firmware_id == firmware_id)
            .filter(Report.state == state)
            .order_by(Report.timestamp.desc())
            .limit(limit)
            .all()
        )
    else:
        reports = (
            db.session.query(Report)
            .filter(Report.firmware_id == firmware_id)
            .order_by(Report.timestamp.desc())
            .limit(limit)
            .all()
        )
    return render_template(
        "firmware-analytics-reports.html",
        category="firmware",
        fw=fw,
        state=state,
        reports=reports,
    )


@bp_firmware.route("/<int:firmware_id>/tests")
@login_required
def route_tests(firmware_id: int) -> Any:

    # get details about the firmware
    try:
        fw = (
            db.session.query(Firmware).filter(Firmware.firmware_id == firmware_id).one()
        )
    except NoResultFound:
        flash("No firmware matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not fw.check_acl("@view"):
        flash("Permission denied: Insufficient permissions to view firmware", "danger")
        return redirect(url_for("firmware.route_firmware"))

    return render_template("firmware-tests.html", category="firmware", fw=fw)


@bp_firmware.route("/shard/search/<kind>/<value>")
@login_required
def route_shard_search(kind: str, value: str) -> Any:
    """
    Show firmware with shards that match the value
    """

    if kind == "guid":
        fws = (
            db.session.query(Firmware)
            .join(Component)
            .join(ComponentShard)
            .filter(ComponentShard.guid == value)
            .order_by(Firmware.firmware_id.desc())
            .all()
        )
    elif kind == "checksum":
        fws = (
            db.session.query(Firmware)
            .join(Component)
            .join(ComponentShard)
            .join(ComponentShardChecksum)
            .filter(ComponentShardChecksum.value == value)
            .order_by(Firmware.firmware_id.desc())
            .all()
        )
    else:
        flash("Invalid kind!", "warning")
        return redirect(url_for("firmware.route_firmware"))
    if not fws:
        flash("No shards matched!", "warning")
        return redirect(url_for("firmware.route_firmware"))

    # filter by ACL
    fws_safe: List[Firmware] = []
    for fw in fws:
        if fw.check_acl("@view"):
            fws_safe.append(fw)

    return render_template(
        "firmware-search.html",
        category="firmware",
        state="search",
        remote=None,
        fws=fws_safe,
    )


@bp_firmware.post("/<int:firmware_id>/generic/create")
@login_required
def route_generic_create(firmware_id: int) -> Any:
    """Adds a generic component for a firmware"""

    # find firmware
    try:
        fw = (
            db.session.query(Firmware)
            .filter(Firmware.firmware_id == firmware_id)
            .with_for_update(of=Firmware)
            .one()
        )
    except NoResultFound:
        flash("No firmware {}".format(firmware_id), "warning")
        return redirect(url_for("firmware.route_firmware"))
    if fw.md_generic:
        flash("Firmware already has a generic component", "warning")
        return redirect(
            url_for("components.route_show", component_id=fw.md_generic.component_id)
        )

    # security check
    if not fw.check_acl("@modify"):
        flash(
            "Permission denied: Insufficient permissions to modify firmware", "danger"
        )
        return redirect(url_for("firmware.route_show", firmware_id=firmware_id))

    # create new component based on the best component available
    if not fw.md_prio:
        flash("Firmware has no default component", "warning")
        return redirect(
            url_for("components.route_show", component_id=fw.md_generic.component_id)
        )
    md = Component()
    md.priority = 9
    md.appstream_id = "org.fixme." + fw.md_prio.appstream_id
    md.appstream_type = "generic"
    md.filename_xml = "generic.metainfo.xml"
    md.name = fw.md_prio.name
    md.summary = fw.md_prio.summary
    md.metadata_license = fw.md_prio.metadata_license
    md.url_homepage = fw.md_prio.url_homepage
    md.category = fw.md_prio.category
    md.requirements.append(
        ComponentRequirement(
            kind="id", value="org.freedesktop.fwupd", compare="ge", version="1.5.3"
        )
    )
    fw.mds.append(md)
    db.session.add(
        Task(
            value=json.dumps({"id": fw.firmware_id}),
            caller=__name__,
            user=g.user,
            url=url_for("components.route_show", component_id=md.component_id),
            function="lvfs.firmware.utils.task_sign_fw",
        )
    )
    db.session.commit()

    flash("Firmware updated", "info")
    return redirect(url_for("components.route_show", component_id=md.component_id))


@bp_firmware.route("/<int:firmware_id>/assets")
@login_required
def route_assets(firmware_id: int) -> Any:
    """Shows all firmware assets"""
    try:
        fw = (
            db.session.query(Firmware).filter(Firmware.firmware_id == firmware_id).one()
        )
    except NoResultFound:
        flash("Failed to get firmware details: No a firmware with that ID", "warning")
        return redirect(url_for("firmwares.route_list_admin"), 302)

    # security check
    if not fw.check_acl("@modify"):
        flash("Permission denied: Insufficient permissions", "danger")
        return redirect(url_for("firmware.route_firmware"), 403)

    return render_template("firmware-assets.html", category="firmware", fw=fw)


@bp_firmware.post("/<int:firmware_id>/asset/upload")
@login_required
def route_asset_upload(firmware_id: int) -> Any:
    """Allows uploading a firmware asset"""

    # check exists
    try:
        fw = (
            db.session.query(Firmware)
            .filter(Firmware.firmware_id == firmware_id)
            .with_for_update(of=Firmware)
            .one()
        )
    except NoResultFound:
        flash("Failed to add asset: No firmware with that ID", "warning")
        return redirect(url_for("firmware.route_firmware"), 302)

    # security check
    if not fw.check_acl("@modify"):
        flash("Permission denied: Insufficient permissions", "danger")
        return redirect(url_for("firmware.route_firmware"), 403)

    # read file
    try:
        fileitem = request.files["file"]
    except KeyError:
        flash("Failed to add asset: No file", "warning")
        return redirect(url_for("firmware.route_assets", firmware_id=firmware_id), 302)
    if not fileitem.filename:
        flash("Failed to add asset: No filename", "warning")
        return redirect(url_for("firmware.route_assets", firmware_id=firmware_id), 302)
    blob = fileitem.read()

    # write to EFS and save to database
    asset = FirmwareAsset(
        fw=fw,
        filename="{}-{}".format(hashlib.sha256(blob).hexdigest(), fileitem.filename),
        user=g.user,
    )
    if os.path.exists(asset.absolute_path):
        flash(
            "Failed to add asset: The file {} already exists".format(asset.filename),
            "warning",
        )
        return redirect(url_for("firmware.route_assets", firmware_id=firmware_id), 302)
    with open(asset.absolute_path, "wb") as f:
        f.write(blob)
    fw.assets.append(asset)
    db.session.commit()

    # success
    flash("Added asset {}".format(asset.filename), "info")
    return redirect(url_for("firmware.route_assets", firmware_id=firmware_id), 302)


@bp_firmware.post("/<int:firmware_id>/asset/<int:firmware_asset_id>/delete")
@login_required
def route_asset_delete(firmware_id: int, firmware_asset_id: int) -> Any:
    """Allows deleting a firmware asset"""

    # check exists
    try:
        asset = (
            db.session.query(FirmwareAsset)
            .filter(FirmwareAsset.firmware_asset_id == firmware_asset_id)
            .join(Firmware)
            .filter(Firmware.firmware_id == firmware_id)
            .one()
        )
    except NoResultFound:
        flash("Failed to delete asset: No asset with those IDs", "warning")
        return redirect(url_for("firmware.route_firmware"), 302)

    # security check
    if not asset.fw.check_acl("@modify"):
        flash("Permission denied: Insufficient permissions", "danger")
        return redirect(url_for("firmware.route_firmware"), 403)

    os.remove(asset.absolute_path)
    db.session.delete(asset)
    db.session.commit()
    flash("Deleted asset", "info")
    return redirect(url_for("firmware.route_assets", firmware_id=firmware_id), 302)
