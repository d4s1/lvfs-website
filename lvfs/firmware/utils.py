#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=protected-access,unused-argument

import os
import codecs
import difflib
import hashlib
import datetime
import json
from typing import List

from collections import defaultdict

from lxml import etree as ET
from flask import render_template, url_for
from flask import current_app as app
from sqlalchemy.exc import NoResultFound

from cabarchive import CabArchive, CabFile, NotSupportedError

from jcat import JcatFile, JcatBlobSha1, JcatBlobSha256, JcatBlobKind

from lvfs import db, ploader

from lvfs.components.models import Component
from lvfs.emails import send_email_sync
from lvfs.firmware.models import FirmwareRevision, FirmwareEventKind, FirmwareAction
from lvfs.tasks.models import Task
from lvfs.tests.utils import _test_run_all
from lvfs.metadata.models import Remote
from lvfs.metadata.utils import (
    _generate_metadata_mds,
    MetadataExportKind,
)
from lvfs.metadata.utils import _regenerate_and_sign_metadata_remote
from lvfs.util import _get_settings, _get_sanitized_basename

from .models import Firmware, FirmwareEvent


def _firmware_delete(fw: Firmware, task: Task) -> None:

    # find remotes
    try:
        remote = db.session.query(Remote).filter(Remote.name == "deleted").one()
    except NoResultFound:
        task.add_fail("No deleted remote")
        return

    # mark as invalid
    fw.events.append(
        FirmwareEvent(
            kind=FirmwareEventKind.DELETED.value,
            remote_old=fw.remote,
            remote=remote,
            user=task.user,
        )
    )
    fw.remote = remote
    db.session.commit()

    # asynchronously sign
    db.session.add(
        Task(
            value=json.dumps({"id": fw.remote.remote_id}),
            caller=__name__,
            user=task.user,
            function="lvfs.metadata.utils.task_regenerate_remote",
        )
    )
    db.session.commit()


def _delete_embargo_obsoleted_fw(task: Task) -> None:

    # only on the production instance
    if app.config["RELEASE"] != "production":
        return

    # all embargoed firmware
    emails = defaultdict(list)
    for (firmware_id,) in (
        db.session.query(Firmware.firmware_id)
        .join(Remote)
        .filter(Remote.name.startswith("embargo"))
        .order_by(Firmware.timestamp.asc())
        .with_for_update(of=Firmware)
    ):

        # less than 6 months old
        fw = (
            db.session.query(Firmware)
            .filter(Firmware.firmware_id == firmware_id)
            .first()
        )
        if not fw:
            continue
        if fw.target_duration < datetime.timedelta(days=30 * 6):
            continue

        # check that all the components are available with new versions
        all_newer = True
        for md in fw.mds:
            md_newest = None
            for md_new in (
                db.session.query(Component)
                .filter(Component.appstream_id == md.appstream_id)
                .join(Firmware)
                .join(Remote)
                .filter(Remote.is_public)
                .order_by(Firmware.timestamp.asc())
            ):
                if md_new > md or (md_newest and md_new > md_newest):
                    md_newest = md_new
                    break
            if not md_newest:
                all_newer = False
                break
            task.add_pass(
                "{} {} [{}] is newer than {} [{}]".format(
                    md.appstream_id,
                    md_newest.version_display,
                    md_newest.fw.remote.name,
                    md.version_display,
                    md.fw.remote.name,
                )
            )
        if not all_newer:
            continue

        # delete, but not purge...
        _firmware_delete(fw, task=task)

        # dedupe emails by user
        emails[fw.user].append(fw)

    # send email to the user that uploaded them, unconditionally
    for user in emails:
        send_email_sync(
            "[LVFS] Firmware has been obsoleted",
            user.email_address,
            render_template("email-firmware-obsolete.txt", user=user, fws=emails[user]),
            task=task,
        )

    # all done
    db.session.commit()


def _purge_old_deleted_firmware(task: Task) -> None:

    # find all unsigned firmware
    for (firmware_id,) in (
        db.session.query(Firmware.firmware_id)
        .join(Remote)
        .filter(Remote.name == "deleted")
        .order_by(Firmware.timestamp.asc())
        .with_for_update(of=Firmware)
    ):
        fw = (
            db.session.query(Firmware)
            .filter(Firmware.firmware_id == firmware_id)
            .first()
        )
        if not fw:
            continue
        if fw.target_duration > datetime.timedelta(days=30):
            for rev in fw.revisions:
                if os.path.exists(rev.absolute_path):
                    os.remove(rev.absolute_path)
            task.add_pass(
                "Removing {} as age {}".format(fw.firmware_id, fw.target_duration)
            )
            for md in fw.mds:
                for shard in md.shards:
                    path = shard.absolute_path
                    if os.path.exists(path):
                        os.remove(path)
            db.session.delete(fw)
            db.session.commit()


def _delete_old_revisions(task: Task) -> None:

    settings = _get_settings()
    try:
        unused_revision_gc = int(settings["unused_revision_gc"])
    except KeyError:
        return

    # find all old revisions and delete them
    now = datetime.datetime.utcnow()
    for (firmware_id,) in db.session.query(Firmware.firmware_id).order_by(
        Firmware.timestamp.asc()
    ):
        fw = (
            db.session.query(Firmware)
            .filter(Firmware.firmware_id == firmware_id)
            .with_for_update(of=Firmware)
            .first()
        )
        if not fw:
            continue
        # if the latest release is newer than the timeval then just skip GC
        if fw.revisions[0].timestamp > now - datetime.timedelta(
            hours=unused_revision_gc
        ):
            continue
        for rev in fw.revisions[1:]:
            if rev.immutable:
                continue
            if rev.timestamp > now - datetime.timedelta(hours=unused_revision_gc):
                continue
            task.add_pass("autodeleting fw {} revision {}".format(firmware_id, rev))
            if os.path.exists(rev.absolute_path):
                os.remove(rev.absolute_path)
            db.session.delete(rev)
            db.session.commit()


def _promote_firmware_action_fw(fw: Firmware, remote: Remote, task: Task) -> None:

    # we can't auto-promoted firmware with problems
    if fw.problems:
        for u in fw.get_possible_users_to_email:
            if not u.get_action("notify-promote"):
                continue
            send_email_sync(
                "[LVFS] Firmware has NOT been auto-promoted to {} due to problems".format(
                    fw.remote.name
                ),
                u.email_address,
                render_template("email-firmware-promoted.txt", user=task.user, fw=fw),
                task=task,
            )
        return

    # sign
    for r in set([remote, fw.remote] + [vendor.remote for vendor in fw.odm_vendors]):
        _regenerate_and_sign_metadata_remote(r, task=task)

    # some tests only run when the firmware is in stable
    ploader.ensure_test_for_fw(fw)
    db.session.add(
        Task(
            value=json.dumps({"id": fw.firmware_id}),
            caller=__name__,
            user=task.user,
            url=url_for("firmware.route_show", firmware_id=fw.firmware_id),
            function="lvfs.tests.utils.task_run_for_firmware",
        )
    )

    # all okay
    fw.events.append(
        FirmwareEvent(
            kind=FirmwareEventKind.MOVED.value,
            remote_old=fw.remote,
            remote=remote,
            user=task.user,
        )
    )
    fw.remote_id = remote.remote_id
    db.session.commit()

    # send email
    for u in fw.get_possible_users_to_email:
        if not u.get_action("notify-promote"):
            continue
        send_email_sync(
            "[LVFS] Firmware has been auto-promoted to {}".format(fw.remote.name),
            u.email_address,
            render_template("email-firmware-promoted.txt", user=task.user, fw=fw),
            task=task,
        )


def _promote_firmware_action(task: Task) -> None:

    # find all firmwares that need actions
    for (firmware_action_id,) in (
        db.session.query(FirmwareAction.firmware_action_id)
        .filter(FirmwareAction.dtime == None)
        .filter(FirmwareAction.mtime > datetime.datetime.utcnow())
        .order_by(FirmwareAction.ctime.asc())
    ):
        action = (
            db.session.query(FirmwareAction)
            .filter(FirmwareAction.firmware_action_id == firmware_action_id)
            .join(Firmware)
            .with_for_update(of=[Firmware, FirmwareAction])
            .first()
        )
        if not action:
            continue
        _promote_firmware_action_fw(action.fw, action.remote, task=task)
        action.dtime = datetime.datetime.utcnow()
        db.session.commit()


def _delete_old_uploaded_revisions(task: Task) -> None:

    # find all old unpaired revisions and delete them
    now = datetime.datetime.utcnow()
    for (firmware_revision_id,) in (
        db.session.query(FirmwareRevision.firmware_revision_id)
        .filter(FirmwareRevision.immutable == False)
        .filter(FirmwareRevision.firmware_id == None)
        .filter(FirmwareRevision.timestamp < now - datetime.timedelta(days=30))
        .order_by(FirmwareRevision.timestamp.asc())
    ):
        rev = (
            db.session.query(FirmwareRevision)
            .filter(FirmwareRevision.firmware_revision_id == firmware_revision_id)
            .join(Firmware)
            .with_for_update(of=[Firmware, FirmwareRevision])
            .first()
        )
        if not rev:
            continue
        task.add_pass("autodeleting old revision {}".format(rev))
        os.remove(rev.absolute_path)
        db.session.delete(rev)
        db.session.commit()


def task_autodelete(task: Task) -> None:
    _delete_embargo_obsoleted_fw(task)
    _delete_old_revisions(task)
    _delete_old_uploaded_revisions(task)
    _purge_old_deleted_firmware(task)
    _promote_firmware_action(task)


def _generate_diff(blob_old: bytes, blob_new: bytes) -> str:

    # this might be UTF-16 from the upload!
    xml_old = None
    for bom, name in [
        (codecs.BOM_UTF16_LE, "utf-16le"),
        (codecs.BOM_UTF16_BE, "utf-16be"),
    ]:
        if blob_old.startswith(bom):
            xml_old = blob_old[len(bom) :].decode(name)
            break
    if not xml_old:
        xml_old = blob_old.decode()

    # compare
    fromlines = xml_old.replace("\r", "").split("\n")
    tolines = blob_new.decode().split("\n")
    diff = difflib.unified_diff(fromlines, tolines)
    return "\n".join(list(diff)[3:])


def task_nuke(task: Task) -> None:
    fw = (
        db.session.query(Firmware)
        .filter(Firmware.firmware_id == task.id)
        .join(Remote)
        .filter(Remote.name == "deleted")
        .with_for_update(of=[Firmware, Remote])
        .first()
    )
    if not fw:
        return

    # really delete resources
    for rev in fw.revisions:
        try:
            os.remove(rev.absolute_path)
        except (FileNotFoundError, PermissionError) as e:
            task.add_fail("Failed to delete {}".format(rev.absolute_path), str(e))
    for asset in fw.assets:
        try:
            os.remove(asset.absolute_path)
        except (FileNotFoundError, PermissionError) as e:
            task.add_fail("Failed to delete {}".format(asset.absolute_path), str(e))

    # delete shard cache if they exist
    for md in fw.mds:
        for shard in md.shards:
            try:
                os.remove(shard.absolute_path)
            except (FileNotFoundError, PermissionError) as e:
                task.add_fail("Failed to delete {}".format(shard.absolute_path), str(e))

    # delete everything we stored about the firmware
    db.session.delete(fw)
    db.session.commit()


def task_sign_fw(task: Task) -> None:
    fw = (
        db.session.query(Firmware)
        .filter(Firmware.firmware_id == task.id)
        .with_for_update(of=Firmware)
        .first()
    )
    if not fw:
        return
    _sign_fw(fw, task=task)

    # just do it now
    if not fw.remote.is_public:
        _regenerate_and_sign_metadata_remote(fw.remote, task=task)


def _sign_fw(fw: Firmware, task: Task) -> None:

    # load the .cab file
    fn = fw.revisions[0].absolute_path
    try:
        with open(fn, "rb") as f:
            cabarchive = CabArchive(f.read(), flattern=True)
    except (IOError, FileNotFoundError, NotSupportedError, NotImplementedError) as e:
        raise NotImplementedError("cannot read %s" % fn) from e

    # inform the plugin loader
    ploader.archive_presign(fw)

    # create Jcat file
    jcatfile = JcatFile()

    # sign each component in the archive
    for md in fw.mds:
        if not md.filename_contents:
            continue
        try:

            # create Jcat item with SHA1 and SHA256 checksum blob
            cabfile = cabarchive[md.filename_contents]
            jcatitem = jcatfile.get_item(md.filename_contents)
            jcatitem.add_blob(JcatBlobSha1(cabfile.buf))
            jcatitem.add_blob(JcatBlobSha256(cabfile.buf))

            # sign using plugins
            for blob in ploader.archive_sign(cabfile.buf):

                # add GPG only to archive for backwards compat with older fwupd
                if blob.kind == JcatBlobKind.GPG:
                    fn_blob = md.filename_contents + "." + blob.filename_ext
                    cabarchive[fn_blob] = CabFile(blob.data)

                # add to Jcat file too
                jcatitem.add_blob(blob)

            # add other things like the inclusion proof
            for blob in ploader.component_sign(md):
                jcatitem.add_blob(blob)

        except KeyError as e:
            raise NotImplementedError(
                "no {} firmware found".format(md.filename_contents)
            ) from e

    # rewrite the metainfo.xml file to reflect latest changes and sign it
    for md in fw.mds:

        # write new metainfo.xml file
        component = _generate_metadata_mds(
            [md], export_kind=MetadataExportKind.METAINFO
        )
        blob_xml = b'<?xml version="1.0" encoding="UTF-8"?>\n' + ET.tostring(
            component, encoding="UTF-8", xml_declaration=False, pretty_print=True
        )
        if md.filename_xml in cabarchive:
            diff: str = _generate_diff(cabarchive[md.filename_xml].buf, blob_xml)
            if diff:
                task.add_pass("Reformatted XML", diff)
        cabarchive[md.filename_xml] = CabFile(blob_xml)

        # sign it
        jcatitem = jcatfile.get_item(md.filename_xml)
        jcatitem.add_blob(JcatBlobSha1(blob_xml))
        jcatitem.add_blob(JcatBlobSha256(blob_xml))
        for blob in ploader.archive_sign(blob_xml):
            jcatitem.add_blob(blob)

    # write jcat file
    if jcatfile.items:
        cabarchive["firmware.jcat"] = CabFile(jcatfile.save())

    # update archive with extra files
    ploader.archive_finalize(cabarchive, fw)

    # update the database
    settings = _get_settings()
    cab_data = cabarchive.save()
    fw.checksum_signed_sha1 = hashlib.sha1(cab_data).hexdigest()
    fw.checksum_signed_sha256 = hashlib.sha256(cab_data).hexdigest()
    fw.signed_timestamp = datetime.datetime.utcnow()
    fw.signed_epoch = int(settings["signed_epoch"])

    # update the download size
    for md in fw.mds:
        md.release_download_size = len(cab_data)

    # add new revision
    rev = FirmwareRevision(
        filename="{}-{}".format(
            fw.checksum_signed_sha256, _get_sanitized_basename(fw.original_filename)
        )
    )
    with open(rev.absolute_path, "wb") as f:
        f.write(cab_data)
    fw.revisions.append(rev)
    db.session.commit()

    # inform the plugin loader
    ploader.file_modified(rev.absolute_path)

    # inform the plugin loader
    ploader.archive_hash(fw)


def _sign_firmware_all(task: Task) -> None:

    # find all unsigned firmware
    fws = db.session.query(Firmware).filter(Firmware.signed_timestamp == None).all()
    if not fws:
        return

    # sign each firmware in each file
    for fw in fws:
        if fw.is_deleted:
            continue
        _sign_fw(fw, task=task)

    # drop caches in other sessions
    # db.session.expire_all()


def task_modify_fw(task: Task) -> None:
    values = json.loads(task.value)
    fw = (
        db.session.query(Firmware)
        .filter(Firmware.firmware_id == values["id"])
        .join(Component)
        .with_for_update(of=[Firmware, Component])
        .first()
    )
    if not fw:
        return
    if values["retry-all"]:
        for test in fw.tests:
            test.retry()

    # ensure the test has been added for the new firmware type
    ploader.ensure_test_for_fw(fw)

    # just do it now
    _test_run_all(fw.tests, task=task)
    _sign_fw(fw, task=task)
    if not fw.remote.is_public:
        _regenerate_and_sign_metadata_remote(fw.remote, task=task)
    db.session.commit()


def task_promote_fw(task: Task) -> None:
    values = json.loads(task.value)
    fw = (
        db.session.query(Firmware)
        .filter(Firmware.firmware_id == values["id"])
        .join(Component)
        .with_for_update(of=[Firmware, Component])
        .first()
    )
    if not fw:
        return

    # set new remote
    target = values["target"]
    if target == "embargo":
        remote = fw.vendor.remote
    else:
        try:
            remote = db.session.query(Remote).filter(Remote.name == target).one()
        except NoResultFound:
            task.add_fail("No remote for target {}".format(target))
            return

    # same as before
    if fw.remote.remote_id == remote.remote_id:
        task.add_fail("Firmware already in target {}".format(target))
        return

    # vendor has to fix the problems first
    probs: List[str] = []
    for problem in fw.problems:
        if problem.kind in probs:
            continue
        if (target == "embargo" and not problem.allow_embargo) or target in [
            "stable",
            "testing",
        ]:
            probs.append(problem.kind)
    if probs:
        task.add_fail("Problems must be fixed first {}".format(", ".join(probs)))
        return

    # some tests only run when the firmware is in stable
    if remote.name == "stable":
        ploader.ensure_test_for_fw(fw)

    # all okay
    fw.events.append(
        FirmwareEvent(
            kind=FirmwareEventKind.MOVED.value,
            remote_old=fw.remote,
            remote=remote,
            user=task.user,
        )
    )
    fw.remote = remote
    db.session.commit()

    # sync
    if target != "private":
        _test_run_all(fw.tests, task=task)
    for r in set([remote, fw.remote] + [vendor.remote for vendor in fw.odm_vendors]):
        _regenerate_and_sign_metadata_remote(r, task=task)

    # send email
    for u in fw.get_possible_users_to_email:
        if u == task.user:
            continue
        if u.get_action("notify-promote"):
            send_email_sync(
                "[LVFS] Firmware has been promoted",
                u.email_address,
                render_template(
                    "email-firmware-promoted.txt", user=u, user_upload=task.user, fw=fw
                ),
                task=task,
            )
    db.session.commit()
