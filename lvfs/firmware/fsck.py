#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods,protected-access

from typing import List, Optional
import datetime
import json
import os
from sqlalchemy import or_
from flask import g, url_for

from cabarchive import CabArchive, CorruptionError
from jcat import JcatFile, JcatBlobKind

from lvfs.util import _get_settings
from lvfs.tasks.models import Task
from lvfs import db

from .models import Firmware, FirmwareRevision


def _fsck_firmware_unsigned(self: Firmware, task: Task) -> None:
    """invalidate the signature of firmware signed with an older key"""

    # sanity check
    if not self.signed_timestamp:
        return

    # unset the signed timestamp as required
    settings = _get_settings()
    signed_epoch = int(settings["signed_epoch"])

    # we signed this recently enough to be epoch 1
    if self.signed_timestamp.replace(tzinfo=None) > datetime.datetime(2020, 3, 5):
        self.signed_epoch = 1
    # not good enough
    if self.signed_epoch != signed_epoch:
        self.signed_timestamp = None
        task.add_pass(
            "Database::Firmware",
            "Invaliding signing checksum of {}".format(self.firmware_id),
        )


def _fsck_firmware_check_exists(self: Firmware, task: Task) -> None:
    """revision no longer exists"""
    for rev in self.revisions:
        if not os.path.exists(rev.absolute_path):
            task.add_fail(
                "EFS",
                "Firmware #{} has missing revision {} {}".format(
                    self.firmware_id, rev.firmware_revision_id, rev.absolute_path
                ),
            )


def _fsck_firmware_fix_properties(self: Firmware, task: Task) -> None:
    """fix up any broken properties"""

    for md in self.mds:
        if md.release_tag in ["None", ""]:
            task.add_fail(
                "Component",
                "Firmware #{} has invalid release tag {}, fixing".format(
                    self.firmware_id, md.release_tag
                ),
            )
            md.release_tag = None


def _fsck_firmware_consistency(self: Firmware, task: Task) -> None:
    """multiple Firmware objects pointing at the same filesystem object"""
    if not self.revisions:
        return
    for fw2 in (
        db.session.query(Firmware)
        .filter(Firmware.firmware_id != self.firmware_id)
        .join(FirmwareRevision)
        .filter(
            or_(
                FirmwareRevision.filename == self.revisions[0].filename,
                Firmware.checksum_upload_sha1 == self.checksum_upload_sha1,
            )
        )
        .limit(10)
        .all()
    ):
        task.add_fail(
            "Database::Firmware",
            "Firmware {} points to {} [SHA1:{}]".format(
                fw2.firmware_id,
                fw2.filename,
                fw2.checksum_upload_sha1,
            ),
        )


def _fsck_firmware_metainfo_nonnull(
    self: Firmware, task: Task, arc: CabArchive
) -> None:
    """NUL byte in metainfo file"""

    requires_resign: bool = False

    for fn in arc:
        if not fn.endswith(".xml"):
            continue
        if arc[fn].buf[-1] == 0:
            task.add_fail(
                "JCat::Firmware", "NUL found in metainfo #{}".format(self.firmware_id)
            )
            requires_resign = True
            break

    # invalidate and resign
    if requires_resign:
        db.session.add(
            Task(
                value=json.dumps({"id": self.firmware_id}),
                caller=__name__,
                user=g.user,
                url=url_for("firmware.route_show", firmware_id=self.firmware_id),
                function="lvfs.firmware.utils.task_sign_fw",
            )
        )
        db.session.commit()


def _fsck_firmware_metainfo_artifact_binary(
    self: Firmware, task: Task, arc: CabArchive
) -> None:
    """<artifact type="binary"> in metainfo file"""

    requires_resign: bool = False

    for fn in arc:
        if not fn.endswith(".metainfo.xml"):
            continue
        if arc[fn].buf.decode().find('<artifact type="binary">') != -1:
            task.add_fail(
                "JCat::Firmware",
                "binary artifact found in metainfo #{}".format(self.firmware_id),
            )
            requires_resign = True
            break

    # invalidate and resign
    if requires_resign:
        db.session.add(
            Task(
                value=json.dumps({"id": self.firmware_id}),
                caller=__name__,
                user=g.user,
                url=url_for("firmware.route_show", firmware_id=self.firmware_id),
                function="lvfs.firmware.utils.task_sign_fw",
                priority=-10,
            )
        )
        db.session.commit()


def _fsck_firmware_pkcs7_cert_valid(
    self: Firmware, task: Task, arc: CabArchive
) -> None:
    """missing cert from PKCS#7 cert"""

    requires_resign: bool = False

    # check each signature has a server CERTIFICATE, not just a signature
    try:
        jcat_file = JcatFile(arc["firmware.jcat"].buf)
    except KeyError:
        task.add_fail(
            "JCat::Firmware", "No firmware.jcat in archive #{}".format(self.firmware_id)
        )
        requires_resign = True
    else:
        for md in self.mds:

            # files that used to be valid, but are no longer allowed
            if not md.release_installed_size:
                continue

            # check all the things that are supposed be signed in the jcat file
            for fn in [md.filename_contents, md.filename_xml]:
                if not fn:
                    continue
                jcat_item = jcat_file.get_item(fn)
                if jcat_item:
                    # is this big enough to include the certificate?
                    jcat_blob = jcat_item.get_blob_by_kind(JcatBlobKind.PKCS7)
                    if not jcat_blob:
                        task.add_fail(
                            "JCat::PKCS7",
                            "No cert for #{}".format(self.firmware_id),
                        )
                        requires_resign = True
                    elif not jcat_blob.data:
                        task.add_fail(
                            "JCat::PKCS7",
                            "No valid cert for #{}".format(self.firmware_id),
                        )
                        requires_resign = True
                    elif len(jcat_blob.data) < 0x400:
                        task.add_fail(
                            "JCat::PKCS7",
                            "Invalid cert for #{}".format(self.firmware_id),
                        )
                        requires_resign = True
                else:
                    task.add_fail(
                        "JCat::Component",
                        "No jcat item for #{}".format(self.firmware_id),
                    )
                    requires_resign = True

    # invalidate and resign
    if requires_resign:
        db.session.add(
            Task(
                value=json.dumps({"id": self.firmware_id}),
                caller=__name__,
                user=g.user,
                url=url_for("firmware.route_show", firmware_id=self.firmware_id),
                function="lvfs.firmware.utils.task_sign_fw",
            )
        )
        db.session.commit()


def _fsck_firmware_check_archive(self: Firmware, task: Task) -> None:
    """load archive from disk"""

    # load cabarchive
    try:
        arc = CabArchive(self.blob, flattern=True)
    except CorruptionError:
        task.add_fail(
            "EFS::CabArchive",
            "Cannot load firmware archive #{}".format(self.firmware_id),
        )
        return

    # cert valid
    _fsck_firmware_metainfo_nonnull(self, task, arc)
    _fsck_firmware_metainfo_artifact_binary(self, task, arc)
    _fsck_firmware_pkcs7_cert_valid(self, task, arc)


def _fsck_firmware_resign(self: Firmware, task: Task) -> None:
    """unsigned firmware"""
    if self.signed_timestamp:
        return
    db.session.add(
        Task(
            value=json.dumps({"id": self.firmware_id}),
            caller=__name__,
            user=g.user,
            url=url_for("firmware.route_show", firmware_id=self.firmware_id),
            function="lvfs.firmware.utils.task_sign_fw",
        )
    )
    db.session.commit()
    task.add_pass("Database::Firmware", "Resigning #{}".format(self.firmware_id))


def _fsck(self: Firmware, task: Task, kinds: Optional[List[str]] = None) -> None:

    # these need a cache
    if not kinds or "consistency" in kinds:
        _fsck_firmware_consistency(self, task)
    if not kinds or "signed-epoch" in kinds:
        _fsck_firmware_unsigned(self, task)
    if not kinds or "check-exists" in kinds:
        _fsck_firmware_check_exists(self, task)
    if not kinds or "resign" in kinds:
        _fsck_firmware_resign(self, task)
    if not kinds or "release-properties" in kinds:
        _fsck_firmware_fix_properties(self, task)
    if not kinds or "check-archive" in kinds:
        _fsck_firmware_check_archive(self, task)
