#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

import datetime
from typing import Any, Optional

from flask import Blueprint, request, url_for, redirect, flash, render_template, g
from flask_login import login_required
from sqlalchemy.exc import NoResultFound, IntegrityError

from lvfs import db

from lvfs.util import admin_login_required
from lvfs.tasks.models import Task, TaskScheduler

bp_tasks = Blueprint("tasks", __name__, template_folder="templates")


@bp_tasks.route("/")
@bp_tasks.route("/state/<state>")
@bp_tasks.route("/limit/<int:limit>")
@login_required
@admin_login_required
def route_list(state: Optional[str] = None, limit: int = 15) -> Any:
    page = request.args.get("page", 1, type=int)
    stmt = db.session.query(Task)
    if state == "pending":
        stmt = stmt.filter(Task.started_ts == None, Task.ended_ts == None)
    elif state == "running":
        stmt = stmt.filter(Task.started_ts != None, Task.ended_ts == None)
    task_pgs = stmt.order_by(Task.task_id.desc()).paginate(page=page, per_page=limit)
    return render_template(
        "task-list.html", category="admin", task_pgs=task_pgs, state=state
    )


@bp_tasks.post("/delete/<int:task_id>")
@login_required
@admin_login_required
def route_delete(task_id: int) -> Any:

    # get tests
    try:
        db.session.query(Task).filter(Task.task_id == task_id).delete()
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Task cannot be deleted", "warning")
        return redirect(url_for("tasks.route_list"))

    flash("Task has been deleted", "info")
    return redirect(url_for("tasks.route_list"))


@bp_tasks.post("/<int:task_id>/cancel")
@login_required
@admin_login_required
def route_cancel(task_id: int) -> Any:

    # get tests
    try:
        task = (
            db.session.query(Task)
            .filter(Task.task_id == task_id)
            .with_for_update(of=Task)
            .one()
        )
    except NoResultFound:
        flash("No task matched", "warning")
        return redirect(url_for("tasks.route_list"))
    try:
        task.ended_ts = datetime.datetime.utcnow()
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Task cannot be cancelled")
        return redirect(url_for("tasks.route_list"))

    flash("Task has been canceled", "info")
    return redirect(url_for("tasks.route_list"))


@bp_tasks.post("/<int:task_id>/retry")
@login_required
@admin_login_required
def route_retry(task_id: int) -> Any:

    try:
        task = (
            db.session.query(Task)
            .filter(Task.task_id == task_id)
            .with_for_update(of=Task)
            .one()
        )
    except NoResultFound:
        flash("No task matched", "warning")
        return redirect(url_for("tasks.route_list"))
    db.session.add(
        Task(
            caller=task.caller,
            user=task.user,
            priority=task.priority,
            value=task.value,
            url=task.url,
            function=task.function,
        )
    )
    db.session.commit()

    flash("Task has been rescheduled", "info")
    return redirect(url_for("tasks.route_list"))


@bp_tasks.route("/scheduler/all")
@login_required
@admin_login_required
def route_scheduler_list() -> Any:
    task_scheds = (
        db.session.query(TaskScheduler)
        .order_by(TaskScheduler.task_scheduler_id.asc())
        .all()
    )
    return render_template(
        "task-scheduler-list.html", category="admin", task_scheds=task_scheds
    )


@bp_tasks.post("/scheduler/create")
@login_required
@admin_login_required
def route_scheduler_create() -> Any:

    # add scheduled task
    try:
        task_sched = TaskScheduler(
            function=request.form["function"], user=g.user, interval=0
        )
        db.session.add(task_sched)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Already exists", "warning")
        return redirect(url_for("tasks.route_scheduler_list"))
    except KeyError:
        flash("No form data found!", "warning")
        return redirect(url_for("tasks.route_scheduler_list"))
    flash("Added task scheduler", "info")
    return redirect(
        url_for(
            "tasks.route_scheduler_show", task_scheduler_id=task_sched.task_scheduler_id
        )
    )


@bp_tasks.post("/scheduler/<int:task_scheduler_id>/delete")
@login_required
@admin_login_required
def route_scheduler_delete(task_scheduler_id: int) -> Any:

    # get task
    try:
        db.session.query(TaskScheduler).filter(
            TaskScheduler.task_scheduler_id == task_scheduler_id
        ).delete()
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Cannot delete", "danger")
        return redirect(url_for("tasks.route_scheduler_list"))
    flash("Deleted task scheduler", "info")
    return redirect(url_for("tasks.route_scheduler_list"))


@bp_tasks.post("/scheduler/<int:task_scheduler_id>/modify")
@login_required
@admin_login_required
def route_scheduler_modify(task_scheduler_id: int) -> Any:

    # find task
    try:
        task_sched = (
            db.session.query(TaskScheduler)
            .filter(TaskScheduler.task_scheduler_id == task_scheduler_id)
            .with_for_update(of=TaskScheduler)
            .one()
        )
    except NoResultFound:
        flash("No task scheduler found", "info")
        return redirect(url_for("tasks.route_scheduler_list"))

    # modify task
    for key in ["interval", "function", "priority"]:
        if key in request.form:
            setattr(task_sched, key, request.form[key])
    db.session.commit()

    # success
    flash("Modified task scheduler", "info")
    return redirect(
        url_for("tasks.route_scheduler_show", task_scheduler_id=task_scheduler_id)
    )


@bp_tasks.route("/scheduler/<int:task_scheduler_id>")
@login_required
@admin_login_required
def route_scheduler_show(task_scheduler_id: int) -> Any:

    # find task
    try:
        task_sched = (
            db.session.query(TaskScheduler)
            .filter(TaskScheduler.task_scheduler_id == task_scheduler_id)
            .one()
        )
    except NoResultFound:
        flash("No task scheduler found", "info")
        return redirect(url_for("tasks.route_scheduler_list"))

    # show details
    return render_template("task-details.html", category="admin", task_sched=task_sched)
