#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods

import datetime
import json
import os
from typing import Optional, Type
from types import TracebackType

from sqlalchemy import Column, Integer, Text, DateTime, Boolean, ForeignKey, Index
from sqlalchemy.orm import relationship

from lvfs import db


class TaskScheduler(db.Model):  # type: ignore

    __tablename__ = "task_schedulers"

    task_scheduler_id = Column(Integer, primary_key=True)
    task_id = Column(Integer, ForeignKey("tasks.task_id"), nullable=True)
    user_id = Column(Integer, ForeignKey("users.user_id"))
    interval = Column(Integer, nullable=False)
    function = Column(Text, unique=True, nullable=False)
    created_ts = Column(DateTime, default=datetime.datetime.utcnow)
    started_ts = Column(DateTime, default=None)
    priority = Column(Integer, default=0)  # higher is more urgent

    task = relationship("Task")
    user = relationship("User", foreign_keys=[user_id])

    @property
    def color(self) -> str:
        if self.task:
            if self.task.success:
                return "success"
        elif self.started_ts:
            return "info"
        return "danger"

    def __repr__(self) -> str:
        return "TaskScheduler object {}:{}".format(
            self.task_scheduler_id, self.function
        )


class TaskAttribute(db.Model):  # type: ignore
    __tablename__ = "task_attributes"

    fsck_attribute_id = Column(Integer, primary_key=True)
    task_id = Column(
        Integer,
        ForeignKey("tasks.task_id", ondelete="CASCADE"),
        nullable=False,
        index=True,
    )
    title = Column(Text, nullable=False)
    message = Column(Text, default=None)
    success = Column(Boolean, default=False)
    created_ts = Column(DateTime, nullable=True, default=datetime.datetime.utcnow)

    task = relationship("Task", back_populates="attributes")

    def __repr__(self) -> str:
        return "TaskAttribute object {}:{}".format(self.title, self.message)


class Task(db.Model):  # type: ignore

    __tablename__ = "tasks"
    __table_args__ = (Index("idx_tasks_user_id_function", "user_id", "function"),)

    task_id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False)
    container_id = Column(Text, default=None)
    created_ts = Column(DateTime, default=datetime.datetime.utcnow)
    started_ts = Column(DateTime, default=None)
    ended_ts = Column(DateTime, default=None)
    value = Column(Text, default=None)  # JSON
    function = Column(Text, nullable=False, index=True)  # to call
    caller = Column(Text, nullable=False, index=True)  # called from
    priority = Column(Integer, default=0)  # higher is more urgent
    status = Column(Text, default=None)
    url = Column(Text, default=None)
    percentage_current = Column(Integer, default=0)
    percentage_total = Column(Integer, default=0)

    attributes = relationship(
        "TaskAttribute",
        order_by="asc(TaskAttribute.fsck_attribute_id)",
        lazy="joined",
        back_populates="task",
        cascade="all,delete,delete-orphan",
    )
    user = relationship("User", foreign_keys=[user_id])

    def add_pass(self, title: str, message: Optional[str] = None) -> None:
        self.attributes.append(
            TaskAttribute(title=title, message=message, success=True)
        )

    def add_fail(self, title: str, message: Optional[str] = None) -> None:
        self.attributes.append(
            TaskAttribute(title=title, message=message, success=False)
        )

    def attr(self, key: str) -> Optional[str]:
        try:
            return json.loads(self.value)[key]
        except (KeyError, TypeError):
            return None

    @property
    def id(self) -> Optional[str]:
        return self.attr("id")

    @property
    def percentage(self) -> int:
        if not self.percentage_total:
            return 0
        if not self.percentage_current:
            return 0
        return int(
            float(self.percentage_current) * float(100) / float(self.percentage_total)
        )

    @property
    def is_running(self) -> bool:
        if self.started_ts and not self.ended_ts:
            return True
        return False

    @property
    def priority_description(self) -> str:
        if self.priority <= -10:
            return "lowest"
        if self.priority < 0:
            return "low"
        if self.priority < 5:
            return "normal"
        return "high"

    @property
    def color(self) -> str:
        if self.is_running:
            return "info"
        if self.success:
            return "success"
        return "danger"

    @property
    def success(self) -> bool:
        if not self.attributes:
            return True
        for attr in self.attributes:
            if not attr.success:
                return False
        return True

    def __enter__(self) -> None:
        self.container_id = os.environ.get("CONTAINER_ID")

    def __exit__(
        self,
        exc_type: Optional[Type[BaseException]],
        exc_value: Optional[BaseException],
        traceback: Optional[TracebackType],
    ) -> None:
        self.status = None
        self.percentage_current = 0
        self.percentage_total = 0
        self.ended_ts = datetime.datetime.utcnow()

    def __repr__(self) -> str:
        return "Task object {}:{}".format(self.task_id, self.function)
