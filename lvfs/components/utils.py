#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

import io
from zipfile import ZipFile, ZIP_DEFLATED

from flask import g
from uswid import (
    uSwidContainer,
    uSwidEntity,
    uSwidEntityRole,
    uSwidFormatCycloneDX,
    uSwidFormatSwid,
    uSwidIdentity,
    uSwidLink,
)

from lvfs.components.models import Component

COMPONENT_SWID_REL_TO_DISPLAY = {
    "compiler": "Compiler",
    "component": "Component",
    "license": "License",
    "parent": "Parent",
    "see-also": "See Also",
}


def _build_swid_identity_root(md: Component) -> uSwidIdentity:
    """create identity for the index with original vendor and LVFS"""

    identity = uSwidIdentity(
        tag_id=md.appstream_id,
        software_name=md.name,
        software_version=md.version_display,
    )
    identity.add_entity(
        uSwidEntity(
            name="LVFS",
            regid=g.host_name,
            roles=[uSwidEntityRole.TAG_CREATOR, uSwidEntityRole.DISTRIBUTOR],
        )
    )
    identity.add_entity(
        uSwidEntity(
            name=md.fw.vendor.display_name,
            regid=md.fw.vendor.regid,
            roles=[uSwidEntityRole.SOFTWARE_CREATOR],
        )
    )
    for swid in md.swids:
        identity.add_link(
            uSwidLink(rel="component", href="swid:{}".format(swid.tag_id))
        )
    return identity


def _build_swid_archive(md: Component) -> bytes:

    buf = io.BytesIO()
    identity: uSwidIdentity = _build_swid_identity_root(md)
    with ZipFile(buf, "a", ZIP_DEFLATED, False) as zf:
        zf.writestr("index.xml", uSwidFormatSwid().save(uSwidContainer([identity])))
        for swid in md.swids:
            zf.writestr("{}.xml".format(swid.tag_id), swid.value)

        # mark the files as having been created on Windows
        for zfile in zf.filelist:
            zfile.create_system = 0

    return buf.getvalue()


def _build_swid_cyclonedx(md: Component) -> bytes:

    # create export
    container: uSwidContainer = uSwidContainer([_build_swid_identity_root(md)])
    for swid in md.swids:
        for identity in uSwidFormatSwid().load(swid.value.encode()):
            container.append(identity)
    return uSwidFormatCycloneDX().save(container)
