#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods,protected-access,too-many-instance-attributes
# pylint: disable=too-many-lines,deprecated-module

import os
import collections
import datetime
import math
import hashlib
import zlib
from typing import Optional, List, Dict, Any
from distutils.version import StrictVersion
from lxml import etree as ET

from flask import g, url_for
from flask import current_app as app

from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    Float,
    ForeignKey,
    Integer,
    String,
    Text,
    UniqueConstraint,
)
from sqlalchemy.orm import relationship
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.exc import OperationalError

from pkgversion import vercmp

from lvfs import db

from lvfs.claims.models import Claim
from lvfs.devices.models import Product
from lvfs.users.models import User
from lvfs.vendors.models import VendorTag
from lvfs.util import _split_search_string, _sanitize_keyword, _validate_appstream_id


class ComponentShardInfo(db.Model):  # type: ignore

    __tablename__ = "component_shard_infos"

    component_shard_info_id = Column(Integer, primary_key=True)
    guid = Column(String(36), default=None, index=True, unique=True)
    description = Column(Text, default=None)
    cnt = Column(Integer, default=0)
    claim_id = Column(Integer, ForeignKey("claims.claim_id"), nullable=True, index=True)

    shards = relationship("ComponentShard", cascade="all,delete,delete-orphan")
    claim = relationship("Claim")

    def __repr__(self) -> str:
        return "ComponentShardInfo object %s" % self.component_shard_info_id


class ComponentShardChecksum(db.Model):  # type: ignore

    __tablename__ = "component_shard_checksums"

    checksum_id = Column(Integer, primary_key=True)
    component_shard_id = Column(
        Integer,
        ForeignKey("component_shards.component_shard_id"),
        nullable=False,
        index=True,
    )
    kind = Column(Text, nullable=False, default=None)
    value = Column(Text, nullable=False, default=None, index=True)

    shard = relationship("ComponentShard")

    def __repr__(self) -> str:
        return "ComponentShardChecksum object %s(%s)" % (self.kind, self.value)


class ComponentShardCertificate(db.Model):  # type: ignore

    __tablename__ = "component_shard_certificates"

    component_shard_certificate_id = Column(Integer, primary_key=True)
    component_shard_id = Column(
        Integer,
        ForeignKey("component_shards.component_shard_id"),
        nullable=False,
        index=True,
    )
    kind = Column(Text, default=None)
    plugin_id = Column(Text, default=None)
    description = Column(Text, default=None)
    serial_number = Column(Text, default=None)
    not_before = Column(DateTime, default=None)
    not_after = Column(DateTime, default=None)

    shard = relationship("ComponentShard", back_populates="certificates")

    @property
    def color(self) -> str:
        if self.not_before and self.not_before > self.shard.md.fw.timestamp:
            return "danger"
        if self.not_after and self.not_after < self.shard.md.fw.timestamp:
            return "danger"
        return "success"

    def __repr__(self) -> str:
        data: List[str] = []
        if self.serial_number:
            data.append("serial_number:{}".format(self.serial_number))
        if self.not_before:
            data.append("not_before:{}".format(self.not_before))
        if self.not_after:
            data.append("not_after:{}".format(self.not_after))
        if self.description:
            data.append("desc:{}".format(self.description))
        return "ComponentShardCertificate ({})".format(", ".join(data))


def _calculate_entropy(s: bytes) -> float:
    probabilities = [n_x / len(s) for x, n_x in collections.Counter(s).items()]
    e_x = [-p_x * math.log(p_x, 2) for p_x in probabilities]
    return sum(e_x)


class ComponentShardClaim(db.Model):  # type: ignore

    __tablename__ = "component_shard_claims"

    component_shard_claim_id = Column(Integer, primary_key=True)
    component_shard_info_id = Column(
        Integer, ForeignKey("component_shard_infos.component_shard_info_id")
    )
    checksum = Column(Text, nullable=False, default=None)
    claim_id = Column(Integer, ForeignKey("claims.claim_id"), nullable=True)

    info = relationship("ComponentShardInfo")
    claim = relationship("Claim")

    __table_args__ = (
        UniqueConstraint(
            "component_shard_info_id",
            "checksum",
            name="uq_component_shard_claims_shard_info_id_checksum",
        ),
    )

    def __repr__(self) -> str:
        return "ComponentShardClaim object {},{} -> {}({})".format(
            self.info.guid, self.checksum, self.kind, self.value
        )


class ComponentShardAttribute(db.Model):  # type: ignore
    __tablename__ = "component_shard_attributes"

    component_shard_attribute_id = Column(Integer, primary_key=True)
    component_shard_id = Column(
        Integer,
        ForeignKey("component_shards.component_shard_id"),
        nullable=False,
        index=True,
    )
    plugin_id = Column(Text, default=None, index=True)  # owns
    key = Column(Text, nullable=False, index=True)
    value = Column(Text, default=None, index=True)
    comment = Column(Text, default=None)

    component_shard = relationship("ComponentShard", back_populates="attributes")

    def __repr__(self) -> str:
        return "ComponentShardAttribute object %s=%s [%s]" % (
            self.key,
            self.value,
            self.plugin_id,
        )


class ComponentShard(db.Model):  # type: ignore

    __tablename__ = "component_shards"

    component_shard_id = Column(Integer, primary_key=True)
    component_id = Column(
        Integer, ForeignKey("components.component_id"), nullable=False, index=True
    )
    component_shard_info_id = Column(
        Integer,
        ForeignKey("component_shard_infos.component_shard_info_id"),
        default=None,
    )
    ctime = Column(DateTime, default=datetime.datetime.utcnow)
    plugin_id = Column(Text, default=None, index=True)  # created
    guid = Column(String(36), default=None, index=True)
    name = Column(Text, default=None)  # an appstream_id
    size = Column(Integer, default=0)
    entropy = Column(Float, default=0.0)

    checksums = relationship(
        "ComponentShardChecksum",
        back_populates="shard",
        cascade="all,delete,delete-orphan",
    )
    certificates = relationship(
        "ComponentShardCertificate",
        order_by="desc(ComponentShardCertificate.component_shard_certificate_id)",
        back_populates="shard",
        cascade="all,delete,delete-orphan",
    )
    info = relationship("ComponentShardInfo", back_populates="shards")
    yara_query_results = relationship(
        "YaraQueryResult", cascade="all,delete,delete-orphan"
    )

    md = relationship("Component", back_populates="shards")
    attributes = relationship(
        "ComponentShardAttribute",
        back_populates="component_shard",
        cascade="all,delete,delete-orphan",
    )

    def get_attr_value(self, key: str) -> Optional[str]:
        for attr in self.attributes:
            if attr.key == key:
                return attr.value
        return None

    @property
    def description(self) -> Optional[str]:
        if self.info.description:
            return self.info.description
        if self.name.endswith("Pei"):
            return "The Pre-EFI Initialization phase is invoked early in the boot flow."
        if self.name.endswith("Dxe"):
            return "The Driver Execution Environment phase is where most of the system \
                    initialization is performed."
        return None

    @property
    def blob(self) -> Optional[bytes]:
        if not hasattr(self, "_blob"):
            # restore from disk if available
            fn = self.absolute_path
            if not os.path.exists(fn):
                return None
            with open(fn, "rb") as f:
                self._blob = zlib.decompressobj().decompress(f.read())
        return self._blob

    @blob.setter
    def blob(self, value: bytes) -> None:
        self._blob = value

    @property
    def checksum(self) -> Optional[str]:
        for csum in self.checksums:
            if csum.kind == "SHA256":
                return csum.value
        return None

    def set_blob(self, value: bytes, checksums: Optional[List[str]] = None) -> None:
        """Set data blob and add checksum objects"""
        self._blob = value
        self.size = len(value)
        self.entropy = _calculate_entropy(value)

        # default fallback
        if not checksums:
            checksums = ["SHA1", "SHA256"]

        # SHA1 is what's used by researchers, but considered broken
        if "SHA1" in checksums:
            csum = ComponentShardChecksum(
                value=hashlib.sha1(value).hexdigest(), kind="SHA1"
            )
            self.checksums.append(csum)

        # SHA256 is now the best we have
        if "SHA256" in checksums:
            csum = ComponentShardChecksum(
                value=hashlib.sha256(value).hexdigest(), kind="SHA256"
            )
            self.checksums.append(csum)

        # SHA384 used in Intel BootGuard
        if "SHA384" in checksums:
            csum = ComponentShardChecksum(
                value=hashlib.sha384(value).hexdigest(), kind="SHA384"
            )
            self.checksums.append(csum)

    @property
    def absolute_path(self) -> str:

        # check the legacy location first, then use the modern one
        md_path = os.path.join(app.config["SHARD_DIR"], str(self.component_id))
        if not self.checksum or os.path.exists(os.path.join(md_path, self.name)):
            return os.path.join(md_path, self.name)
        return os.path.join(md_path, self.checksum)

    def save(self) -> None:
        fn = self.absolute_path
        os.makedirs(os.path.dirname(fn), exist_ok=True)
        with open(fn, "wb") as f:
            f.write(zlib.compress(self._blob))

    def __repr__(self) -> str:
        return "ComponentShard object {}:{}".format(self.guid, self.name)


class ComponentIssue(db.Model):  # type: ignore

    __tablename__ = "component_issues"

    component_issue_id = Column(Integer, primary_key=True)
    component_id = Column(
        Integer, ForeignKey("components.component_id"), nullable=False, index=True
    )
    kind = Column(Text, nullable=False)
    value = Column(Text, nullable=False)
    description = Column(Text, nullable=True)
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=True)
    published = Column(DateTime, default=None, nullable=True)
    timestamp = Column(DateTime, default=datetime.datetime.utcnow, nullable=True)

    md = relationship("Component", back_populates="issues")
    user = relationship("User", foreign_keys=[user_id])

    ISSUE_KINDS = ["cve", "intel", "lenovo", "dell", "vince"]

    @property
    def url(self) -> Optional[str]:
        if self.kind == "cve":
            return "https://nvd.nist.gov/vuln/detail/{}".format(self.value)
        if self.kind == "vince":
            return "https://www.kb.cert.org/vuls/id/{}".format(self.value)
        if self.kind == "dell":
            # you have to search for the issue number yourself...
            return "https://www.dell.com/support/security/en-us/"
        if self.kind == "lenovo":
            return "https://support.lenovo.com/us/en/product_security/{}".format(
                self.value
            )
        if self.kind == "intel" and self.value.startswith("INTEL-SA-"):
            return "https://www.intel.com/content/www/us/en/security-center/advisory/{}".format(
                self.value
            )
        return None

    @property
    def value_display(self) -> str:
        if self.kind == "vince":
            return "VU#{}".format(self.value)
        return self.value

    @property
    def release_delta(self) -> Optional[int]:
        """the difference between the CVE release date and the firmware release date, in days"""
        if not self.published:
            return None
        return (self.md.fw.release_ts - self.published).days

    @property
    def problem(self) -> Optional[Claim]:
        if self.kind == "cve":
            parts = self.value.split("-")
            if len(parts) != 3 or parts[0] != "CVE":
                return Claim(
                    kind="invalid-issue",
                    icon="warning",
                    summary="Invalid component issue",
                    description="Format expected to be CVE-XXXX-XXXXX",
                )
            if not parts[1].isnumeric() or int(parts[1]) < 1995:
                return Claim(
                    kind="invalid-issue",
                    icon="warning",
                    summary="Invalid component issue",
                    description="Invalid year in CVE value",
                )
            if not parts[2].isnumeric():
                return Claim(
                    kind="invalid-issue",
                    icon="warning",
                    summary="Invalid component issue",
                    description="Expected integer in CVE token",
                )
            return None
        if self.kind == "dell":
            parts = self.value.split("-")
            if len(parts) != 3 or parts[0] != "DSA":
                return Claim(
                    kind="invalid-issue",
                    icon="warning",
                    summary="Invalid component issue",
                    description="Format expected to be DSA-XXXX-XXX",
                )
            if not parts[1].isnumeric or int(parts[1]) < 1995:
                return Claim(
                    kind="invalid-issue",
                    icon="warning",
                    summary="Invalid component issue",
                    description="Invalid year in DSA value",
                )
            if not parts[2].isnumeric():
                return Claim(
                    kind="invalid-issue",
                    icon="warning",
                    summary="Invalid component issue",
                    description="Expected integer in DSA token",
                )
            return None
        if self.kind == "vince":
            parts = self.value.split("-")
            if len(parts) > 1 or len(parts[0]) < 6 or not parts[0].isnumeric():
                return Claim(
                    kind="invalid-issue",
                    icon="warning",
                    summary="Invalid component issue",
                    description="Format expected to be 123456, got {}".format(parts[0]),
                )
            return None
        if self.kind == "lenovo":
            parts = self.value.split("-")
            if len(parts) != 2 or parts[0] != "LEN":
                return Claim(
                    kind="invalid-issue",
                    icon="warning",
                    summary="Invalid component issue",
                    description="Format expected to be LEN-XXXXX",
                )
            if not parts[1].isnumeric():
                return Claim(
                    kind="invalid-issue",
                    icon="warning",
                    summary="Invalid component issue",
                    description="Expected integer in LEN token",
                )
            return None
        if self.kind == "intel":
            parts = self.value.split("-")
            if len(parts) != 3 or parts[0] != "INTEL" or parts[1] not in ["SA", "TA"]:
                return Claim(
                    kind="invalid-issue",
                    icon="warning",
                    summary="Invalid component issue",
                    description="Format expected to be INTEL-XA-XXXXX",
                )
            if not parts[2].isnumeric():
                return Claim(
                    kind="invalid-issue",
                    icon="warning",
                    summary="Invalid component issue",
                    description="Expected integer in INTEL-XA token",
                )
            return None
        return Claim(
            kind="invalid-issue",
            icon="warning",
            summary="Invalid component kind",
            description="Issue kind {} not supported".format(self.kind),
        )

    def __repr__(self) -> str:
        return "<ComponentIssue {}>".format(self.value_display)


def _is_valid_url(url: str) -> bool:
    if not url.startswith("https://") and not url.startswith("http://"):
        return False
    return True


class ComponentClaim(db.Model):  # type: ignore

    __tablename__ = "component_claims"

    component_claim_id = Column(Integer, primary_key=True)
    component_id = Column(
        Integer, ForeignKey("components.component_id"), nullable=False, index=True
    )
    claim_id = Column(
        Integer, ForeignKey("claims.claim_id"), nullable=False, index=True
    )

    md = relationship("Component", back_populates="component_claims")
    claim = relationship("Claim")

    def __repr__(self) -> str:
        return "<ComponentClaim {}>".format(self.component_claim_id)


class ComponentRef(db.Model):  # type: ignore

    __tablename__ = "component_refs"

    component_ref_id = Column(Integer, primary_key=True)
    component_id = Column(Integer, ForeignKey("components.component_id"), index=True)
    vendor_id = Column(
        Integer, ForeignKey("vendors.vendor_id"), nullable=False, index=True
    )
    vendor_id_partner = Column(
        Integer, ForeignKey("vendors.vendor_id"), nullable=False, index=True
    )
    protocol_id = Column(Integer, ForeignKey("protocol.protocol_id"))
    appstream_id = Column(Text, default=None)
    version = Column(Text, nullable=False)
    release_tag = Column(Text, default=None)
    date = Column(DateTime, default=None)
    name = Column(Text, nullable=False)
    url = Column(Text, default=None)
    status = Column(Text)

    md = relationship("Component")
    vendor = relationship("Vendor", foreign_keys=[vendor_id])
    vendor_partner = relationship(
        "Vendor", foreign_keys=[vendor_id_partner], back_populates="mdrefs"
    )
    protocol = relationship("Protocol")

    def __lt__(self, other: Any) -> bool:
        return vercmp(self.version, other.version) < 0

    def __eq__(self, other: Any) -> bool:
        return vercmp(self.version, other.version) == 0

    @property
    def version_with_tag(self) -> str:
        if self.release_tag:
            return "{} ({})".format(self.release_tag, self.version)
        return self.version

    @property
    def icon_name(self) -> Optional[str]:
        if self.status == "private":
            return "lock"
        if self.status == "testing":
            return "vial"
        if self.status == "stable":
            return "globe-europe"
        if self.status == "trash":
            return "trash"
        if self.status == "embargo":
            return "user-lock"
        return None

    def __repr__(self) -> str:
        return "<ComponentRef {}>".format(self.version)


class ComponentDescription(db.Model):  # type: ignore

    __tablename__ = "component_translations"

    component_description_id = Column(Integer, primary_key=True)
    component_id = Column(
        Integer, ForeignKey("components.component_id"), nullable=False, index=True
    )
    user_id = Column(Integer, ForeignKey("users.user_id"), default=None)
    ctime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    value = Column(Text, default=None)  # text
    locale = Column(Text, default=None)  # en_GB, None == en_US

    __table_args__ = (
        UniqueConstraint("component_id", "locale", name="uq_md_kind_locale"),
    )

    md = relationship("Component")
    user = relationship("User")

    @property
    def locale_with_fallback(self) -> str:
        return self.locale or "en_US"

    def __lt__(self, other: Any) -> bool:
        return vercmp(self.locale, other.locale) < 0

    def __eq__(self, other: Any) -> bool:
        return vercmp(self.locale, other.locale) == 0

    def __repr__(self) -> str:
        return "<ComponentDescription {}({}:{})>".format(
            self.kind, self.locale, self.value
        )


class Component(db.Model):  # type: ignore

    __tablename__ = "components"

    _blob: Optional[bytes] = None

    component_id = Column(Integer, primary_key=True)
    firmware_id = Column(
        Integer, ForeignKey("firmware.firmware_id"), nullable=False, index=True
    )
    protocol_id = Column(Integer, ForeignKey("protocol.protocol_id"))
    category_id = Column(Integer, ForeignKey("categories.category_id"))
    checksum_contents_sha1 = Column(String(40), default=None)
    checksum_contents_sha256 = Column(String(64), default=None)
    appstream_id = Column(Text, nullable=False, index=True)
    appstream_type = Column(Text, default=None)
    branch = Column(Text, default=None)
    name = Column(Text, default=None)
    name_variant_suffix = Column(Text, default=None)
    summary = Column(Text, default=None)
    icon = Column(Text, default=None)
    description = Column(Text, default=None)  # markdown format
    details_url = Column(Text, default=None)
    source_url = Column(Text, default=None)
    url_homepage = Column(Text, default=None)
    metadata_license_id = Column(Integer, ForeignKey("licenses.license_id"))
    project_license_id = Column(Integer, ForeignKey("licenses.license_id"))
    filename_contents = Column(Text, default=None)
    filename_xml = Column(Text, nullable=False)
    release_timestamp = Column(Integer, default=0)
    version = Column(Text, default=None)
    release_installed_size = Column(Integer, default=0)
    release_download_size = Column(Integer, default=0)
    release_urgency = Column(Text, default=None)
    release_tag = Column(Text, default=None)
    release_message = Column(Text, default=None)  # LVFS::UpdateMessage
    release_image = Column(Text, default=None)  # LVFS::UpdateImage
    release_image_safe = Column(Text, default=None)
    screenshot_url = Column(Text, default=None)
    screenshot_url_safe = Column(Text, default=None)
    screenshot_caption = Column(Text, default=None)
    inhibit_download = Column(Boolean, default=False)
    verfmt_id = Column(Integer, ForeignKey("verfmts.verfmt_id"))
    priority = Column(Integer, default=0)
    install_duration = Column(Integer, default=0)
    device_integrity = Column(Text, default=None)  # LVFS::DeviceIntegrity

    fw = relationship("Firmware", back_populates="mds", lazy="joined")
    descriptions = relationship(
        "ComponentDescription", back_populates="md", cascade="all,delete,delete-orphan"
    )
    requirements = relationship(
        "ComponentRequirement", back_populates="md", cascade="all,delete,delete-orphan"
    )
    issues = relationship(
        "ComponentIssue", back_populates="md", cascade="all,delete,delete-orphan"
    )
    component_claims = relationship(
        "ComponentClaim", back_populates="md", cascade="all,delete,delete-orphan"
    )
    issue_values = association_proxy("issues", "value")
    device_checksums = relationship(
        "ComponentChecksum", back_populates="md", cascade="all,delete,delete-orphan"
    )
    guids = relationship(
        "ComponentGuid",
        back_populates="md",
        order_by="asc(ComponentGuid.guid_id)",
        cascade="all,delete,delete-orphan",
    )
    guid_values = association_proxy("guids", "value")
    tags = relationship(
        "ComponentTag",
        back_populates="md",
        order_by="asc(ComponentTag.tag_id)",
        cascade="all,delete,delete-orphan",
    )
    tag_values = association_proxy("tags", "value")
    shards = relationship(
        "ComponentShard",
        order_by="desc(ComponentShard.component_shard_id)",
        back_populates="md",
        cascade="all,delete,delete-orphan",
    )
    keywords = relationship(
        "ComponentKeyword", back_populates="md", cascade="all,delete,delete-orphan"
    )
    swids = relationship(
        "ComponentSwid", back_populates="md", cascade="all,delete,delete-orphan"
    )
    mdrefs = relationship(
        "ComponentRef", back_populates="md", cascade="all,delete,delete-orphan"
    )
    protocol = relationship("Protocol", foreign_keys=[protocol_id])
    category = relationship("Category", foreign_keys=[category_id])
    project_license = relationship("License", foreign_keys=[project_license_id])
    metadata_license = relationship("License", foreign_keys=[metadata_license_id])
    verfmt = relationship("Verfmt", foreign_keys=[verfmt_id])
    yara_query_results = relationship(
        "YaraQueryResult", cascade="all,delete,delete-orphan"
    )

    def __lt__(self, other: Any) -> bool:
        return vercmp(self.version_display, other.version_display) < 0

    def __eq__(self, other: Any) -> bool:
        return vercmp(self.version_display, other.version_display) == 0

    @property
    def release_dt(self) -> Any:
        return datetime.datetime.fromtimestamp(self.release_timestamp)

    def _vendor_tag_with_attr(self, attr: Optional[str] = None) -> Optional[VendorTag]:

        # no vendor
        if not self.fw.vendor:
            return None

        # category match
        if self.category:
            for tag in self.fw.vendor.tags:
                if attr and not getattr(tag, attr):
                    continue
                if tag.category_id == self.category_id:
                    return tag
                if (
                    self.category.fallback
                    and tag.category_id == self.category.fallback.category_id
                ):
                    return tag

        # the 'any' category
        for tag in self.fw.vendor.tags:
            if attr and not getattr(tag, attr):
                continue
            if not tag.category:
                return tag

        # failed
        return None

    def get_description_by_locale(
        self,
        locale: Optional[str] = None,
    ) -> Optional[ComponentDescription]:
        for tx in self.descriptions:
            if tx.locale == locale:
                return tx
        return None

    @property
    def needs_issues(self) -> bool:

        # already got
        if self.issues:
            return False

        # check for the magic words in the update description
        tx = self.get_description_by_locale()
        if not tx:
            return False
        if not tx.value:
            return False
        if "security".casefold() in tx.value.casefold():
            return True
        return False

    @property
    def vendor_tag(self) -> Optional[VendorTag]:
        return self._vendor_tag_with_attr()

    @property
    def icon_with_fallback(self) -> Optional[str]:
        if self.icon:
            return self.icon
        if self.protocol and self.protocol.icon:
            return self.protocol.icon
        if self.category and self.category.icon:
            return self.category.icon
        if self.category and self.category.fallback and self.category.fallback.icon:
            return self.category.fallback.icon
        return None

    @property
    def product(self) -> Optional[Product]:
        if not self.appstream_id:
            return None
        try:
            return (
                db.session.query(Product)
                .filter(Product.appstream_id == self.appstream_id)
                .first()
            )
        except OperationalError:
            return None

    @property
    def is_eol(self) -> bool:
        product = self.product
        if not product:
            return False
        return product.state == Product.STATE_EOL

    @property
    def details_url_with_fallback(self) -> Optional[str]:

        # set explicitly
        if self.details_url:
            return self.details_url

        # get tag for category
        tag = self._vendor_tag_with_attr("details_url")
        if not tag:
            return None

        # return with string substitutions; if not set then return invalid
        details_url = tag.details_url
        replacements: Dict[str, Optional[str]] = {
            "$RELEASE_TAG$": self.release_tag,
            "$VERSION$": self.version_display,
        }
        for key, value in replacements.items():
            if details_url.find(key) != -1:
                if not value:
                    return None
                details_url = details_url.replace(key, value)

        # success
        return details_url

    @property
    def blob(self) -> Optional[bytes]:
        if not hasattr(self, "_blob") or not self._blob:
            self._blob = None
            self.fw._ensure_blobs()
        return self._blob

    @blob.setter
    def blob(self, value: Optional[bytes]) -> None:
        self._blob = value

    @property
    def names(self) -> Optional[List[str]]:
        if not self.name:
            return None
        return self.name.split("/")

    @property
    def appstream_id_prefix(self) -> str:
        sections = self.appstream_id.split(".")
        for idx, section in enumerate(sections):
            if idx == 0:
                continue
            if section in ["com", "ru", "org", "net", "co", "uk", "ua", "ca", "tw"]:
                continue
            return ".".join(sections[: idx + 1])
        return self.appstream_id

    @property
    def certificates(self) -> List[ComponentShardCertificate]:
        certs: List[ComponentShardCertificate] = []
        for shard in self.shards:
            certs.extend(shard.certificates)
        return certs

    def shard_by_guid(self, guid: str) -> List[ComponentShard]:
        shards: List[ComponentShard] = []
        for shard in self.shards:
            if shard.guid == guid:
                shards.append(shard)
        return shards

    @property
    def name_with_category(self) -> str:
        name = self.name
        if self.name_variant_suffix:
            name += " (" + self.name_variant_suffix + ")"
        if self.category:
            if self.category.name:
                name += " " + self.category.name
            else:
                name += " " + self.category.value
        return name

    @property
    def name_with_vendor(self) -> str:
        name = self.fw.vendor.display_name + " " + self.name
        if self.name_variant_suffix:
            name += " (" + self.name_variant_suffix + ")"
        return name

    @property
    def claims(self) -> List[Claim]:
        return [component_claim.claim for component_claim in self.component_claims]

    @property
    def autoclaims(self) -> List[Claim]:
        claims: List[Claim] = []
        if self.protocol:
            if self.protocol.is_signed or (
                self.protocol.is_transport and self.device_integrity == "signed"
            ):
                claims.append(
                    Claim(
                        kind="signed-firmware",
                        icon="success",
                        summary="Update is cryptographically signed",
                        url="https://lvfs.readthedocs.io/en/latest/claims.html#signed-firmware",
                    )
                )
            else:
                claims.append(
                    Claim(
                        kind="no-signed-firmware",
                        icon="warning",
                        summary="Update is not cryptographically signed",
                        url="https://lvfs.readthedocs.io/en/latest/claims.html#signed-firmware",
                    )
                )
            if self.protocol.can_verify:
                claims.append(
                    Claim(
                        kind="verify-firmware",
                        icon="success",
                        summary="Firmware can be verified after flashing",
                        url="https://lvfs.readthedocs.io/en/latest/claims.html#verified-firmware",
                    )
                )
                if self.category and self.category.expect_device_checksum:
                    if self.device_checksums:
                        claims.append(
                            Claim(
                                kind="device-checksum",
                                icon="success",
                                summary="Firmware has attestation checksums",
                                url="https://lvfs.readthedocs.io/en/latest/claims.html#device-checksums",
                            )
                        )
                    else:
                        claims.append(
                            Claim(
                                kind="no-device-checksum",
                                icon="warning",
                                summary="Firmware has no attestation checksums",
                                url="https://lvfs.readthedocs.io/en/latest/claims.html#device-checksums",
                            )
                        )
            else:
                claims.append(
                    Claim(
                        kind="no-verify-firmware",
                        icon="warning",
                        summary="Firmware cannot be verified after flashing",
                        url="https://lvfs.readthedocs.io/en/latest/claims.html#verified-firmware",
                    )
                )
        if self.swids:
            claims.append(
                Claim(
                    kind="swids",
                    icon="success",
                    summary="Firmware has SBoM",
                    url="https://lvfs.readthedocs.io/en/latest/claims.html#software-bill-of-materials",
                )
            )
        else:
            claims.append(
                Claim(
                    kind="no-swids",
                    icon="warning",
                    summary="Firmware has no detected SBoM",
                    url="https://lvfs.readthedocs.io/en/latest/claims.html#software-bill-of-materials",
                )
            )
        if self.checksum_contents_sha1 and not self.fw.vendor.is_community:
            claims.append(
                Claim(
                    kind="vendor-provenance",
                    icon="success",
                    summary="Added to the LVFS by {}".format(
                        self.fw.vendor.display_name
                    ),
                    url="https://lvfs.readthedocs.io/en/latest/claims.html#vendor-provenance",
                )
            )
        if self.source_url:
            claims.append(
                Claim(
                    kind="source-url",
                    icon="success",
                    summary="Source code available",
                    url="https://lvfs.readthedocs.io/en/latest/claims.html#source-url",
                )
            )
        if self.is_eol:
            claims.append(
                Claim(
                    kind="is-eol",
                    icon="warning",
                    summary="Vendor considers this device end of life",
                    url="https://lvfs.readthedocs.io/en/latest/claims.html#end-of-life",
                )
            )
        return claims

    @property
    def security_level(self) -> int:
        claims: Dict[str, Claim] = {}
        for claim in self.autoclaims:
            claims[claim.kind] = claim
        if "signed-firmware" in claims and "device-checksum" in claims:
            return 2
        if "signed-firmware" in claims:
            return 1
        return 0

    @property
    def requires_source_url(self) -> bool:
        if self.project_license:
            return self.project_license.requires_source
        return False

    @property
    def version_with_tag(self) -> Optional[str]:
        if self.release_tag:
            return "{} ({})".format(self.release_tag, self.version_display)
        return self.version_display

    @property
    def version_display(self) -> Optional[str]:
        if self.version and self.version.isdigit():
            if self.verfmt:
                tmp = self.verfmt._uint32_to_str(int(self.version))
                if tmp:
                    return tmp
        return self.version

    @property
    def version_sections(self) -> int:
        if not self.version_display:
            return 0
        return len(self.version_display.split("."))

    @property
    def _problems_for_descriptions(self) -> List[Claim]:

        # verify update description
        problems = []
        for tx in self.descriptions:
            from lvfs.util import _xml_from_markdown
            from lvfs.claims.utils import _get_update_description_problems

            root = _xml_from_markdown(tx.value)
            if root is None:
                problems.append(
                    Claim(
                        kind="no-release-description",
                        icon="warning",
                        summary="Release description for locale {} is missing".format(
                            tx.locale
                        ),
                        description="All components should have a suitable update "
                        "description before a firmware is moved to stable.\n"
                        "These can also can be set in the .metainfo.xml file.",
                    )
                )
                continue
            for problem in _get_update_description_problems(root):
                problem.summary += " ({})".format(tx.locale_with_fallback)
                problems.append(problem)

            # check for OEMs just pasting in the XML like before
            for element_name in ["p", "li", "ul", "ol"]:
                if tx.value.find("<" + element_name + ">") != -1:
                    problems.append(
                        Claim(
                            kind="invalid-release-description",
                            icon="warning",
                            summary="No valid update description ({})".format(
                                tx.locale_with_fallback
                            ),
                            description="Release description cannot contain XML markup",
                        )
                    )
                    break

            # check for update message in release notes
            if tx.value and self.protocol and self.protocol.update_message:
                for action_text in [
                    "do not turn off your computer",
                    "remove the ac adaptor",
                    "will be restarted automatically",
                    "next time the dock usb cable is unplugged",
                ]:
                    if tx.value.lower().find(action_text) != -1:
                        problems.append(
                            Claim(
                                kind="invalid-release-description",
                                icon="warning",
                                summary="Invalid update description detected",
                                description="The protocol already specifies an update message of '{}'. "
                                "Please remove the duplicate message from the update description.".format(
                                    self.protocol.update_message
                                ),
                            )
                        )
                        break

            # check for unwanted text
            for action_text in [
                "supported product scope",
                "supported devices and firmware version",
            ]:
                if tx.value and tx.value.lower().find(action_text) != -1:
                    problems.append(
                        Claim(
                            kind="invalid-release-description",
                            icon="warning",
                            summary="Invalid update description detected",
                            description="The text should not contain '{}' as per the style guide.".format(
                                action_text
                            ),
                        )
                    )

        if not self.descriptions:
            problems.append(
                Claim(
                    kind="no-release-description",
                    icon="warning",
                    summary="Release description is missing",
                    description="All components should have a suitable update "
                    "description before a firmware is moved to stable.\n"
                    "Writing good quality release notes are really important "
                    "as some users may be worried about an update that does "
                    "not explain what it fixes.\n"
                    "This also can be set in the .metainfo.xml file.",
                )
            )
        return problems

    @property
    def _problems_for_firmware(self) -> List[Claim]:

        # verify update description
        problems = self._problems_for_descriptions

        # enforce the VersionFormat if the version is an integer
        if self.version:
            if self.version.isdigit() and not self.verfmt:
                problems.append(
                    Claim(
                        kind="missing-version-format",
                        icon="warning",
                        summary="LVFS::VersionFormat is required for integer version",
                        description="The version format has to be set if the version is ambiguous.",
                        url=url_for(
                            "components.route_show", component_id=self.component_id
                        ),
                    )
                )

        # urgency is now a hard requirement
        if not self.release_urgency or self.release_urgency == "unknown":
            problems.append(
                Claim(
                    kind="no-release-urgency",
                    icon="warning",
                    summary="Release urgency has not been set",
                    description="All components should have an appropriate "
                    "update urgency before a firmware is moved to stable.\n"
                    "This also can be set in the .metainfo.xml file.",
                )
            )

        # release timestamp is now a hard requirement
        if self.release_timestamp == 0:
            problems.append(
                Claim(
                    kind="no-release-timestamp",
                    icon="warning",
                    summary="Release timestamp was not set",
                    description="All components should have an appropriate "
                    "update timestamp.\n"
                    "This also can be set in the .metainfo.xml file.",
                )
            )

        # release timestamp has to also be sane unless it has ever been public
        elif (
            not app.config.get("DEBUG", None)
            and not self.fw.release_ts
            and (
                self.release_dt
                < datetime.datetime.today() - datetime.timedelta(days=5 * 365)
                or self.release_dt
                > datetime.datetime.today() + datetime.timedelta(days=365)
            )
        ):
            problems.append(
                Claim(
                    kind="invalid-release-timestamp",
                    icon="warning",
                    summary="Release timestamp {} is not valid".format(
                        self.release_dt.strftime("%Y-%m-%d")
                    ),
                    description="All components should have an appropriate "
                    "update timestamp. Updates can be backdated up to a limit "
                    "of 5 years and post-dated up to 1 year.",
                )
            )

        # we are going to be making policy decision on this soon
        if not self.protocol or self.protocol.value == "unknown":
            problems.append(
                Claim(
                    kind="no-protocol",
                    icon="warning",
                    summary="Update protocol has not been set",
                    description="All components should have a defined update protocol "
                    "before being moved to stable.\n"
                    "This also can be set in the .metainfo.xml file.",
                    url=url_for(
                        "components.route_show", component_id=self.component_id
                    ),
                )
            )
        elif (
            self.release_message or self.release_image
        ) and not self.protocol.allow_custom_update_message:
            problems.append(
                Claim(
                    kind="update-message-invalid-for-protocol",
                    icon="warning",
                    summary="An update message has been set",
                    description="The protocol {} does not allow a release message or image to be shown.".format(
                        self.protocol.value
                    ),
                    url=url_for(
                        "components.route_show", component_id=self.component_id
                    ),
                )
            )

        # check the GUIDs are valid
        for guid in self.guids:
            from lvfs.util import _validate_guid

            if not _validate_guid(guid.value):
                problems.append(
                    Claim(
                        kind="invalid-guid",
                        icon="warning",
                        summary="Component GUID invalid",
                        description="GUID {} is not valid".format(guid.value),
                        url=url_for(
                            "components.route_show", component_id=self.component_id
                        ),
                    )
                )
            elif guid.value in [
                "230c8b18-8d9b-53ec-838b-6cfc0383493a",  # main-system-firmware
                "f15aa55c-9cd5-5942-85ae-a6bf8740b96c",  # MST-panamera
                "d6072785-6fc0-5f83-9d49-11376e7f48b1",  # MST-leaf
                "49ec4eb4-c02b-58fc-8935-b1ee182405c7",  # MST-tesla
            ]:
                problems.append(
                    Claim(
                        kind="invalid-guid",
                        icon="warning",
                        summary="Component GUID is too generic",
                        description="GUID {} is not valid".format(guid.value),
                        url=url_for(
                            "components.route_show", component_id=self.component_id
                        ),
                    )
                )

        # check CHIDs are in place where the GUID is shared between vendors
        for guid in self.guids:
            if guid.value in [
                "19aef08f-2701-4db3-b90b-d4bc1412ade7",  # TGL-LP Consumer
                "9ef0a03e-25ed-4ab4-9b66-077ca06c9506",  # TGL-LP Corporate
                "d1144042-2deb-410c-a7cc-6333c352a976",  # TGL—H Consumer
                "1d9282eb-f2bf-4c75-8307-028a34978eb9",  # TGL-H Corporate
                "23192307-d667-4bdf-af1a-6059db171246",  # ADL-LP Consumer
                "4e78ce68-5389-4a95-bf10-e3568c30caf8",  # ADL-LP Corporate
                "7aa69739-8f78-41cb-bf44-854e2cb516bd",  # ADL-H Consumer
                "347efe23-9f9a-4b26-b4bd-e2414872dd14",  # ADL-H Corporate
            ]:
                if not self.find_req("hardware"):
                    problems.append(
                        Claim(
                            kind="hardware-requirement-required",
                            icon="warning",
                            summary="CHID hardware requirement is required",
                            description="The GUID {} is shared between multiple vendors "
                            "and so the firmware must have a CHID hardware requirement "
                            "to lock it to a specific OEM.".format(guid.value),
                            url=url_for(
                                "components.route_show", component_id=self.component_id
                            ),
                        )
                    )

        # verfmt is now a hard requirement for stable firmware
        if not self.verfmt:
            problems.append(
                Claim(
                    kind="no-verfmt",
                    icon="warning",
                    summary="Version format has not been set",
                    description="All components should have a defined version format "
                    "before being moved to stable.\n"
                    "This also can be set in the .metainfo.xml file.",
                    url=url_for(
                        "components.route_show", component_id=self.component_id
                    ),
                )
            )

        # does any other firmware have this <id> and a different verfmt?
        else:
            other_mds = (
                db.session.query(Component)
                .filter(Component.component_id != self.component_id)
                .filter(Component.appstream_id == self.appstream_id)
                .filter(Component.verfmt_id != self.verfmt_id)
                .filter(Component.verfmt_id != None)
                .order_by(Component.release_timestamp.desc())
            )
            for other_md in other_mds:
                if not other_md.fw.remote.is_signed:
                    continue
                problems.append(
                    Claim(
                        kind="conflicting-verfmt",
                        icon="warning",
                        allow_embargo=False,
                        summary="The firmware version format is inconsistent",
                        description="Firmware version {} already exists on the LVFS with version format {}. "
                        "The version format cannot change within a specific firmware stream.".format(
                            other_md.version_display, other_md.verfmt.name
                        ),
                        url=url_for(
                            "firmware.route_show", firmware_id=other_md.fw.firmware_id
                        ),
                    )
                )

        # check the version matches the expected section count
        if self.verfmt and self.verfmt.value != "plain" and self.verfmt.sections:
            if self.version_sections != self.verfmt.sections:
                problems.append(
                    Claim(
                        kind="invalid-version-for-format",
                        icon="warning",
                        summary="Version format invalid",
                        description="The version number {} incompatible with {}.".format(
                            self.version_display, self.verfmt.value
                        ),
                        url=url_for(
                            "components.route_show", component_id=self.component_id
                        ),
                    )
                )

        # firmware can't be pushed to public with a private protocol
        if self.protocol and not self.protocol.is_public:
            problems.append(
                Claim(
                    kind="no-protocol",
                    icon="warning",
                    summary="Update protocol is not public",
                    url=url_for(
                        "components.route_show", component_id=self.component_id
                    ),
                )
            )

        # requires a [optionally signed] report
        if (
            self.protocol
            and self.protocol.require_report
            and self.fw.report_success_cnt == 0
        ):
            problems.append(
                Claim(
                    kind="require-report",
                    icon="warning",
                    summary="Firmware protocol requires fwupd report",
                    description="This firmware requires the uploading ODM or responsible OEM to "
                    "upload a fwupd success report from the target hardware.",
                    url="https://lvfs.readthedocs.io/en/latest/testing.html#signed-reports",
                )
            )

        # some firmware requires a source URL
        if self.requires_source_url and not self.source_url:
            problems.append(
                Claim(
                    kind="no-source",
                    icon="warning",
                    summary="Update does not link to source code",
                    description="Firmware that uses the GNU General Public License has to "
                    "provide a link to the source code used to build the firmware.",
                    url=url_for(
                        "components.route_show",
                        component_id=self.component_id,
                        page="update",
                    ),
                )
            )

        # the URL has to be valid if provided
        if self.details_url and not _is_valid_url(self.details_url):
            problems.append(
                Claim(
                    kind="invalid-details-url",
                    icon="warning",
                    summary="The update details URL was provided but not valid",
                    url=url_for(
                        "components.route_show",
                        page="update",
                        component_id=self.component_id,
                    ),
                )
            )
        if self.source_url and not _is_valid_url(self.source_url):
            problems.append(
                Claim(
                    kind="invalid-source-url",
                    icon="warning",
                    summary="The release source URL was provided but not valid",
                    url=url_for(
                        "components.route_show",
                        page="update",
                        component_id=self.component_id,
                    ),
                )
            )

        # invalid project license
        if self.project_license:
            if self.project_license.is_content:
                problems.append(
                    Claim(
                        kind="project-license-invalid",
                        icon="warning",
                        summary="Invalid project license",
                        description="The component requires a valid license, e.g. GPL-2.0+",
                        url=url_for(
                            "components.route_show", component_id=self.component_id
                        ),
                    )
                )
        else:
            problems.append(
                Claim(
                    kind="project-license-invalid",
                    icon="warning",
                    summary="Missing project license",
                    description="The component requires a project license, e.g. GPL-2.0+",
                    url=url_for(
                        "components.route_show", component_id=self.component_id
                    ),
                )
            )

        # requires itself exactly
        req = None
        for guid in self.guid_values:
            req = self.find_req("firmware", guid)
        if req and req.compare == "eq":
            rc = vercmp(self.version_display, req.version)
            if rc == 0:
                problems.append(
                    Claim(
                        kind="requirement-invalid",
                        icon="warning",
                        summary="Requirement was invalid",
                        description="The existing firmware version requirement "
                        "is the same as the uploaded version.",
                        url=url_for(
                            "components.route_show", component_id=self.component_id
                        ),
                    )
                )

        # requires itself with a higher version than provided
        if req and req.compare in ["gt", "ge"]:
            rc = vercmp(self.version_display, req.version)
            if rc < 0:
                problems.append(
                    Claim(
                        kind="requirement-invalid",
                        icon="warning",
                        summary="Requirement was invalid",
                        description="The existing firmware version requirement "
                        "is higher than the uploaded version.",
                        url=url_for(
                            "components.route_show", component_id=self.component_id
                        ),
                    )
                )

        # add all CVE problems
        for issue in self.issues:
            if issue.problem:
                problems.append(issue.problem)

        # done
        return problems

    @property
    def problems(self) -> List[Claim]:

        problems = []

        # specific to type="firmware"
        if not self.appstream_type or self.appstream_type == "firmware":
            problems.extend(self._problems_for_firmware)

        # a suitable AppStream ID
        if self.appstream_id:
            if not _validate_appstream_id(self.appstream_id):
                problems.append(
                    Claim(
                        kind="invalid-appstream-id",
                        icon="warning",
                        allow_embargo=False,
                        summary="<id> contains invalid characters",
                        description="The AppStream ID has to be unique for each device "
                        "firmware stream as it used to combine separate <release> tags.",
                        url=url_for(
                            "components.route_show", component_id=self.component_id
                        ),
                    )
                )
            if len(self.appstream_id.split(".")) < 4:
                problems.append(
                    Claim(
                        kind="invalid-appstream-id",
                        icon="warning",
                        allow_embargo=False,
                        summary="<id> Should contain at least 4 sections to identify the model",
                        description="The AppStream ID has to be unique for each device firmware "
                        "stream as it used to combine <release> tags into components.",
                        url=url_for(
                            "components.route_show", component_id=self.component_id
                        ),
                    )
                )

        # use categories instead
        if self.name:
            category: Dict[str, List[str]] = {
                "system": ["X-System"],
                "device": ["X-Device"],
                "bios": ["X-System"],
                "me": [
                    "X-ManagementEngine",
                    "X-CorporateManagementEngine",
                    "X-ConsumerManagementEngine",
                ],
                "embedded": ["X-EmbeddedController"],
                "controller": ["X-EmbeddedController"],
                "microcode": ["X-CpuMicrocode"],
                "dock": ["X-Dock", "X-UsbDock"],
            }
            words = [word.lower() for word in self.name.split(" ")]
            for search, values in category.items():
                if search in words:

                    problems.append(
                        Claim(
                            kind="invalid-name",
                            icon="warning",
                            summary="<name> should not contain {}, use <category> {} instead".format(
                                search, " or ".join(values)
                            ),
                            description="The category should not be repeated in the component "
                            "name. Typically you will need to remove the name suffix to just "
                            "include the model name.",
                            url=url_for(
                                "components.route_show", component_id=self.component_id
                            ),
                        )
                    )

            # tokens banned outright
            for search in ["firmware", "update", "(r)", "(c)"]:
                if search in words:
                    problems.append(
                        Claim(
                            kind="invalid-name",
                            icon="warning",
                            summary="<name> tag should not contain the word {}".format(
                                search
                            ),
                            description="The name should reflect only the product name and not the"
                            " action.",
                            url=url_for(
                                "components.route_show", component_id=self.component_id
                            ),
                        )
                    )

        # does any *other* firmware have this exact <id> and version?
        other_mds = (
            db.session.query(Component)
            .filter(Component.component_id != self.component_id)
            .filter(Component.appstream_id == self.appstream_id)
            .filter(Component.version == self.version)
            .order_by(Component.release_timestamp.desc())
        )
        for other_md in other_mds:
            # ignore when private or deleted
            if not other_md.fw.remote.is_signed:
                continue
            # relax the rule if either is composite firmware
            if len(other_md.fw.mds) > 1 or len(self.fw.mds) > 1:
                continue
            problems.append(
                Claim(
                    kind="duplicate-version",
                    icon="warning",
                    allow_embargo=False,
                    summary="The firmware version already exists",
                    description="A matching firmware version {} already exists on the LVFS. "
                    "Either this firmware or the other firmware needs to be deleted "
                    "to resolve this issue.".format(self.version_display),
                    url=url_for(
                        "firmware.route_show", firmware_id=other_md.fw.firmware_id
                    ),
                )
            )

        # we are going to be uing this in the UI soon
        if not self.category or self.category.value == "unknown":
            problems.append(
                Claim(
                    kind="no-category",
                    icon="warning",
                    summary="Firmware category has not been set",
                    description="All components should have a defined update category "
                    "before being moved to stable.\n"
                    "This also can be set in the .metainfo.xml file.",
                    url=url_for(
                        "components.route_show", component_id=self.component_id
                    ),
                )
            )
        elif (
            self.category.value == "X-Device"
            and self.protocol
            and not self.protocol.allow_device_category
        ):
            problems.append(
                Claim(
                    kind="category-invalid-for-protocol",
                    icon="warning",
                    summary="The category is not valid",
                    description="The protocol {} does not allow the category to be X-Device.".format(
                        self.protocol.value
                    ),
                    url=url_for(
                        "components.route_show", component_id=self.component_id
                    ),
                )
            )

        # the OEM doesn't manage this namespace
        values = [ns.value for ns in self.fw.vendor.namespaces]
        if not values:
            problems.append(
                Claim(
                    kind="no-vendor-namespace",
                    icon="warning",
                    summary="No AppStream namespace for vendor",
                    description="Your vendor does not have permission to own this AppStream ID "
                    "component prefix.\n"
                    "Please either change the firmware vendor or contact the "
                    "LVFS administrator to fix this.",
                )
            )
        elif self.appstream_id_prefix not in values:
            problems.append(
                Claim(
                    kind="invalid-vendor-namespace",
                    icon="warning",
                    summary="Invalid vendor namespace",
                    description="{} does not have permission to own the AppStream ID "
                    "component prefix of {}.\n"
                    "Please either change the vendor to the correct OEM or contact "
                    "the LVFS administrator to fix this.".format(
                        self.fw.vendor.display_name, self.appstream_id_prefix
                    ),
                    url=url_for(
                        "firmware.route_affiliation", firmware_id=self.fw.firmware_id
                    ),
                )
            )

        # name_variant_suffix contains a word in the name
        if self.name_variant_suffix:
            nvs_words = self.name_variant_suffix.split(" ")
            nvs_kws = [_sanitize_keyword(word) for word in nvs_words]
            for word in self.name.split(" "):
                if _sanitize_keyword(word) in nvs_kws:
                    problems.append(
                        Claim(
                            kind="invalid-name-variant-suffix",
                            icon="warning",
                            summary="{} is already part of the <name>".format(word),
                            url=url_for(
                                "components.route_show", component_id=self.component_id
                            ),
                        )
                    )

            # also check banned words
            banned_words: List[str] = []
            for word in nvs_kws:
                if word in [
                    "bmc",
                    "firmware",
                    "for",
                    "fw",
                    "none",
                    "release",
                    "type",
                    "types",
                    "update",
                    "thinkcentre",
                    "types",
                ]:
                    banned_words.append(word)
            if banned_words:
                problems.append(
                    Claim(
                        kind="invalid-name-variant-suffix",
                        icon="warning",
                        summary="The word '{}' should not be included in <name_variant_suffix>".format(
                            ", ".join(banned_words)
                        ),
                        url=url_for(
                            "components.route_show", component_id=self.component_id
                        ),
                    )
                )

        # name contains something banned
        if self.name:
            name = self.name.lower()
            for word in [
                "(",
                ")",
                "<",
                ">",
                "bios",
                "bmc",
                "firmware",
                "fwupd",
                "lvfs",
                "update",
            ]:
                if name.find(word) != -1:
                    problems.append(
                        Claim(
                            kind="invalid-name",
                            icon="warning",
                            summary="{} cannot be included as part of <name>".format(
                                word
                            ),
                            url=url_for(
                                "components.route_show", component_id=self.component_id
                            ),
                        )
                    )

        # name contains the vendor
        if self.name and self.fw.vendor.display_name:
            if self.name.upper().find(self.fw.vendor.display_name.upper()) != -1:
                problems.append(
                    Claim(
                        kind="invalid-name",
                        icon="warning",
                        summary="The vendor should not be part of the name",
                        description="The vendor name {} should not be be included in {}.\n"
                        "Please remove the vendor name from the "
                        "firmware name as it will be prefixed "
                        "automatically as required.".format(
                            self.fw.vendor.display_name, self.name
                        ),
                        url=url_for(
                            "components.route_show", component_id=self.component_id
                        ),
                    )
                )

        # release description contains the composite name
        if self.name and self.name.find("/") != -1:
            for tx in self.descriptions:
                if tx.value and tx.value.find(self.name) != -1:
                    problems.append(
                        Claim(
                            kind="invalid-release-description",
                            icon="warning",
                            summary="No valid update description",
                            description="Release description should not contain the component name: {}".format(
                                self.name
                            ),
                        )
                    )

        # only very new fwupd versions support <branch>
        if self.branch:
            req = self.find_req("id", "org.freedesktop.fwupd")
            if (
                not req
                or req.compare != "ge"
                or StrictVersion(req.version) < StrictVersion("1.5.0")
            ):
                problems.append(
                    Claim(
                        kind="requirement-missing",
                        icon="warning",
                        summary="A requirement is missing for branch",
                        description="The requirement of org.freedesktop.fwupd "
                        ">= 1.5.0 is missing to allow setting "
                        "a branch name",
                        url=url_for(
                            "components.route_show", component_id=self.component_id
                        ),
                    )
                )

        # only very new fwupd versions support LVFS:Supported
        if self.fw and self.fw.vendor.is_community:
            req = self.find_req("id", "org.freedesktop.fwupd")
            if (
                not req
                or req.compare != "ge"
                or StrictVersion(req.version) < StrictVersion("1.7.5")
            ):
                problems.append(
                    Claim(
                        kind="requirement-missing",
                        icon="warning",
                        summary="A requirement is missing for community supported firmware",
                        description="The requirement of org.freedesktop.fwupd "
                        ">= 1.7.5 is missing to force showing the user the required warning text.",
                        url=url_for(
                            "components.route_show", component_id=self.component_id
                        ),
                    )
                )

        # only very new fwupd versions support <recommends> and <suggests>
        hardnesses = {req.hardness for req in self.requirements}
        if "recommends" in hardnesses or "suggests" in hardnesses:
            req = self.find_req("id", "org.freedesktop.fwupd")
            if (
                not req
                or req.compare != "ge"
                or StrictVersion(req.version) < StrictVersion("1.6.2")
            ):
                problems.append(
                    Claim(
                        kind="requirement-missing",
                        icon="warning",
                        summary="A requirement is missing for soft requirements",
                        description="The requirement of org.freedesktop.fwupd "
                        ">= 1.6.2 is missing to allow using soft requirements.",
                        url=url_for(
                            "components.route_show", component_id=self.component_id
                        ),
                    )
                )

        # the vendor has to have been permitted to use this branch
        if self.branch:
            branches = [br.value for br in self.fw.vendor.branches]
            if not branches:
                problems.append(
                    Claim(
                        kind="branch-invalid",
                        icon="warning",
                        summary="The specified component branch was invalid",
                        description="This vendor cannot ship firmware with branches",
                        url=url_for(
                            "components.route_show", component_id=self.component_id
                        ),
                    )
                )
            elif self.branch not in branches:
                problems.append(
                    Claim(
                        kind="branch-invalid",
                        icon="warning",
                        summary="The specified component branch was invalid",
                        description="This vendor is only allowed to use branches {}".format(
                            "|".join(branches)
                        ),
                        url=url_for(
                            "components.route_show", component_id=self.component_id
                        ),
                    )
                )

        # release tag is not provided and required or user has used the example
        if self.category:
            tag = self.vendor_tag
            if tag:
                if tag.enforce and not self.release_tag:
                    problems.append(
                        Claim(
                            kind="release-id-invalid",
                            icon="warning",
                            summary="The component requires a {}".format(tag.name),
                            description="All components for vendor {} with category {} "
                            "must have a release {}.".format(
                                self.fw.vendor.display_name_with_team,
                                self.category.name,
                                tag.name,
                            ),
                            url=url_for(
                                "components.route_show", component_id=self.component_id
                            ),
                        )
                    )
                if tag.example == self.release_tag:
                    problems.append(
                        Claim(
                            kind="release-id-invalid",
                            icon="warning",
                            summary="The component requires a valid release {}".format(
                                tag.name
                            ),
                            description="All components for vendor {} with category {} "
                            "must have the correct release {}.".format(
                                self.fw.vendor.display_name_with_team,
                                self.category.name,
                                tag.name,
                            ),
                            url=url_for(
                                "components.route_show", component_id=self.component_id
                            ),
                        )
                    )

        # invalid value
        if self.release_tag:
            if self.release_tag in ["None", ""]:
                problems.append(
                    Claim(
                        kind="release-id-invalid",
                        icon="warning",
                        summary="The release ID '{}' was not a permitted value".format(
                            self.release_tag
                        ),
                        description="Do not set a release ID if the value is unset.",
                        url=url_for(
                            "components.route_show", component_id=self.component_id
                        ),
                    )
                )

        # invalid metadata license
        if self.metadata_license:
            if not self.metadata_license.is_content:
                problems.append(
                    Claim(
                        kind="metadata-license-invalid",
                        icon="warning",
                        summary="Invalid metadata license",
                        description="The component requires a valid license, e.g. CC0-1.0",
                        url=url_for(
                            "components.route_show", component_id=self.component_id
                        ),
                    )
                )
        else:
            problems.append(
                Claim(
                    kind="metadata-license-invalid",
                    icon="warning",
                    summary="Missing metadata license",
                    description="The component requires a metadata license, e.g. CC0-1.0",
                    url=url_for(
                        "components.route_show", component_id=self.component_id
                    ),
                )
            )

        # set the URL for the component
        for problem in problems:
            if problem.url:
                continue
            problem.url = url_for(
                "components.route_show", component_id=self.component_id, page="update"
            )
        return problems

    @property
    def has_complex_requirements(self) -> bool:
        seen: List[str] = []
        for rq in self.requirements:
            if rq.hardness:
                return True
            if rq.kind == "firmware":
                if rq.value not in [None, "bootloader"]:
                    return True
                if rq.depth:
                    return True
            key = rq.kind + ":" + str(rq.value)
            if key in seen:
                return True
            seen.append(key)
        return False

    def add_keywords_from_string(self, value: str, priority: int = 0) -> None:
        existing_keywords: Dict[str, ComponentKeyword] = {}
        for kw in self.keywords:
            existing_keywords[kw.value] = kw
        for keyword in _split_search_string(value):
            if keyword in existing_keywords:
                continue
            self.keywords.append(ComponentKeyword(value=keyword, priority=priority))

    def find_req(
        self,
        kind: str,
        value: Optional[str] = None,
        hardness: Optional[str] = None,
    ) -> Optional["ComponentRequirement"]:
        """Find a ComponentRequirement from the kind and/or value"""
        for rq in self.requirements:
            if rq.hardness != hardness:
                continue
            if rq.kind != kind:
                continue
            if value is not None and rq.value != value:
                continue
            return rq
        return None

    def add_claim(self, claim: Claim) -> None:
        for component_claim in self.component_claims:
            if component_claim.claim.kind == claim.kind:
                return
        self.component_claims.append(ComponentClaim(claim=claim))

    def check_acl(self, action: str, user: Optional[User] = None) -> bool:

        # fall back
        if not user:
            user = g.user
        if not user:
            return False
        if user.check_acl("@admin"):
            return True

        # depends on the action requested
        if action in (
            "@modify-keywords",
            "@modify-requirements",
            "@modify-checksums",
            "@modify-appstream-id",
            "@modify-updateinfo",
        ):
            if self.fw.remote.name != "stable":
                if user.check_acl("@qa") and self.fw._is_permitted_action(action, user):
                    return True
                if self.fw._is_owner(user):
                    return True
            return False
        raise NotImplementedError(
            "unknown security check action: %s:%s" % (self, action)
        )

    def __repr__(self) -> str:
        return "Component object %s" % self.appstream_id


class ComponentRequirement(db.Model):  # type: ignore

    __tablename__ = "requirements"

    requirement_id = Column(Integer, primary_key=True)
    component_id = Column(
        Integer, ForeignKey("components.component_id"), nullable=False, index=True
    )
    kind = Column(Text, nullable=False, index=True)
    value = Column(Text, default=None)
    compare = Column(Text, default=None)
    version = Column(Text, default=None)
    depth = Column(Integer, default=None)
    hardness = Column(Text, default=None)  # None is a hard requirement

    md = relationship("Component", back_populates="requirements")

    def to_element(self) -> ET.Element:
        child = ET.Element(self.kind)
        if self.value:
            child.text = self.value
        if self.compare:
            child.set("compare", self.compare)
        if self.version:
            child.set("version", self.version)
        if self.depth:
            child.set("depth", str(self.depth))
        return child

    def __repr__(self) -> str:
        return "ComponentRequirement object %s/%s/%s/%s/%s" % (
            self.kind,
            self.value,
            self.compare,
            self.version,
            self.hardness or "requires",
        )


class ComponentGuid(db.Model):  # type: ignore

    __tablename__ = "guids"
    guid_id = Column(Integer, primary_key=True)
    component_id = Column(
        Integer, ForeignKey("components.component_id"), nullable=False, index=True
    )
    value = Column(Text, nullable=False)
    ctime = Column(DateTime, default=datetime.datetime.utcnow)

    md = relationship("Component", back_populates="guids")

    __table_args__ = (
        UniqueConstraint("component_id", "value", name="uq_guids_component_id_value"),
    )

    def __repr__(self) -> str:
        return "ComponentGuid object %s" % self.guid_id


class ComponentTag(db.Model):  # type: ignore

    __tablename__ = "component_tags"
    tag_id = Column(Integer, primary_key=True)
    component_id = Column(
        Integer, ForeignKey("components.component_id"), nullable=False, index=True
    )
    value = Column(Text, nullable=False)
    ctime = Column(DateTime, default=datetime.datetime.utcnow)

    md = relationship("Component", back_populates="tags")

    __table_args__ = (
        UniqueConstraint(
            "component_id", "value", name="uq_component_tags_component_id_value"
        ),
    )

    def __repr__(self) -> str:
        return "ComponentTag object %s" % self.tag_id


class ComponentKeyword(db.Model):  # type: ignore

    __tablename__ = "keywords"

    keyword_id = Column(Integer, primary_key=True)
    component_id = Column(
        Integer, ForeignKey("components.component_id"), nullable=False, index=True
    )
    priority = Column(Integer, default=0)
    value = Column(Text, nullable=False)
    ctime = Column(DateTime, default=datetime.datetime.utcnow)

    md = relationship("Component", back_populates="keywords")

    def __repr__(self) -> str:
        return "ComponentKeyword object %s" % self.value


class ComponentSwid(db.Model):  # type: ignore

    __tablename__ = "swids"

    swid_id = Column(Integer, primary_key=True)
    component_id = Column(
        Integer, ForeignKey("components.component_id"), nullable=False, index=True
    )
    tag_id = Column(Text, nullable=False, index=True)
    value = Column(Text, nullable=False)
    ctime = Column(DateTime, default=datetime.datetime.utcnow)
    plugin_id = Column(Text, default=None)

    md = relationship("Component", back_populates="swids")

    def __repr__(self) -> str:
        return "ComponentSwid object {}={}".format(self.swid_id, self.tag_id)


class ComponentChecksum(db.Model):  # type: ignore

    __tablename__ = "checksums"

    checksum_id = Column(Integer, primary_key=True)
    component_id = Column(
        Integer, ForeignKey("components.component_id"), nullable=False, index=True
    )
    kind = Column(Text, nullable=False, default=None)
    value = Column(Text, nullable=False, default=None)
    ctime = Column(DateTime, default=datetime.datetime.utcnow)

    md = relationship("Component")

    def __repr__(self) -> str:
        return "ComponentChecksum object %s(%s)" % (self.kind, self.value)
