#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-many-lines,too-many-nested-blocks

import json
from typing import List, Optional, Any
import dateutil.parser

from flask import (
    Blueprint,
    escape,
    flash,
    make_response,
    redirect,
    render_template,
    request,
    url_for,
    g,
)
from flask_login import login_required

from sqlalchemy import func
from sqlalchemy.exc import NoResultFound, IntegrityError
from sqlalchemy.orm import joinedload

from uswid import uSwidIdentity, uSwidFormatSwid

from lvfs import db, cache

from lvfs.categories.models import Category
from lvfs.licenses.models import License
from lvfs.firmware.models import Firmware
from lvfs.hash import _is_sha1, _is_sha256
from lvfs.metadata.models import Remote
from lvfs.metadata.utils import _export_component_to_xml
from lvfs.protocols.models import Protocol
from lvfs.reports.models import Report, ReportAttribute
from lvfs.tasks.models import Task
from lvfs.util import (
    _validate_appstream_id,
    _validate_guid,
    _validate_tag,
)
from lvfs.verfmts.models import Verfmt

from .utils import (
    _build_swid_archive,
    _build_swid_cyclonedx,
    _build_swid_identity_root,
    COMPONENT_SWID_REL_TO_DISPLAY,
)
from .models import (
    Component,
    ComponentChecksum,
    ComponentGuid,
    ComponentTag,
    ComponentIssue,
    ComponentKeyword,
    ComponentRequirement,
    ComponentDescription,
)

bp_components = Blueprint("components", __name__, template_folder="templates")


def _sanitize_markdown_text(txt: str) -> str:
    txt = txt.replace("\r", "")
    new_lines = [line.strip() for line in txt.strip().split("\n")]
    return "\n".join(new_lines)


@bp_components.route("/problems")
@login_required
def route_problems() -> Any:
    """
    Show all components with problems
    """
    mds: List[Component] = []
    for md in db.session.query(Component).order_by(Component.release_timestamp.desc()):
        if not md.problems:
            continue
        if not md.check_acl("@modify-updateinfo"):
            continue
        if md.fw.is_deleted:
            continue
        mds.append(md)
    return render_template("component-problems.html", category="firmware", mds=mds)


@bp_components.route("/problems/<remote_name>")
@login_required
def route_problems_remote(remote_name: str) -> Any:
    """
    Show all components with problems
    """
    mds: List[Component] = []
    for md in (
        db.session.query(Component)
        .join(Firmware)
        .join(Remote)
        .filter(Remote.name == remote_name)
        .order_by(Component.release_timestamp.desc())
    ):
        if not md.problems:
            continue
        mds.append(md)
    return render_template("component-problems.html", category="firmware", mds=mds)


@bp_components.route("/<int:component_id>/shards")
@login_required
def route_shards(component_id: int) -> Any:
    """
    Show the shards of each component
    """

    # get firmware component
    try:
        md = (
            db.session.query(Component)
            .filter(Component.component_id == component_id)
            .one()
        )
    except NoResultFound:
        flash("No component matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    fw = md.fw
    if not fw:
        flash("No component matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))
    if not fw.check_acl("@view"):
        flash("Permission denied: Unable to view component", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    return render_template(
        "component-shards.html", category="firmware", md=md, page="shards"
    )


@bp_components.route("/<int:component_id>/certificates")
@login_required
def route_certificates(component_id: int) -> Any:
    """
    Show the shards of each component
    """

    # get firmware component
    try:
        md = (
            db.session.query(Component)
            .filter(Component.component_id == component_id)
            .one()
        )
    except NoResultFound:
        flash("No component matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    fw = md.fw
    if not fw:
        flash("No firmware matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))
    if not fw.check_acl("@view"):
        flash("Permission denied: Unable to view component", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    return render_template(
        "component-certificates.html", category="firmware", md=md, page="certificates"
    )


@bp_components.route("/<int:component_id>/export")
@login_required
def route_export(component_id: int) -> Any:
    """
    Show the shards of each component
    """

    # get firmware component
    try:
        md = (
            db.session.query(Component)
            .filter(Component.component_id == component_id)
            .one()
        )
    except NoResultFound:
        flash("No component matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    fw = md.fw
    if not fw:
        flash("No firmware matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))
    if not fw.check_acl("@view"):
        flash("Permission denied: Unable to view component", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))
    return render_template(
        "component-export.html",
        category="firmware",
        md=md,
        xml=escape(_export_component_to_xml(md)),
        page="export",
    )


@bp_components.post("/<int:component_id>/modify")
@login_required
def route_modify(component_id: int) -> Any:
    """Modifies the component properties"""

    # find firmware
    try:
        md = (
            db.session.query(Component)
            .filter(Component.component_id == component_id)
            .join(Firmware)
            .with_for_update(of=[Firmware, Component])
            .one()
        )
    except NoResultFound:
        flash("No component matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not md.check_acl("@modify-updateinfo"):
        flash(
            "Permission denied: Insufficient permissions to modify firmware", "danger"
        )
        return redirect(url_for("components.route_show", component_id=component_id))

    # set new metadata values
    page = "overview"
    locale = request.form.get("locale")
    if locale in ["", "en_US"]:
        locale = None
    retry_all_tests = False
    if "screenshot_url" in request.form:
        md.screenshot_url = request.form["screenshot_url"]
    if "protocol_id" in request.form:
        if md.protocol_id != request.form["protocol_id"]:
            md.protocol_id = request.form["protocol_id"]
            retry_all_tests = True
    if "verfmt_id" in request.form:
        if md.verfmt_id != request.form["verfmt_id"]:
            md.verfmt_id = request.form["verfmt_id"] or None
    if "metadata_license_id" in request.form:
        if md.metadata_license_id != request.form["metadata_license_id"]:
            md.metadata_license_id = request.form["metadata_license_id"] or None
    if "project_license_id" in request.form:
        if md.project_license_id != request.form["project_license_id"]:
            md.project_license_id = request.form["project_license_id"] or None
    if "category_id" in request.form:
        category_id: Optional[str] = request.form["category_id"]
        if not category_id:
            category_id = None
        if md.category_id != category_id:
            md.category_id = category_id
            retry_all_tests = True
    if "screenshot_caption" in request.form:
        md.screenshot_caption = _sanitize_markdown_text(
            request.form["screenshot_caption"]
        )
    if "install_duration" in request.form:
        try:
            md.install_duration = int(request.form["install_duration"])
        except ValueError:
            md.install_duration = 0
        page = "install_duration"
    if "urgency" in request.form:
        md.release_urgency = request.form["urgency"]
        page = "update"
    if "description" in request.form:
        tx: Optional[ComponentDescription] = md.get_description_by_locale(locale)
        if not tx:
            tx = ComponentDescription(
                locale=locale,
                user=g.user,
            )
            md.descriptions.append(tx)
        tx.value = _sanitize_markdown_text(request.form["description"])
        page = "description"
    if "details_url" in request.form:
        md.details_url = request.form["details_url"]
        page = "update"
    if "source_url" in request.form:
        md.source_url = request.form["source_url"]
        page = "update"
    if "appstream_id" in request.form:
        md.appstream_id = request.form["appstream_id"]
        if not _validate_appstream_id(md.appstream_id):
            flash("AppStream ID was not valid", "warning")
            return redirect(url_for("components.route_show", component_id=component_id))
    if "name" in request.form:
        md.name = request.form["name"]
    if "summary" in request.form:
        md.summary = request.form["summary"]
    if "release_dt" in request.form:
        try:
            date = dateutil.parser.parse(request.form["release_dt"])
            md.release_timestamp = int(date.timestamp())
        except dateutil.parser.ParserError:
            flash("Release date was not valid", "warning")
            return redirect(url_for("components.route_show", component_id=component_id))
    if "name_variant_suffix" in request.form:
        md.name_variant_suffix = request.form["name_variant_suffix"]
    if "branch" in request.form:
        md.branch = request.form["branch"]
    if "release_tag" in request.form:
        md.release_tag = request.form["release_tag"] or None
    if "release_message" in request.form:
        md.release_message = request.form["release_message"]
    if "release_image" in request.form:
        md.release_image = request.form["release_image"]
    if "device_integrity" in request.form:
        md.device_integrity = request.form["device_integrity"]

    # async
    db.session.add(
        Task(
            value=json.dumps({"id": md.fw.firmware_id, "retry-all": retry_all_tests}),
            caller=__name__,
            user=g.user,
            url=url_for("components.route_show", component_id=component_id),
            function="lvfs.firmware.utils.task_modify_fw",
        )
    )
    db.session.commit()

    flash("Component updated", "info")
    return redirect(
        url_for(
            "components.route_show", component_id=component_id, page=page, locale=locale
        )
    )


@bp_components.route("/<int:component_id>/checksums")
@login_required
def route_checksums(component_id: int) -> Any:
    """Show firmware component information"""

    # get firmware component
    try:
        md = (
            db.session.query(Component)
            .filter(Component.component_id == component_id)
            .one()
        )
    except NoResultFound:
        flash("No component matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    fw = md.fw
    if not fw:
        flash("No firmware matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))
    if not fw.check_acl("@view"):
        flash("Permission denied: Unable to view component", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # find reports witch device checksums that match this firmware
    checksum_counts = (
        db.session.query(func.count(ReportAttribute.value), ReportAttribute.value)
        .join(Report)
        .filter(Report.state == 2)
        .filter(Report.firmware_id == fw.firmware_id)
        .filter(ReportAttribute.key == "ChecksumDevice")
        .group_by(ReportAttribute.value)
        .all()
    )
    device_checksums = [csum.value for csum in md.device_checksums]
    return render_template(
        "component-checksums.html",
        category="firmware",
        md=md,
        page="checksums",
        device_checksums=device_checksums,
        checksum_counts=checksum_counts,
    )


@bp_components.route("/<int:component_id>")
@bp_components.route("/<int:component_id>/<page>")
@bp_components.route("/<int:component_id>/<page>/<locale>")
@login_required
def route_show(
    component_id: int, page: str = "overview", locale: Optional[str] = None
) -> Any:
    """Show firmware component information"""

    # get firmware component
    try:
        md = (
            db.session.query(Component)
            .filter(Component.component_id == component_id)
            .options(joinedload(Component.guids))
            .one()
        )
    except NoResultFound:
        flash("No component matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    fw = md.fw
    if not fw:
        flash("No firmware matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))
    if not fw.check_acl("@view"):
        flash("Permission denied: Unable to view other vendor firmware", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # firmware requirements are too complicated to show on the simplified fiew
    if page == "requires" and md.has_complex_requirements:
        page = "requires-advanced"

    verfmts = db.session.query(Verfmt).order_by(Verfmt.name.asc()).all()
    protocols = db.session.query(Protocol).order_by(Protocol.name.asc()).all()
    for protocol in protocols:
        if protocol.value == "unknown":
            protocols.remove(protocol)
            protocols.insert(0, protocol)
            break
    categories = db.session.query(Category).order_by(Category.name.asc()).all()
    metadata_licenses = (
        db.session.query(License)
        .filter(License.is_content)
        .order_by(License.name.asc())
        .all()
    )
    project_licenses = (
        db.session.query(License)
        .filter(License.is_content == False)
        .order_by(License.name.asc())
        .all()
    )
    return render_template(
        "component-" + page + ".html",
        category="firmware",
        protocols=protocols,
        verfmts=verfmts,
        categories=categories,
        metadata_licenses=metadata_licenses,
        project_licenses=project_licenses,
        md=md,
        page=page,
        locale=locale,
    )


@bp_components.post("/<int:component_id>/requirement/delete/<int:requirement_id>")
@login_required
def route_requirement_delete(component_id: int, requirement_id: int) -> Any:

    # get firmware component
    try:
        rq = (
            db.session.query(ComponentRequirement)
            .filter(ComponentRequirement.requirement_id == requirement_id)
            .join(Component)
            .filter(Component.component_id == component_id)
            .join(Firmware)
            .with_for_update(of=[ComponentRequirement, Component, Firmware])
            .one()
        )
    except NoResultFound:
        flash("No requirement matched!", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # get the firmware for the requirement
    md = rq.md
    if not md:
        flash("No metadata matched!", "warning")
        return redirect(url_for("components.route_show", component_id=component_id))

    # security check
    if not md.check_acl("@modify-requirements"):
        flash("Permission denied: Unable to modify other vendor firmware", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # remove chid
    db.session.delete(rq)
    db.session.add(
        Task(
            value=json.dumps({"id": md.fw.firmware_id}),
            caller=__name__,
            user=g.user,
            url=url_for("components.route_show", component_id=md.component_id),
            function="lvfs.firmware.utils.task_sign_fw",
        )
    )
    db.session.commit()

    # log
    flash("Removed requirement %s" % rq.value, "info")
    return redirect(
        url_for("components.route_show", component_id=md.component_id, page="requires")
    )


@bp_components.post("/<int:component_id>/requirement/create")
@login_required
def route_requirement_create(component_id: int) -> Any:
    """Adds a requirement to a component"""

    # get firmware component
    try:
        md = (
            db.session.query(Component)
            .filter(Component.component_id == component_id)
            .join(Firmware)
            .with_for_update(of=[Firmware, Component])
            .one()
        )
    except NoResultFound:
        flash("No component matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not md.check_acl("@modify-requirements"):
        flash("Permission denied: Unable to modify other vendor firmware", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # validate form has enough data
    try:
        if request.form["kind"] == "hardware":
            if not _validate_guid(request.form["value"]):
                flash(
                    "Cannot add requirement: {} is not a valid GUID".format(
                        request.form["value"]
                    ),
                    "warning",
                )
                return redirect(
                    url_for(
                        "components.route_show",
                        component_id=md.component_id,
                        page="requires",
                    )
                )
        elif request.form["kind"] in ["firmware", "id", "client"]:
            if not request.form["value"]:
                flash(
                    "Cannot add an empty requirement",
                    "warning",
                )
                return redirect(
                    url_for(
                        "components.route_show",
                        component_id=md.component_id,
                        page="requires",
                    )
                )
        else:
            flash("No valid kind specified!", "danger")
            return redirect(
                url_for(
                    "components.route_show", component_id=component_id, page="requires"
                )
            )
    except KeyError:
        flash("No value specified!", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # validate ID is a valid AppStream ID
    if request.form["kind"] == "id" and not _validate_appstream_id(
        request.form["value"]
    ):
        flash(
            "Cannot add requirement: {} is not a valid AppStream ID".format(
                request.form["value"]
            ),
            "warning",
        )
        return redirect(
            url_for(
                "components.route_show", component_id=md.component_id, page="requires"
            )
        )

    # support empty too
    compare = request.form.get("compare", None)
    if not compare:
        compare = None
    version = request.form.get("version", None)
    if not version:
        version = None
    depth = request.form.get("depth", None)
    if not depth:
        depth = None
    hardness = request.form.get("hardness", None)
    if not hardness:
        hardness = None

    # firmware is unset value
    value: Optional[str] = request.form["value"].strip()
    if value == "self":
        value = None

    # add requirement
    rq = ComponentRequirement(
        kind=request.form["kind"],
        value=value,
        compare=compare,
        version=version,
        depth=depth,
        hardness=hardness,
    )
    md.requirements.append(rq)
    db.session.add(
        Task(
            value=json.dumps({"id": md.fw.firmware_id}),
            caller=__name__,
            user=g.user,
            url=url_for("components.route_show", component_id=md.component_id),
            function="lvfs.firmware.utils.task_sign_fw",
        )
    )
    db.session.commit()

    flash("Added requirement", "info")
    return redirect(
        url_for("components.route_show", component_id=md.component_id, page="requires")
    )


@bp_components.post("/<int:component_id>/requirement/modify")
@login_required
def route_requirement_modify(component_id: int) -> Any:
    """Adds a requirement to a component"""

    # get firmware component
    try:
        md = (
            db.session.query(Component)
            .filter(Component.component_id == component_id)
            .join(Firmware)
            .with_for_update(of=[Firmware, Component])
            .one()
        )
    except NoResultFound:
        flash("No component matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not md.check_acl("@modify-requirements"):
        flash("Permission denied: Unable to modify other vendor firmware", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # validate CHID is a valid GUID
    try:
        if request.form["kind"] not in ["hardware", "firmware", "id", "client"]:
            flash("No valid kind specified!", "danger")
            return redirect(url_for("components.route_show", component_id=component_id))
        if request.form["kind"] == "hardware" and not _validate_guid(
            request.form["value"]
        ):
            flash(
                "Cannot add requirement: %s is not a valid GUID"
                % request.form["value"],
                "warning",
            )
            return redirect(
                url_for(
                    "components.route_show",
                    component_id=md.component_id,
                    page="requires",
                )
            )
    except KeyError:
        flash("No value specified!", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # validate ID is a valid AppStream ID
    if request.form["kind"] == "id" and not _validate_appstream_id(
        request.form["value"]
    ):
        flash(
            "Cannot add requirement: {} is not a valid AppStream ID".format(
                request.form["value"]
            ),
            "warning",
        )
        return redirect(
            url_for(
                "components.route_show", component_id=md.component_id, page="requires"
            )
        )

    # empty string is None
    try:
        value: Optional[str] = request.form["value"]
        if not value:
            value = None
        hardness: Optional[str] = request.form["hardness"]
        if not hardness:
            hardness = None
    except KeyError:
        flash("Cannot add requirement: not enough data", "warning")
        return redirect(
            url_for(
                "components.route_show", component_id=md.component_id, page="requires"
            )
        )

    # check it's not already been added
    rq = md.find_req(request.form["kind"], value, hardness)
    if rq:
        if "version" in request.form:
            rq.version = request.form["version"]
        if "compare" in request.form:
            if request.form["compare"] == "any":
                db.session.delete(rq)
                db.session.commit()
                flash("Deleted requirement %s" % rq.value, "info")
                return redirect(
                    url_for(
                        "components.route_show",
                        component_id=md.component_id,
                        page="requires",
                    )
                )
            rq.compare = request.form["compare"]
        if rq.value:
            flash("Modified requirement %s" % rq.value, "info")
        else:
            flash("Modified requirement firmware", "info")
    else:
        # add requirement
        rq = ComponentRequirement(
            kind=request.form["kind"],
            value=value,
            compare=request.form.get("compare", None),
            version=request.form.get("version", None),
            depth=request.form.get("depth", None),
            hardness=hardness,
        )
        md.requirements.append(rq)
        flash("Added requirement", "info")

    # asynchronously sign
    db.session.add(
        Task(
            value=json.dumps({"id": md.fw.firmware_id}),
            caller=__name__,
            user=g.user,
            url=url_for("components.route_show", component_id=md.component_id),
            function="lvfs.firmware.utils.task_sign_fw",
        )
    )
    db.session.commit()

    return redirect(
        url_for("components.route_show", component_id=md.component_id, page="requires")
    )


@bp_components.post("/<int:component_id>/guid/delete/<int:guid_id>")
@login_required
def route_guid_delete(component_id: int, guid_id: int) -> Any:

    # get firmware component
    try:
        gu = (
            db.session.query(ComponentGuid)
            .filter(ComponentGuid.guid_id == guid_id)
            .join(Component)
            .filter(Component.component_id == component_id)
            .join(Firmware)
            .with_for_update(of=[Firmware, Component])
            .one()
        )
    except NoResultFound:
        flash("No GUID matched!", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # security check
    md = gu.md
    if not md.check_acl("@modify-appstream-id"):
        flash("Permission denied: Unable to modify other vendor firmware", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # remove chid
    db.session.delete(gu)
    db.session.add(
        Task(
            value=json.dumps({"id": md.fw.firmware_id}),
            caller=__name__,
            user=g.user,
            url=url_for("components.route_show", component_id=md.component_id),
            function="lvfs.firmware.utils.task_sign_fw",
        )
    )
    db.session.commit()

    # log
    flash("Removed GUID {}".format(gu.value), "info")
    return redirect(
        url_for("components.route_show", component_id=md.component_id, page="guids")
    )


@bp_components.post("/<int:component_id>/guid/create")
@login_required
def route_guid_create(component_id: int) -> Any:
    """Adds a guid to a component"""

    # check we have data
    value = request.form.get("value")
    if not value:
        flash("No data provided", "warning")
        return redirect(url_for("components.route_show", component_id=component_id))

    # get firmware component
    try:
        md = (
            db.session.query(Component)
            .filter(Component.component_id == component_id)
            .join(Firmware)
            .with_for_update(of=[Firmware, Component])
            .one()
        )
    except NoResultFound:
        flash("No component matched!", "warning")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not md.check_acl("@modify-requirements"):
        flash("Permission denied: Unable to modify GUIDs", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # validate is a valid GUID
    if not _validate_guid(value):
        flash("Cannot add GUID: {} is not valid".format(value), "warning")
        return redirect(
            url_for("components.route_show", component_id=md.component_id, page="guids")
        )

    # add guid
    try:
        md.guids.append(ComponentGuid(value=value))
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("GUID already added", "warning")
        return redirect(
            url_for("components.route_show", component_id=md.component_id, page="guids")
        )
    db.session.add(
        Task(
            value=json.dumps({"id": md.fw.firmware_id}),
            caller=__name__,
            user=g.user,
            url=url_for("components.route_show", component_id=md.component_id),
            function="lvfs.firmware.utils.task_sign_fw",
        )
    )
    db.session.commit()

    flash("Added GUID", "info")
    return redirect(
        url_for("components.route_show", component_id=md.component_id, page="guids")
    )


@bp_components.post("/<int:component_id>/tag/delete/<tag_id>")
@login_required
def route_tag_delete(component_id: int, tag_id: int) -> Any:

    # get firmware component
    try:
        tag = (
            db.session.query(ComponentTag)
            .filter(ComponentTag.tag_id == tag_id)
            .join(Component)
            .filter(Component.component_id == component_id)
            .join(Firmware)
            .with_for_update(of=[Firmware, Component, ComponentTag])
            .one()
        )
    except NoResultFound:
        flash("No tag matched!", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # security check
    md = tag.md
    if not md.check_acl("@modify-appstream-id"):
        flash("Permission denied: Unable to modify other vendor firmware", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # remove chid
    db.session.delete(tag)
    db.session.add(
        Task(
            value=json.dumps({"id": md.fw.firmware_id}),
            caller=__name__,
            user=g.user,
            url=url_for("components.route_show", component_id=md.component_id),
            function="lvfs.firmware.utils.task_sign_fw",
        )
    )
    db.session.commit()

    # log
    flash("Removed tag {}".format(tag.value), "info")
    return redirect(
        url_for("components.route_show", component_id=md.component_id, page="tags")
    )


@bp_components.post("/<int:component_id>/tag/create")
@login_required
def route_tag_create(component_id: int) -> Any:
    """Adds a tag to a component"""

    # check we have data
    value = request.form.get("value")
    if not value:
        flash("No data provided", "warning")
        return redirect(url_for("components.route_show", component_id=component_id))

    # get firmware component
    try:
        md = (
            db.session.query(Component)
            .filter(Component.component_id == component_id)
            .join(Firmware)
            .with_for_update(of=[Firmware, Component])
            .one()
        )
    except NoResultFound:
        flash("No component matched!", "warning")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not md.check_acl("@modify-requirements"):
        flash("Permission denied: Unable to modify tags", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # validate is a valid tag
    if not _validate_tag(value):
        flash("Cannot add tag: {} is not valid".format(value), "warning")
        return redirect(
            url_for("components.route_show", component_id=md.component_id, page="tags")
        )

    # add tag
    try:
        md.tags.append(ComponentTag(value=value))
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("tag already added", "warning")
        return redirect(
            url_for("components.route_show", component_id=md.component_id, page="tags")
        )
    db.session.add(
        Task(
            value=json.dumps({"id": md.fw.firmware_id}),
            caller=__name__,
            user=g.user,
            url=url_for("components.route_show", component_id=md.component_id),
            function="lvfs.firmware.utils.task_sign_fw",
        )
    )
    db.session.commit()

    flash("Added tag", "info")
    return redirect(
        url_for("components.route_show", component_id=md.component_id, page="tags")
    )


@bp_components.post("/<int:component_id>/keyword/<int:keyword_id>/delete")
@login_required
def route_keyword_delete(component_id: int, keyword_id: int) -> Any:

    # get firmware component
    try:
        kw = (
            db.session.query(ComponentKeyword)
            .filter(ComponentKeyword.keyword_id == keyword_id)
            .join(Component)
            .filter(Component.component_id == component_id)
            .join(Firmware)
            .with_for_update(of=[Firmware, Component, ComponentKeyword])
            .one()
        )
    except NoResultFound:
        flash("No keyword matched!", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # get the firmware for the keyword
    md = kw.md
    if not md:
        flash("No metadata matched!", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # security check
    if not md.check_acl("@modify-keywords"):
        flash("Permission denied: Unable to modify other vendor firmware", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # remove chid
    db.session.delete(kw)
    db.session.add(
        Task(
            value=json.dumps({"id": md.fw.firmware_id}),
            caller=__name__,
            user=g.user,
            url=url_for("components.route_show", component_id=md.component_id),
            function="lvfs.firmware.utils.task_sign_fw",
        )
    )
    db.session.commit()

    # log
    flash("Removed keyword %s" % kw.value, "info")
    return redirect(
        url_for("components.route_show", component_id=md.component_id, page="keywords")
    )


@bp_components.post("/<int:component_id>/keyword/create")
@login_required
def route_keyword_create(component_id: int) -> Any:
    """Adds one or more keywords to the existing component"""

    # get firmware component
    try:
        md = (
            db.session.query(Component)
            .filter(Component.component_id == component_id)
            .join(Firmware)
            .with_for_update(of=[Firmware, Component])
            .one()
        )
    except NoResultFound:
        flash("No component matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not md.check_acl("@modify-keywords"):
        flash("Permission denied: Unable to modify other vendor firmware", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # add keyword
    try:
        md.add_keywords_from_string(request.form["value"], priority=5)
    except KeyError:
        flash("No value specified!", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))
    db.session.add(
        Task(
            value=json.dumps({"id": md.fw.firmware_id}),
            caller=__name__,
            user=g.user,
            url=url_for("components.route_show", component_id=md.component_id),
            function="lvfs.firmware.utils.task_sign_fw",
        )
    )
    db.session.commit()

    flash("Added keywords", "info")
    return redirect(
        url_for("components.route_show", component_id=md.component_id, page="keywords")
    )


@bp_components.post("/<int:component_id>/issue/<component_issue_id>/delete")
@login_required
def route_issue_delete(component_id: int, component_issue_id: int) -> Any:

    # get firmware component
    try:
        issue = (
            db.session.query(ComponentIssue)
            .filter(
                ComponentIssue.component_issue_id == component_issue_id,
            )
            .join(Component)
            .filter(Component.component_id == component_id)
            .join(Firmware)
            .with_for_update(of=[Firmware, Component, ComponentIssue])
            .one()
        )
    except NoResultFound:
        flash("No issue matched!", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # permission check
    md = issue.md
    if not md.check_acl("@modify-updateinfo"):
        flash("Permission denied: Unable to modify firmware", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # remove issue
    db.session.delete(issue)
    db.session.add(
        Task(
            value=json.dumps({"id": md.fw.firmware_id}),
            caller=__name__,
            user=g.user,
            url=url_for("components.route_show", component_id=md.component_id),
            function="lvfs.firmware.utils.task_sign_fw",
        )
    )
    db.session.commit()

    # log
    flash("Removed {}".format(issue.value_display), "info")
    return redirect(
        url_for("components.route_show", component_id=md.component_id, page="issues")
    )


def _autoimport_issues(md: Component, prefix: str, kind: str) -> int:
    issues: List[ComponentIssue] = []
    start = 0
    tx: Optional[ComponentDescription] = md.get_description_by_locale()
    if not tx:
        return 0
    tmp = tx.value
    description_new = ""

    while True:

        # look for a CVE token
        idx = tmp.find(prefix, start)
        if idx == -1:
            description_new += tmp[start:]
            break

        # yay, so save what we've got so far
        description_new += tmp[start:idx]

        # find the end of the CVE value
        issue_len = 0
        for char in tmp[idx + len(prefix) :]:
            if char != "-" and not char.isnumeric():
                break
            issue_len += 1

        # extract the CVE value, and add to the component if required
        value = tmp[idx : idx + len(prefix) + issue_len]
        if value not in md.issue_values:
            if value.startswith("VU#"):
                issue = ComponentIssue(kind=kind, value=value[3:], user=g.user)
            else:
                issue = ComponentIssue(kind=kind, value=value, user=g.user)
            if issue.problem:
                description_new += value
            else:
                issues.append(issue)

        # advance string to end of CVE number
        start = idx + len(prefix) + issue_len

        # add something the user has to manually remove
        description_new += "REMOVE_ME"

    # success
    if issues:
        tx.value = description_new
        md.issues.extend(issues)

    return len(issues)


@bp_components.post("/<int:component_id>/issue/autoimport")
@login_required
def route_issue_autoimport(component_id: int) -> Any:

    # get firmware component
    try:
        md = (
            db.session.query(Component)
            .filter(Component.component_id == component_id)
            .join(Firmware)
            .with_for_update(of=[Firmware, Component])
            .one()
        )
    except NoResultFound:
        flash("No component matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # permission check
    if not md.check_acl("@modify-updateinfo"):
        flash("Permission denied: Unable to modify firmware", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # find any valid CVE numbers in the existing description
    n_issues = _autoimport_issues(md, "CVE-", "cve")
    n_issues += _autoimport_issues(md, "DSA-", "dell")
    n_issues += _autoimport_issues(md, "LEN-", "lenovo")
    n_issues += _autoimport_issues(md, "INTEL-SA-", "intel")
    n_issues += _autoimport_issues(md, "INTEL-TA-", "intel")
    n_issues += _autoimport_issues(md, "VU#", "vince")

    # success
    if not n_issues:
        flash("No issues could be detected", "info")
    else:
        db.session.add(
            Task(
                value=json.dumps({"id": md.fw.firmware_id}),
                caller=__name__,
                user=g.user,
                url=url_for("components.route_show", component_id=md.component_id),
                function="lvfs.firmware.utils.task_sign_fw",
            )
        )
        db.session.commit()
        flash(
            "Added {} issues — now review the update description for sanity".format(
                n_issues
            ),
            "info",
        )
    return redirect(
        url_for(
            "components.route_show", component_id=md.component_id, page="description"
        )
    )


@bp_components.post("/<int:component_id>/issue/create")
@login_required
def route_issue_create(component_id: int) -> Any:
    """Adds one or more CVEs to the existing component"""

    # get firmware component
    try:
        md = (
            db.session.query(Component)
            .filter(Component.component_id == component_id)
            .join(Firmware)
            .with_for_update(of=[Firmware, Component])
            .one()
        )
    except NoResultFound:
        flash("No component matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not md.check_acl("@modify-updateinfo"):
        flash("Permission denied: Unable to modify firmware", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # add issue
    try:
        values = request.form["value"].split(",")
    except KeyError:
        flash("No value specified!", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))
    for value in values:
        if value in md.issue_values:
            flash("Already exists: {}".format(value), "info")
            continue
        if value.startswith("CVE-"):
            issue = ComponentIssue(kind="cve", value=value, user=g.user)
        elif value.startswith("VU#"):
            issue = ComponentIssue(kind="vince", value=value[3:], user=g.user)
        elif value.startswith("DSA-"):
            issue = ComponentIssue(kind="dell", value=value, user=g.user)
        elif value.startswith("LEN-"):
            issue = ComponentIssue(kind="lenovo", value=value, user=g.user)
        elif value.startswith("INTEL-SA-") or value.startswith("INTEL-TA-"):
            issue = ComponentIssue(kind="intel", value=value, user=g.user)
        else:
            flash("Issue invalid: {}".format(value), "danger")
            return redirect(
                url_for(
                    "components.route_show", component_id=component_id, page="issues"
                )
            )
        if issue.problem:
            flash("Issue invalid: {}".format(issue.problem.description), "danger")
            return redirect(
                url_for(
                    "components.route_show", component_id=component_id, page="issues"
                )
            )
        flash("Added {}".format(value), "info")
        md.issues.append(issue)
    db.session.add(
        Task(
            value=json.dumps({"id": md.fw.firmware_id}),
            caller=__name__,
            user=g.user,
            url=url_for("components.route_show", component_id=md.component_id),
            function="lvfs.firmware.utils.task_sign_fw",
        )
    )
    db.session.commit()

    return redirect(
        url_for("components.route_show", component_id=md.component_id, page="issues")
    )


@bp_components.post("/<int:component_id>/checksum/delete/<int:checksum_id>")
@login_required
def route_checksum_delete(component_id: int, checksum_id: int) -> Any:

    # get firmware component
    try:
        csum = (
            db.session.query(ComponentChecksum)
            .filter(ComponentChecksum.checksum_id == checksum_id)
            .join(Component)
            .filter(Component.component_id == component_id)
            .join(Firmware)
            .with_for_update(of=[Firmware, Component, ComponentChecksum])
            .one()
        )
    except NoResultFound:
        flash("No checksum matched!", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # get the component for the checksum
    md = csum.md
    if not md:
        flash("No metadata matched!", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # security check
    if not md.check_acl("@modify-checksums"):
        flash("Permission denied: Unable to modify other vendor firmware", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # remove chid
    db.session.delete(csum)
    db.session.add(
        Task(
            value=json.dumps({"id": md.fw.firmware_id}),
            caller=__name__,
            user=g.user,
            url=url_for("components.route_show", component_id=md.component_id),
            function="lvfs.firmware.utils.task_sign_fw",
        )
    )
    db.session.commit()

    # log
    flash("Removed device checksum", "info")
    return redirect(
        url_for("components.route_show", component_id=md.component_id, page="checksums")
    )


@bp_components.post("/<int:component_id>/checksum/create")
@login_required
def route_checksum_create(component_id: int) -> Any:
    """Adds a checksum to a component"""

    # get firmware component
    try:
        md = (
            db.session.query(Component)
            .filter(Component.component_id == component_id)
            .join(Firmware)
            .with_for_update(of=[Firmware, Component])
            .one()
        )
    except NoResultFound:
        flash("No component matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))

    # security check
    if not md.check_acl("@modify-checksums"):
        flash("Permission denied: Unable to modify other vendor firmware", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))

    # validate is a valid hash
    try:
        hash_value = request.form["value"].lower()
    except KeyError:
        flash("No value specified!", "danger")
        return redirect(url_for("components.route_show", component_id=component_id))
    if _is_sha1(hash_value):
        hash_kind = "SHA1"
    elif _is_sha256(hash_value):
        hash_kind = "SHA256"
    else:
        flash("%s is not a recognized SHA1 or SHA256 hash" % hash_value, "warning")
        return redirect(
            url_for(
                "components.route_show", component_id=md.component_id, page="checksums"
            )
        )

    # check it's not already been added
    for csum in md.device_checksums:
        if csum.value == hash_value:
            flash("%s has already been added" % hash_value, "warning")
            return redirect(
                url_for(
                    "components.route_show",
                    component_id=md.component_id,
                    page="checksums",
                )
            )

    # add checksum
    csum = ComponentChecksum(kind=hash_kind, value=hash_value)
    md.device_checksums.append(csum)
    db.session.add(
        Task(
            value=json.dumps({"id": md.fw.firmware_id}),
            caller=__name__,
            user=g.user,
            url=url_for("components.route_show", component_id=md.component_id),
            function="lvfs.firmware.utils.task_sign_fw",
        )
    )
    db.session.commit()

    flash("Added device checksum", "info")
    return redirect(
        url_for("components.route_show", component_id=md.component_id, page="checksums")
    )


@bp_components.route("/<int:component_id>/download")
@login_required
def route_download(component_id: int) -> Any:

    # get firmware component
    try:
        md = (
            db.session.query(Component)
            .filter(Component.component_id == component_id)
            .one()
        )
    except NoResultFound:
        flash("No component matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))
    if not md.fw.check_acl("@view"):
        flash("Permission denied: Unable to download component", "danger")
        return redirect(url_for("main.route_dashboard"))
    if not md.blob:
        flash("Permission denied: Component has no data", "warning")
        return redirect(url_for("main.route_dashboard"))
    response = make_response(md.blob)
    response.headers.set("Content-Type", "application/octet-stream")
    response.headers.set(
        "Content-Disposition", "attachment", filename=md.filename_contents
    )
    return response


@bp_components.route("/<int:component_id>/swid")
@login_required
def route_swids(component_id: int) -> Any:
    """Build firmware SBoM archive"""

    # get firmware component
    try:
        md = (
            db.session.query(Component)
            .filter(Component.component_id == component_id)
            .one()
        )
    except NoResultFound:
        flash("No component matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))
    if not md.fw.check_acl("@view"):
        flash("Permission denied: Unable to download component", "danger")
        return redirect(url_for("main.route_dashboard"))

    identities: List[uSwidIdentity] = [_build_swid_identity_root(md)]
    for swid in md.swids:
        for identity in uSwidFormatSwid().load(swid.value.encode()):
            identities.append(identity)

    return render_template(
        "component-swids.html",
        category="firmware",
        md=md,
        rel_map=COMPONENT_SWID_REL_TO_DISPLAY,
        page="swid",
        identities=identities,
    )


@bp_components.route("/<int:component_id>/swid/archive")
@cache.cached()
def route_swids_archive(component_id: int) -> Any:
    """Build firmware SBoM archive"""

    # get firmware component
    try:
        md = (
            db.session.query(Component)
            .filter(Component.component_id == component_id)
            .one()
        )
    except NoResultFound:
        flash("No component matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))
    if not md.fw.check_acl("@view"):
        flash("Permission denied: Unable to download component", "danger")
        return redirect(url_for("main.route_dashboard"))
    if not md.swids:
        flash("Component had no SWID metadata", "warning")
        return redirect(url_for("components.route_show", component_id=md.component_id))

    # create archive
    response = make_response(_build_swid_archive(md))
    response.headers.set("Content-Type", "application/zip")
    response.headers.set(
        "Content-Disposition",
        "attachment",
        filename="{}-v{}.zip".format(md.appstream_id, md.version_display),
    )
    return response


@bp_components.route("/<int:component_id>/swid/cyclonedx")
@cache.cached()
def route_swids_cyclonedx(component_id: int) -> Any:
    """Build CycloneDX SBoM"""

    # get firmware component
    try:
        md = (
            db.session.query(Component)
            .filter(Component.component_id == component_id)
            .one()
        )
    except NoResultFound:
        flash("No component matched!", "danger")
        return redirect(url_for("firmware.route_firmware"))
    if not md.fw.check_acl("@view"):
        flash("Permission denied: Unable to download component", "danger")
        return redirect(url_for("main.route_dashboard"))
    if not md.swids:
        flash("Component had no SWID metadata", "warning")
        return redirect(url_for("components.route_show", component_id=md.component_id))

    # create export
    response = make_response(_build_swid_cyclonedx(md))
    response.headers.set("Content-Type", "application/json")
    response.headers.set(
        "Content-Disposition",
        "attachment",
        filename="{}-v{}-bom.json".format(md.appstream_id, md.version_display),
    )
    return response
