#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=unused-argument

import datetime
from typing import List

from flask import render_template
from flask import current_app as app

from lvfs import db

from lvfs.firmware.models import Firmware
from lvfs.emails import send_email_sync
from lvfs.tasks.models import Task

from .models import User, UserAction


def _user_disable_notify(task: Task) -> None:

    # only on the production instance
    if app.config.get("RELEASE") not in ["production", "development"]:
        task.add_pass("skipping user notifications")
        return

    # find all users that have not logged in for over one year, and have never
    # been warned
    now = datetime.datetime.utcnow()
    for user in (
        db.session.query(User)
        .filter(User.auth_type != "disabled")
        .filter(User.atime < now - datetime.timedelta(days=365))
        .filter(User.mtime < User.atime)
        .filter(User.unused_notify_ts == None)
        .with_for_update(of=User)
    ):
        # send email
        send_email_sync(
            "[LVFS] User account unused: ACTION REQUIRED",
            user.email_address,
            render_template("email-unused.txt", user=user),
            task=task,
        )
        user.unused_notify_ts = now
        db.session.commit()


def _user_disable_actual(task: Task) -> None:

    # only on the production instance
    if app.config.get("RELEASE") not in ["production", "development"]:
        task.add_pass("skipping disabling inactive users")
        return

    # find all users that have an atime greater than 1 year and unused_notify_ts > 6 weeks */
    now = datetime.datetime.utcnow()
    for user in (
        db.session.query(User)
        .filter(User.auth_type != "disabled")
        .filter(User.atime < now - datetime.timedelta(days=365))
        .filter(User.mtime < User.atime)
        .filter(User.unused_notify_ts < now - datetime.timedelta(days=42))
        .with_for_update(of=User)
    ):
        task.add_pass(
            "Disabling user {} as unused".format(user.user_id),
            "{} ({}) ".format(user.username, user.display_name),
        )
        user.auth_type = "disabled"
        db.session.commit()


def _user_add_message_survey(task: Task) -> None:

    # only on the production instance or locally
    if app.config.get("RELEASE") not in ["production", "development"]:
        task.add_pass("skipping message about survey")
        return

    now = datetime.datetime.utcnow()
    for user in (
        db.session.query(User)
        .filter(User.auth_type != "disabled")
        .filter(User.ctime < now - datetime.timedelta(days=14))  # user is not too new
        .filter(User.atime > now - datetime.timedelta(days=90))  # user is not inactive
        .order_by(User.user_id.asc())
        .with_for_update(of=User)
    ):

        # already done
        if user.get_action("done-survey-2021"):
            continue

        # user is too inexperienced
        if len(user.fws) < 2:
            continue

        # do not ask the user again
        user.actions.append(UserAction(value="done-survey-2021"))
        db.session.commit()

        # send email
        send_email_sync(
            "[LVFS] Please complete a short survey to help the LVFS",
            user.email_address,
            render_template(
                "email-survey.txt",
                user=user,
            ),
            task=task,
        )


def _user_email_report(task: Task) -> None:

    # only on the production instance
    if app.config.get("RELEASE") not in ["production", "development"]:
        task.add_pass("skipping email reports")
        return

    # find all users
    for user in db.session.query(User).filter(User.auth_type != "disabled"):
        if not user.get_action("notify-non-public"):
            continue
        fws_embargo: List[Firmware] = []
        fws_testing: List[Firmware] = []
        for fw in user.fws:
            if fw.target_duration < datetime.timedelta(days=30):
                continue
            if fw.remote.name == "testing":
                fws_testing.append(fw)
                continue
            if fw.remote.name.startswith("embargo"):
                fws_embargo.append(fw)
                continue
        if fws_embargo or fws_testing:
            send_email_sync(
                "[LVFS] List of non-public firmware",
                user.email_address,
                render_template(
                    "email-non-public.txt",
                    user=user,
                    fws_embargo=fws_embargo,
                    fws_testing=fws_testing,
                ),
                task=task,
            )


def task_disable(task: Task) -> None:
    _user_disable_notify(task)
    _user_disable_actual(task)
    _user_add_message_survey(task)


def task_email_report(task: Task) -> None:
    _user_email_report(task)
