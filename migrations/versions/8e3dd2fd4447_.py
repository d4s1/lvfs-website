# Custom template
"""empty message

Revision ID: 8e3dd2fd4447
Revises: 87d1787f2a63
Create Date: 2023-04-01 09:23:33.759697

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "8e3dd2fd4447"
down_revision = "87d1787f2a63"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("clients", schema=None) as batch_op:
        batch_op.create_index(batch_op.f("ix_clients_addr"), ["addr"], unique=False)


def downgrade():
    with op.batch_alter_table("clients", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_clients_addr"))
