# Custom template
"""empty message

Revision ID: a3cc33e92890
Revises: 8e3dd2fd4447
Create Date: 2023-04-03 16:31:42.506042

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "a3cc33e92890"
down_revision = "8e3dd2fd4447"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("task_attributes", schema=None) as batch_op:
        batch_op.drop_constraint("task_attributes_task_id_fkey", type_="foreignkey")
        batch_op.create_foreign_key(
            None, "tasks", ["task_id"], ["task_id"], ondelete="CASCADE"
        )


def downgrade():
    with op.batch_alter_table("task_attributes", schema=None) as batch_op:
        batch_op.drop_constraint(None, type_="foreignkey")
        batch_op.create_foreign_key(
            "task_attributes_task_id_fkey", "tasks", ["task_id"], ["task_id"]
        )
